/* dia-canvas.c
 * Copyright (C) 2000  James Henstridge
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <glib.h>
#include <gtk/gtk.h>
#include "dia-canvas.h"
#include "dia-canvas-item.h"
#include "dia-canvas-view.h"
#include "dia-marshal.h"
#include "dia-marshal.c"
#include "dia-canvas-grid.h"

/* higher than the pick or redraw idles of the canvas view */
#define UPDATE_PRIORITY (G_PRIORITY_HIGH_IDLE + 30)

enum {
  RENDER,
  DIRTY_REGION,
  EXTENTS_CHANGED,
  LAST_SIGNAL
};

static void dia_canvas_init		(DiaCanvas *canvas);
static void dia_canvas_class_init	(DiaCanvasClass	 *klass);

static void dia_canvas_finalize         (GObject *object);
static void dia_canvas_real_render	(DiaCanvas		 *canvas,
					 DiaRenderer *renderer,
					 DiaRectangle *rect);
static void dia_canvas_real_dirty_region	(DiaCanvas *canvas,
						 DiaRectangle *rect);


static GObjectClass *parent_class = NULL;
static guint canvas_signals[LAST_SIGNAL] = { 0 };

GType
dia_canvas_get_type (void)
{
  static GType canvas_type = 0;

  if (!canvas_type)
    {
      static const GTypeInfo canvas_info =
      {
	sizeof (DiaCanvasClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) dia_canvas_class_init,
	(GClassFinalizeFunc) NULL,
	NULL,

	sizeof (DiaCanvas),
	0, /* n_preallocs */
	(GInstanceInitFunc) dia_canvas_init,
      };

      canvas_type = g_type_register_static (G_TYPE_OBJECT, "DiaCanvas",
					    &canvas_info, 0);
    }

  return canvas_type;
}

static void
dia_canvas_class_init (DiaCanvasClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS(klass);

  parent_class = g_type_class_ref (G_TYPE_OBJECT);

  canvas_signals[RENDER] = 
    g_signal_new ("render",
		   G_OBJECT_CLASS_TYPE(klass),
		   G_SIGNAL_RUN_FIRST,
		   G_STRUCT_OFFSET (DiaCanvasClass, render), NULL, NULL,
		   dia_marshal_VOID__OBJECT_POINTER,
		   G_TYPE_NONE, 2,
		   DIA_TYPE_RENDERER, G_TYPE_POINTER /* rectangle */);
  canvas_signals[DIRTY_REGION] = 
    g_signal_new ("dirty_region",
		   G_OBJECT_CLASS_TYPE(klass),
		   G_SIGNAL_RUN_FIRST,
		   G_STRUCT_OFFSET (DiaCanvasClass, dirty_region), NULL, NULL,
		   gtk_marshal_NONE__POINTER,
		   G_TYPE_NONE, 1,
		   G_TYPE_POINTER /* rectangle */);
  canvas_signals[EXTENTS_CHANGED] = 
    g_signal_new ("extents_changed",
		   G_OBJECT_CLASS_TYPE(klass),
		   G_SIGNAL_RUN_FIRST,
		   G_STRUCT_OFFSET(DiaCanvasClass, extents_changed), NULL,NULL,
		   gtk_marshal_NONE__NONE,
		   G_TYPE_NONE, 0);

  object_class->finalize = dia_canvas_finalize;
  klass->render = dia_canvas_real_render;
  klass->dirty_region = dia_canvas_real_dirty_region;
}


static void
dia_canvas_init (DiaCanvas *canvas)
{
  canvas->extents.left   = 0.0;
  canvas->extents.top    = 0.0;
  canvas->extents.right  = 100.0;
  canvas->extents.bottom = 100.0;

  canvas->root = DIA_CANVAS_ITEM(g_object_new(DIA_TYPE_CANVAS_GRID, NULL));
  canvas->root->canvas = canvas;

  canvas->views = NULL;
  canvas->selection = NULL;
}

/**
 * dia_canvas_new:
 *
 * Creates a new #DiaCanvas.
 *
 * Return value: a #DiaCanvas.
 **/
DiaCanvas *
dia_canvas_new(void)
{
  DiaCanvas *canvas = DIA_CANVAS(g_object_new(DIA_TYPE_CANVAS, NULL));

  return canvas;
}

static void
dia_canvas_remove_canvas(DiaCanvasItem *item,gpointer user_data)
{
  item->canvas = NULL;
}

static void
dia_canvas_finalize(GObject *object)
{
  DiaCanvas *canvas;

  g_return_if_fail(object != NULL);
  g_return_if_fail(DIA_IS_CANVAS(object));

  canvas = DIA_CANVAS(object);

  /* to prevent redrawing, set the canvas to null */

  if (canvas->root) {
    dia_canvas_group_foreach(DIA_CANVAS_GROUP(canvas->root),(GFunc)dia_canvas_remove_canvas,NULL);
    g_object_unref(G_OBJECT(canvas->root));
  }

  if (canvas->update_id)
    g_source_remove(canvas->update_id);

  g_list_free(canvas->views);

  if (parent_class->finalize)
    (* parent_class->finalize) (object);
}

static void
dia_canvas_real_render (DiaCanvas *canvas, DiaRenderer *renderer,
			DiaRectangle *rect)
{
  static guint render_signal_id = 0;

  g_return_if_fail (canvas != NULL);
  g_return_if_fail (DIA_IS_CANVAS (canvas));

  if (!render_signal_id)
    render_signal_id = g_signal_lookup("render", DIA_TYPE_CANVAS_ITEM);

  g_signal_emit(canvas->root, render_signal_id, 0, renderer, rect);
}

static void
dia_canvas_real_dirty_region (DiaCanvas *canvas, DiaRectangle *rect)
{
  g_return_if_fail (canvas != NULL);
  g_return_if_fail (DIA_IS_CANVAS (canvas));
  
}

/**
 * dia_canvas_set_extents:
 * @canvas: a #DiaCanvas.
 * @extents: ??
 *
 * ??
 **/
void
dia_canvas_set_extents(DiaCanvas *canvas, DiaRectangle *extents)
{
  g_return_if_fail (canvas != NULL);
  g_return_if_fail (DIA_IS_CANVAS (canvas));
  g_return_if_fail (extents->top < extents->bottom);
  g_return_if_fail (extents->left < extents->right);

  if (extents->top != canvas->extents.top ||
      extents->left != canvas->extents.left ||
      extents->bottom != canvas->extents.bottom ||
      extents->right != canvas->extents.right) {
    canvas->extents = *extents;
    g_signal_emit(canvas, canvas_signals[EXTENTS_CHANGED], 0);
  }
}

/**
 * dia_canvas_render:
 * @canvas: a #DiaCanvas.
 * @renderer: the #DiaRenderer used.
 * @rect: the area to render.
 *
 * Renders the @rect area with the @renderer.
 **/
void
dia_canvas_render (DiaCanvas *canvas, DiaRenderer *renderer,
		   DiaRectangle *rect)
{
  g_return_if_fail (canvas != NULL);
  g_return_if_fail (DIA_IS_CANVAS (canvas));
  g_return_if_fail (renderer != NULL);
  g_return_if_fail (DIA_IS_RENDERER (renderer));
  g_return_if_fail (rect->top < rect->bottom);
  g_return_if_fail (rect->left < rect->right);

  g_signal_emit (canvas, canvas_signals[RENDER], 0, renderer, rect);
}

/**
 * dia_canvas_add_view:
 * @canvas: a #DiaCanvas.
 * @view: the #DiaCanvasView to add.
 *
 * Adds the @view to the @canvas.
 **/
void
dia_canvas_add_view(DiaCanvas *canvas, DiaCanvasView *view)
{
  g_return_if_fail(canvas != NULL);
  g_return_if_fail(DIA_IS_CANVAS(canvas));
  g_return_if_fail(view != NULL);
  g_return_if_fail(DIA_IS_CANVAS_VIEW(view));
  g_return_if_fail(view->canvas != NULL);

  canvas->views = g_list_append(canvas->views, view);
  view->canvas = canvas;
}

/**
 * dia_canvas_remove_view:
 * @canvas: a #DiaCanvas.
 * @view: the #DiaCanvasView to remove.
 *
 * removes the @view to the @canvas.
 **/
void
dia_canvas_remove_view(DiaCanvas *canvas, DiaCanvasView *view)
{
  g_return_if_fail(canvas != NULL);
  g_return_if_fail(DIA_IS_CANVAS(canvas));
  g_return_if_fail(view != NULL);
  g_return_if_fail(DIA_IS_CANVAS_VIEW(view));
  g_return_if_fail(view->canvas == canvas);

  canvas->views = g_list_remove(canvas->views, view);
}

static DiaCanvasItem *
pick_traverse(DiaCanvasGroup *group, gdouble x, gdouble y,
	      gdouble close_enough)
{
  GList *tmp;
  DiaCanvasItem *closest = NULL;
  static guint distance_signal_id = 0;

  if (!distance_signal_id)
    distance_signal_id = g_signal_lookup("distance",
					 DIA_TYPE_CANVAS_ITEM);
  for (tmp = group->children; tmp != NULL; tmp = tmp->next) {
    DiaCanvasItem *child = tmp->data;
    gdouble dist;

    /* if point is far enough outside of bounding box */
    if (child->bounds.left - close_enough > x  ||
	child->bounds.right + close_enough < x ||
	child->bounds.top - close_enough > y   ||
	child->bounds.bottom + close_enough < y)
      continue;
    if (!child->visible)
      continue; /* skip invisible children */
    if (DIA_IS_CANVAS_GROUP(child)) {
      DiaCanvasItem *possible = pick_traverse(DIA_CANVAS_GROUP(child),
					      x, y, close_enough);

      if (possible) {
	closest = possible;
	continue;
      }
    }
    g_signal_emit(child, distance_signal_id, 0, x, y, &dist);

    if (dist <= close_enough)
      closest = child;
  }
  return closest;
}

/**
 * dia_canvas_get_item_at:
 * @canvas: a #DiaCanvas.
 * @x: the x position.
 * @y: the y position.
 * @close_enough: ??
 *
 * Returns the #DiaCanvasItem at (or near) the (@x,@y) position.
 *
 * Return value: a #DiaCanvasItem.
 **/
DiaCanvasItem *
dia_canvas_get_item_at(DiaCanvas *canvas, gdouble x, gdouble y,
		       gdouble close_enough)
{
  dia_canvas_ensure_update(canvas);
  return pick_traverse(DIA_CANVAS_GROUP(canvas->root), x, y, close_enough);
}

static gint
start_update(DiaCanvas *canvas)
{
  static guint update_signal_id = 0;

  GDK_THREADS_ENTER();

  canvas->in_update = TRUE;

  if (!update_signal_id)
    update_signal_id = g_signal_lookup("update", DIA_TYPE_CANVAS_ITEM);
  g_signal_emit(canvas->root, update_signal_id, 0);
  canvas->root->need_update = FALSE;

  canvas->in_update = FALSE;
  canvas->update_id = 0;

  GDK_THREADS_LEAVE();

  return FALSE;
}

/**
 * dia_canvas_schedule_update:
 * @canvas: a #DiaCanvas.
 *
 * ??
 **/
void
dia_canvas_schedule_update(DiaCanvas *canvas)
{
  g_return_if_fail(canvas != NULL);
  g_return_if_fail(DIA_IS_CANVAS(canvas));

  if (!canvas->update_id)
    canvas->update_id = g_idle_add_full(UPDATE_PRIORITY,
					(GSourceFunc)start_update,
					canvas, NULL);
}

/**
 * dia_canvas_ensure_update:
 * @canvas: a #DiaCanvas.
 *
 * ??
 **/
void
dia_canvas_ensure_update(DiaCanvas *canvas)
{
  g_return_if_fail(canvas != NULL);
  g_return_if_fail(DIA_IS_CANVAS(canvas));

  if (canvas->update_id) {
    static guint update_signal_id = 0;

    gtk_idle_remove(canvas->update_id);
    canvas->update_id = 0;

    canvas->in_update = TRUE;

    if (!update_signal_id)
      update_signal_id = g_signal_lookup("update", DIA_TYPE_CANVAS_ITEM);
    g_signal_emit(canvas->root, update_signal_id, 0);
    canvas->root->need_update = FALSE;

    canvas->in_update = FALSE;
  }
}

/**
 * dia_canvas_dirty_region:
 * @canvas: a #DiaCanvas.
 * @rect: ??
 *
 * ??
 **/
void
dia_canvas_dirty_region(DiaCanvas *canvas, DiaRectangle *rect)
{
  g_return_if_fail(canvas != NULL);
  g_return_if_fail(DIA_IS_CANVAS(canvas));
  g_return_if_fail(rect != NULL);

  g_signal_emit(canvas, canvas_signals[DIRTY_REGION], 0, rect);
}

/**
 * dia_canvas_snap:
 * @canvas: a #DiaCanvas.
 * @avoid: the item that asked snapping.
 * @x: the x position.
 * @y: the y position.
 *
 * This takes (@x,@y) and snap it to the grid.
 *
 * Returns: the distance.
 **/
gdouble
dia_canvas_snap(DiaCanvas *canvas, gdouble *x, gdouble *y)
{
  static guint snap_signal_id = 0;
  gdouble result;

  g_return_val_if_fail(canvas != NULL,G_MAXDOUBLE);
  g_return_val_if_fail(DIA_IS_CANVAS(canvas),G_MAXDOUBLE);
  g_return_val_if_fail(x != NULL,G_MAXDOUBLE);
  g_return_val_if_fail(y != NULL,G_MAXDOUBLE);

  if (!snap_signal_id)
    snap_signal_id = g_signal_lookup("attract", DIA_TYPE_CANVAS_ITEM);

  g_signal_emit(canvas->root, snap_signal_id, 0, x, y, &result);

  return result;
}

void
dia_canvas_connect(DiaCanvas *canvas, DiaCanvasConnection *connection)
{
  static guint connect_signal_id = 0;

  g_return_if_fail(canvas != NULL);
  g_return_if_fail(DIA_IS_CANVAS(canvas));
  g_return_if_fail(connection != NULL);

  if (!connect_signal_id)
    connect_signal_id = g_signal_lookup("connect", DIA_TYPE_CANVAS_ITEM);

  g_signal_emit(canvas->root, connect_signal_id, 0, connection);
}

void
dia_canvas_add_selection(DiaCanvas *canvas, DiaCanvasItem *item)
{
  if( g_list_find(canvas->selection,item) == NULL ) {
    canvas->selection = g_list_append (canvas->selection,item);
    dia_canvas_item_select(item);
  }
}

void
dia_canvas_remove_from_selection(DiaCanvas *canvas, DiaCanvasItem *item)
{
  GList *element;

  element = g_list_find(canvas->selection,item);
  if( element != NULL ) {
    canvas->selection = g_list_delete_link(canvas->selection,element);
    dia_canvas_item_unselect(item);
  }
}

static void
dia_canvas_unset_selected(DiaCanvasItem *item, gpointer user_data)
{
  dia_canvas_item_unselect(item);
}

void
dia_canvas_clear_selection(DiaCanvas *canvas)
{
  g_list_foreach(canvas->selection,(GFunc)dia_canvas_unset_selected,NULL);
  g_list_free(canvas->selection);
  canvas->selection = NULL;
}

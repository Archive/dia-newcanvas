/* dia-canvas-attracted.c
 * Copyright (C) 2001 Xavier Ordoquy
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "dia-canvas-attracted.h"
#include "dia-canvas.h"
#include "dia-canvas-group.h"

#define _(s) (s)

/* properties */
enum {
  PROP_NONE,
  PROP_X,
  PROP_Y,
  PROP_LAST
};

static void dia_canvas_attracted_init       (DiaCanvasAttracted *attracted);
static void dia_canvas_attracted_class_init (DiaCanvasAttractedClass *klass);
static void dia_canvas_attracted_finalize   (GObject *object);

static void dia_canvas_attracted_set_property (GObject *object,
					       guint property_id,
					       const GValue *value,
					       GParamSpec *pspec);
static void dia_canvas_attracted_get_property (GObject *object,
					       guint property_id,
					       GValue *value,
					       GParamSpec *pspec);

static gdouble  dia_canvas_attracted_real_distance  (DiaCanvasItem *item,
						     gdouble x, gdouble y);
static void     dia_canvas_attracted_real_move      (DiaCanvasItem *item,
						     gdouble dx, gdouble dy);
static gdouble  dia_canvas_attracted_real_snapmove  (DiaCanvasItem *item,
						     gdouble *dx, gdouble *dy);

static GObjectClass *parent_class = NULL;


GType
dia_canvas_attracted_get_type (void)
{
  static GtkType dia_canvas_attracted_type = 0;

  if (!dia_canvas_attracted_type)
    {
      static const GTypeInfo dia_canvas_attracted_info =
      {
        sizeof (DiaCanvasAttractedClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) dia_canvas_attracted_class_init,
        (GClassFinalizeFunc) NULL,
        NULL,
        
        sizeof (DiaCanvasAttracted),
        0, /* n_preallocs */
        (GInstanceInitFunc) dia_canvas_attracted_init,
      };

      dia_canvas_attracted_type = g_type_register_static (DIA_TYPE_CANVAS_ITEM,
							  "DiaCanvasAttracted",
							  &dia_canvas_attracted_info,
							  0);
    }
  return dia_canvas_attracted_type;
}

static void
dia_canvas_attracted_class_init (DiaCanvasAttractedClass *klass)
{
  GObjectClass *object_class;
  DiaCanvasItemClass *item_class;

  object_class = G_OBJECT_CLASS(klass);
  item_class = DIA_CANVAS_ITEM_CLASS(klass);
  parent_class = g_type_class_ref(DIA_TYPE_CANVAS_ITEM);

  object_class->set_property = dia_canvas_attracted_set_property;
  object_class->get_property = dia_canvas_attracted_get_property;
  object_class->finalize     = dia_canvas_attracted_finalize;

  item_class->distance   = dia_canvas_attracted_real_distance;
  item_class->move       = dia_canvas_attracted_real_move;
  item_class->snapmove   = dia_canvas_attracted_real_snapmove;

  g_object_class_install_property (object_class,
				   PROP_X,
				   g_param_spec_double ("x", _("Position"),
							_("The position of the attracted point"),
							-G_MAXDOUBLE,
							G_MAXDOUBLE,
							0.0,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_Y,
				   g_param_spec_double ("y", _("Position"),
							_("The position of the attracted point"),
							-G_MAXDOUBLE,
							G_MAXDOUBLE,
							0,
							G_PARAM_READWRITE));
}

static void
dia_canvas_attracted_init (DiaCanvasAttracted *attracted)
{
  DiaCanvasItem *item = DIA_CANVAS_ITEM(attracted);

  item->independant_move = FALSE;
}

static void
dia_canvas_attracted_finalize(GObject *object)
{
  DiaCanvasAttracted *attracted;
  DiaCanvasItem *item;

  g_return_if_fail(object != NULL);
  g_return_if_fail(DIA_IS_CANVAS_ATTRACTED(object));

  attracted = DIA_CANVAS_ATTRACTED(object);
  item      = DIA_CANVAS_ITEM(object);

  if (parent_class->finalize)
    (* parent_class->finalize) (object);
}

static void
dia_canvas_attracted_set_property (GObject *object, guint property_id,
				   const GValue *value, GParamSpec *pspec)
{
  DiaCanvasAttracted *attracted = DIA_CANVAS_ATTRACTED(object);
  gdouble val;

  switch (property_id)
    {

    case PROP_X:
      val = g_value_get_double(value);
      if (val != attracted->pos.x) {
	attracted->pos.x = val;
	dia_canvas_item_request_update(DIA_CANVAS_ITEM(attracted));
      }
      break;

    case PROP_Y:
      val = g_value_get_double(value);
      if (val != attracted->pos.y) {
	attracted->pos.y = val;
	dia_canvas_item_request_update(DIA_CANVAS_ITEM(attracted));
      }
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
dia_canvas_attracted_get_property (GObject *object, guint property_id,
				   GValue *value, GParamSpec *pspec)
{
  DiaCanvasAttracted *attracted = DIA_CANVAS_ATTRACTED(object);

  switch (property_id)
    {
    case PROP_X:
      g_value_set_double(value, attracted->pos.x);
      break;
    case PROP_Y:
      g_value_set_double(value, attracted->pos.y);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

/*****************************************************************************
 ******************************* S I G N A L S *******************************
 *****************************************************************************/

static gdouble
dia_canvas_attracted_real_distance (DiaCanvasItem *item,
				    gdouble x,gdouble y)
{
  double dx = 0.0, dy = 0.0;

  /* not the exact distance, but close enough for our checks */
  if (x < item->bounds.left)
    dx = item->bounds.left - x;
  else if (x > item->bounds.right)
    dx = x - item->bounds.right;

  if (y < item->bounds.top)
    dy = item->bounds.top - y;
  else if (y > item->bounds.bottom)
    dy = y - item->bounds.bottom;

  return dx*dx + dy*dy;
}

static void
dia_canvas_attracted_real_move (DiaCanvasItem *item,
				gdouble dx, gdouble dy)
{
  DiaCanvasAttracted *attracted = DIA_CANVAS_ATTRACTED(item);

  attracted->pos.x += dx;
  attracted->pos.y += dy;
}

static gdouble
dia_canvas_attracted_real_snapmove (DiaCanvasItem *item,
				    gdouble *dx, gdouble *dy)
{
  gdouble x, y, result;
  DiaCanvasAttracted *attracted = DIA_CANVAS_ATTRACTED(item);

  x = attracted->pos.x + *dx;
  y = attracted->pos.y + *dy;
  item->attract_source = TRUE;
  result = dia_canvas_snap(item->canvas, &x, &y);
  item->attract_source = FALSE;
  *dx = x - attracted->pos.x;
  *dy = y - attracted->pos.y;

  return result;
}

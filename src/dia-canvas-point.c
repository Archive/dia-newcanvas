/* dia-canvas-point.c
 * Copyright (C) 2001 Xavier Ordoquy
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "dia-canvas-point.h"
#include "dia-canvas-group.h"
#include <math.h>

#define _(s) (s)
#define sqr(x) ((x)*(x))

/* properties */
enum {
  PROP_NONE,
  PROP_X,
  PROP_Y,
  PROP_POINT_COLOR,
  PROP_LAST
};

static void dia_canvas_point_init         (DiaCanvasPoint *point);
static void dia_canvas_point_class_init   (DiaCanvasPointClass *klass);

static void   dia_canvas_point_set_property (GObject *object,
					     guint property_id,
					     const GValue *value,
					     GParamSpec *pspec);
static void   dia_canvas_point_get_property (GObject *object,
					     guint property_id,
					     GValue *value,
					     GParamSpec *pspec);

static void    dia_canvas_point_real_render    (DiaCanvasItem *item,
						DiaRenderer *renderer,
						DiaRectangle *rect);
static gdouble dia_canvas_point_real_distance  (DiaCanvasItem *item,
						gdouble x, gdouble y);
static void    dia_canvas_point_real_update    (DiaCanvasItem *item);
static void    dia_canvas_point_real_move      (DiaCanvasItem *item,
						gdouble dx, gdouble dy);

static GObjectClass *parent_class = NULL;

GType
dia_canvas_point_get_type (void)
{
  static GtkType canvas_point_type = 0;

  if (!canvas_point_type)
    {
      static const GTypeInfo canvas_point_info =
      {
        sizeof (DiaCanvasPointClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) dia_canvas_point_class_init,
        (GClassFinalizeFunc) NULL,
        NULL,
        
        sizeof (DiaCanvasPoint),
        0, /* n_preallocs */
        (GInstanceInitFunc) dia_canvas_point_init,
      };

      canvas_point_type = g_type_register_static (DIA_TYPE_CANVAS_ITEM,
						      "DiaCanvasPoint",
						      &canvas_point_info,
						      0);
    }
  return canvas_point_type;
}

static void
dia_canvas_point_class_init (DiaCanvasPointClass *klass)
{
  GObjectClass *object_class;
  DiaCanvasItemClass *item_class;

  object_class = G_OBJECT_CLASS(klass);
  item_class   = DIA_CANVAS_ITEM_CLASS(klass);
  parent_class = g_type_class_ref(DIA_TYPE_CANVAS_ITEM);

  object_class->set_property = dia_canvas_point_set_property;
  object_class->get_property = dia_canvas_point_get_property;

  item_class->render     = dia_canvas_point_real_render;
  item_class->update     = dia_canvas_point_real_update;
  item_class->distance   = dia_canvas_point_real_distance;
  item_class->move       = dia_canvas_point_real_move;

  g_object_class_install_property (object_class,
				   PROP_X,
				   g_param_spec_double ("x", _("Position"),
							_("The position of the point"),
							-G_MAXDOUBLE,
							G_MAXDOUBLE,
							0.0,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_Y,
				   g_param_spec_double ("y", _("Position"),
							_("The position of the point"),
							-G_MAXDOUBLE,
							G_MAXDOUBLE,
							0,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_POINT_COLOR,
				   g_param_spec_boxed ("point_color", _("Point color"),
						       _("The point color of the point"),
						       GDK_TYPE_COLOR,
						       G_PARAM_READWRITE));
}

static void
dia_canvas_point_init (DiaCanvasPoint *point)
{
  GdkColor red   = { 0, 0xffff, 0, 0 };

  point->point_color = red;
}

static void
dia_canvas_point_set_property (GObject *object, guint property_id,
			      const GValue *value, GParamSpec *pspec)
{
  DiaCanvasPoint *point = DIA_CANVAS_POINT(object);
  gdouble val;
  GdkColor *color;

  switch (property_id)
    {
    case PROP_X:
      val = g_value_get_double(value);
      if (val != point->point.x) {
	point->point.x = val;
	dia_canvas_item_request_update(DIA_CANVAS_ITEM(point));
      }
      break;
    case PROP_Y:
      val = g_value_get_double(value);
      if (val != point->point.y) {
	point->point.y = val;
	dia_canvas_item_request_update(DIA_CANVAS_ITEM(point));
      }
      break;
    case PROP_POINT_COLOR:
      color = g_value_get_boxed(value);
      point->point_color = *color;
      dia_canvas_item_request_update(DIA_CANVAS_ITEM(point));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
dia_canvas_point_get_property (GObject *object, guint property_id,
				   GValue *value, GParamSpec *pspec)
{
  DiaCanvasPoint *point = DIA_CANVAS_POINT(object);

  switch (property_id)
    {
    case PROP_X:
      g_value_set_double(value, point->point.x);
      break;
    case PROP_Y:
      g_value_set_double(value, point->point.y);
      break;
    case PROP_POINT_COLOR:
      g_value_set_boxed(value, &point->point_color);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
dia_canvas_point_real_render (DiaCanvasItem *item, DiaRenderer *renderer,
			     DiaRectangle *rect)
{
  DiaCanvasPoint *point = DIA_CANVAS_POINT(item);

  dia_renderer_set_color(renderer, &point->point_color);
  dia_renderer_draw_point(renderer, &point->point);
}

static void
dia_canvas_point_real_update (DiaCanvasItem *item)
{
  DiaCanvasPoint *point = DIA_CANVAS_POINT(item);

  /* the point may have been moved, so dirty its former location */
  dia_canvas_dirty_region(item->canvas, &item->bounds);

  item->bounds.left   = point->point.x;
  item->bounds.top    = point->point.y;
  item->bounds.right  = point->point.x;
  item->bounds.bottom = point->point.y;

  /* dirty the new location to redraw ... */
  dia_canvas_dirty_region(item->canvas, &item->bounds);
}

static gdouble
dia_canvas_point_real_distance (DiaCanvasItem *item, gdouble x,gdouble y)
{
  DiaCanvasPoint *point = DIA_CANVAS_POINT(item);

  return sqrt(sqr(point->point.x - x) + sqr(point->point.y - y));
}

static void
dia_canvas_point_real_move (DiaCanvasItem *item,
			   gdouble dx, gdouble dy)
{
  DiaCanvasPoint *point = DIA_CANVAS_POINT(item);

  point->point.x += dx;
  point->point.y += dy;

  dia_canvas_item_request_update(DIA_CANVAS_ITEM(point));
}

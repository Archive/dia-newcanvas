/* dia-canvas-rectangle.c
 * Copyright (C) 2000  James Henstridge
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "dia-canvas-rectangle.h"
#include "dia-canvas-group.h"

#define _(s) (s)

/* properties */
enum {
  PROP_NONE,
  PROP_X1,
  PROP_Y1,
  PROP_X2,
  PROP_Y2,
  PROP_FILL_COLOR,
  PROP_LINE_COLOR,
  PROP_LINE_WIDTH,
  PROP_TEST_FILL,
  PROP_TEST_LINE,
  PROP_LAST
};

static void dia_canvas_rectangle_init       (DiaCanvasRectangle *rect);
static void dia_canvas_rectangle_class_init (DiaCanvasRectangleClass *klass);

static void    dia_canvas_rectangle_set_property (GObject *object,
						  guint property_id,
						  const GValue *value,
						  GParamSpec *pspec);
static void    dia_canvas_rectangle_get_property (GObject *object,
						  guint property_id,
						  GValue *value,
						  GParamSpec *pspec);

static void    dia_canvas_rectangle_real_render    (DiaCanvasItem *item,
						    DiaRenderer *renderer,
						    DiaRectangle *rectangle);
static gdouble dia_canvas_rectangle_real_distance  (DiaCanvasItem *item,
						    gdouble x, gdouble y);
static void    dia_canvas_rectangle_real_update    (DiaCanvasItem *item);
static void    dia_canvas_rectangle_real_move      (DiaCanvasItem *item,
						    gdouble dx, gdouble dy);

static GObjectClass *parent_class = NULL;


GType
dia_canvas_rectangle_get_type (void)
{
  static GtkType canvas_rectangle_type = 0;

  if (!canvas_rectangle_type)
    {
      static const GTypeInfo canvas_rectangle_info =
      {
        sizeof (DiaCanvasRectangleClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) dia_canvas_rectangle_class_init,
        (GClassFinalizeFunc) NULL,
        NULL,
        
        sizeof (DiaCanvasRectangle),
        0, /* n_preallocs */
        (GInstanceInitFunc) dia_canvas_rectangle_init,
      };

      canvas_rectangle_type = g_type_register_static (DIA_TYPE_CANVAS_ITEM,
						      "DiaCanvasRectangle",
						      &canvas_rectangle_info,
						      0);
    }
  return canvas_rectangle_type;
}

static void
dia_canvas_rectangle_class_init (DiaCanvasRectangleClass *klass)
{
  GObjectClass *object_class;
  DiaCanvasItemClass *item_class;

  object_class = G_OBJECT_CLASS(klass);
  item_class = DIA_CANVAS_ITEM_CLASS(klass);
  parent_class = g_type_class_ref(DIA_TYPE_CANVAS_ITEM);

  object_class->set_property = dia_canvas_rectangle_set_property;
  object_class->get_property = dia_canvas_rectangle_get_property;

  item_class->render     = dia_canvas_rectangle_real_render;
  item_class->update     = dia_canvas_rectangle_real_update;
  item_class->distance   = dia_canvas_rectangle_real_distance;
  item_class->move       = dia_canvas_rectangle_real_move;

  g_object_class_install_property (object_class,
				   PROP_X1,
				   g_param_spec_double ("x1", _("Left edge"),
							_("The position of the left edge of the rectangle"),
							-G_MAXDOUBLE,
							G_MAXDOUBLE,
							0.0,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_Y1,
				   g_param_spec_double ("y1", _("Top edge"),
							_("The position of the top edge of the rectangle"),
							-G_MAXDOUBLE,
							G_MAXDOUBLE,
							0,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_X2,
				   g_param_spec_double ("x2", _("Right edge"),
							_("The position of the right edge of the rectangle"),
							-G_MAXDOUBLE,
							G_MAXDOUBLE,
							0,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_Y2,
				   g_param_spec_double ("y2", _("Bottom edge"),
							_("The position of the bottom edge of the rectangle"),
							-G_MAXDOUBLE,
							G_MAXDOUBLE,
							0,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_FILL_COLOR,
				   g_param_spec_boxed ("fill_color", _("Fill color"),
						       _("The fill color of the rectangle (or NULL for empty)"),
						       GDK_TYPE_COLOR,
						       G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_LINE_COLOR,
				   g_param_spec_boxed ("line_color", _("Line color"),
						       _("The line color of the rectangle (or NULL for no outline)"),
						       GDK_TYPE_COLOR,
						       G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_LINE_WIDTH,
				   g_param_spec_double ("line_width", _("Line width"),
							_("The thickness of rectangle's outline"),
							0, G_MAXDOUBLE, 0,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_TEST_FILL,
				   g_param_spec_boolean ("test_fill", _("Test fill"),
							 _("Test if the rectangle is filled or not"),
							 FALSE,
							 G_PARAM_READABLE));
  g_object_class_install_property (object_class,
				   PROP_TEST_LINE,
				   g_param_spec_boolean ("test_line", _("Test line"),
							 _("Test if the rectangle has border or not"),
							 FALSE,
							 G_PARAM_READABLE));
}

static void
dia_canvas_rectangle_init (DiaCanvasRectangle *rect)
{
}

static void
dia_canvas_rectangle_set_property (GObject *object, guint property_id,
				   const GValue *value, GParamSpec *pspec)
{
  DiaCanvasRectangle *rect = DIA_CANVAS_RECTANGLE(object);
  gdouble val;
  GdkColor *color;

  switch (property_id)
    {
    case PROP_X1:
      val = g_value_get_double(value);
      if (val != rect->topleft.x) {
	rect->topleft.x = val;
	dia_canvas_item_request_update(DIA_CANVAS_ITEM(rect));
      }
      break;
    case PROP_Y1:
      val = g_value_get_double(value);
      if (val != rect->topleft.y) {
	rect->topleft.y = val;
	dia_canvas_item_request_update(DIA_CANVAS_ITEM(rect));
      }
      break;
    case PROP_X2:
      val = g_value_get_double(value);
      if (val != rect->bottomright.x) {
	rect->bottomright.x = val;
	dia_canvas_item_request_update(DIA_CANVAS_ITEM(rect));
      }
      break;
    case PROP_Y2:
      val = g_value_get_double(value);
      if (val != rect->bottomright.y) {
	rect->bottomright.y = val;
	dia_canvas_item_request_update(DIA_CANVAS_ITEM(rect));
      }
      break;
    case PROP_FILL_COLOR:
      color = g_value_get_boxed(value);
      if (color) {
	rect->fill_color = *color;
	rect->fill = TRUE;
      } else {
	rect->fill = FALSE;
      }
      dia_canvas_item_request_update(DIA_CANVAS_ITEM(rect));
      break;
    case PROP_LINE_COLOR:
      color = g_value_get_boxed(value);
      if (color) {
	rect->line_color = *color;
	rect->line = TRUE;
      } else {
	rect->line = FALSE;
      }
      dia_canvas_item_request_update(DIA_CANVAS_ITEM(rect));
      break;
    case PROP_LINE_WIDTH:
      val = g_value_get_double(value);
      if (val != rect->line_width) {
	rect->line_width = val;
	dia_canvas_item_request_update(DIA_CANVAS_ITEM(rect));
      }
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
dia_canvas_rectangle_get_property (GObject *object, guint property_id,
				   GValue *value, GParamSpec *pspec)
{
  DiaCanvasRectangle *rect = DIA_CANVAS_RECTANGLE(object);

  switch (property_id)
    {
    case PROP_X1:
      g_value_set_double(value, rect->topleft.x);
      break;
    case PROP_Y1:
      g_value_set_double(value, rect->topleft.y);
      break;
    case PROP_X2:
      g_value_set_double(value, rect->bottomright.x);
      break;
    case PROP_Y2:
      g_value_set_double(value, rect->bottomright.y);
      break;
    case PROP_FILL_COLOR:
      if (rect->fill)
	g_value_set_boxed(value, &rect->fill_color);
      else
	g_value_set_boxed(value, NULL);
      break;
    case PROP_LINE_COLOR:
      if (rect->line)
	g_value_set_boxed(value, &rect->line_color);
      else
	g_value_set_boxed(value, NULL);
      break;
    case PROP_LINE_WIDTH:
      g_value_set_double(value, rect->line_width);
      break;
    case PROP_TEST_FILL:
      if (rect->fill)
	g_value_set_boolean(value, TRUE);
      else
	g_value_set_boolean(value, FALSE);
      break;
    case PROP_TEST_LINE:
      if (rect->line)
	g_value_set_boolean(value, TRUE);
      else
	g_value_set_boolean(value, FALSE);      
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

/*****************************************************************************
 ******************************* S I G N A L S *******************************
 *****************************************************************************/

static void
dia_canvas_rectangle_real_render (DiaCanvasItem *item,
				  DiaRenderer *renderer,
				  DiaRectangle *rectangle)
{
  DiaCanvasRectangle *rect = DIA_CANVAS_RECTANGLE(item);

  if (rect->fill) {
    dia_renderer_set_color(renderer, &rect->fill_color);
    dia_renderer_draw_rectangle(renderer, TRUE,
				&rect->topleft, &rect->bottomright);
  }
  if (rect->line) {
    dia_renderer_set_color(renderer, &rect->line_color);
    dia_renderer_set_line_width(renderer, rect->line_width);
    dia_renderer_draw_rectangle(renderer, FALSE,
				&rect->topleft, &rect->bottomright);
  }
}

static void
dia_canvas_rectangle_real_update (DiaCanvasItem *item)
{
  DiaCanvasRectangle *rect = DIA_CANVAS_RECTANGLE(item);

  /* the rect may have been moved, so dirty its former location */
  dia_canvas_dirty_region(item->canvas, &item->bounds);

  item->bounds.left   = rect->topleft.x;
  item->bounds.top    = rect->topleft.y;
  item->bounds.right  = rect->bottomright.x;
  item->bounds.bottom = rect->bottomright.y;

  if (rect->line) {
    gdouble half_width = rect->line_width / 2;

    item->bounds.left   -= half_width;
    item->bounds.top    -= half_width;
    item->bounds.right  += half_width;
    item->bounds.bottom += half_width;
  }

  /* dirty the new location to redraw ... */
  dia_canvas_dirty_region(item->canvas, &item->bounds);
}

static gdouble
dia_canvas_rectangle_real_distance (DiaCanvasItem *item,
				    gdouble x,gdouble y)
{
  DiaCanvasRectangle *rect = DIA_CANVAS_RECTANGLE(item);
  double dx = 0.0, dy = 0.0;

  /* not the exact distance, but close enough for our checks */
  if (x < item->bounds.left)
    dx = item->bounds.left - x;
  else if (x > item->bounds.right)
    dx = x - item->bounds.right;

  if (y < item->bounds.top)
    dy = item->bounds.top - y;
  else if (y > item->bounds.bottom)
    dy = y - item->bounds.bottom;

  /* if we are inside the bounding box, and this isn't a filled rect,
   * we need to see how far the point is from the edge */
  if (dx == 0 && dy == 0 && !rect->fill) {
    gdouble hwidth = rect->line ? rect->line_width/2 : 0.0;
    gdouble left   = rect->topleft.x + hwidth;
    gdouble top    = rect->topleft.y + hwidth;
    gdouble right  = rect->bottomright.x - hwidth;
    gdouble bottom = rect->bottomright.x - hwidth;

    dx = MIN(0, x - left);
    dx = MIN(x, right - x);
    dy = MIN(0, y - top);
    dy = MIN(y, bottom - y);
  }

  return dx*dx + dy*dy;
}

static void
dia_canvas_rectangle_real_move (DiaCanvasItem *item,
				gdouble dx, gdouble dy)
{
  DiaCanvasRectangle *rect = DIA_CANVAS_RECTANGLE(item);

  rect->topleft.x     += dx;
  rect->topleft.y     += dy;
  rect->bottomright.x += dx;
  rect->bottomright.y += dy;

  dia_canvas_item_request_update(DIA_CANVAS_ITEM(rect));
}

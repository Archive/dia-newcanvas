/* dia-canvas-grid.c
 * Copyright (C) 2001  Xavier Ordoquy
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "dia-canvas-grid.h"
#include "dia-marshal.h"
#include <math.h>
#include <string.h>

#define _(s) (s)

/* properties */
enum {
  PROP_NONE,
  PROP_SPACING,
  PROP_COLOR,
  PROP_SNAPPING,
  PROP_LAST
};

static void dia_canvas_grid_init           (DiaCanvasGrid *rect);
static void dia_canvas_grid_class_init     (DiaCanvasGridClass *klass);

static void   dia_canvas_grid_set_property (GObject *object,
					    guint property_id,
					    const GValue *value,
					    GParamSpec *pspec);
static void   dia_canvas_grid_get_property (GObject *object,
					    guint property_id,
					    GValue *value,
					    GParamSpec *pspec);

static void   dia_canvas_grid_real_render  (DiaCanvasItem *item,
					    DiaRenderer *renderer,
					    DiaRectangle *rect);
static gdouble dia_canvas_grid_real_attract  (DiaCanvasItem *item,
					      gdouble *x, gdouble *y);
static void    dia_canvas_grid_real_connection_moved (DiaCanvasItem *item,
 						      DiaCanvasConnection *connection,
 						      gdouble dx, gdouble dy);


static DiaCanvasItemClass *grand_parent_class = NULL;
static DiaCanvasGroupClass *parent_class = NULL;

GType
dia_canvas_grid_get_type (void)
{
  static GtkType canvas_grid_type = 0;

  if (!canvas_grid_type)
    {
      static const GTypeInfo canvas_grid_info =
      {
        sizeof (DiaCanvasGridClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) dia_canvas_grid_class_init,
        (GClassFinalizeFunc) NULL,
        NULL,
        
        sizeof (DiaCanvasGrid),
        0, /* n_preallocs */
        (GInstanceInitFunc) dia_canvas_grid_init,
      };

      canvas_grid_type = g_type_register_static (DIA_TYPE_CANVAS_GROUP,
						 "DiaCanvasGrid",
						 &canvas_grid_info,
						 0);
    }
  return canvas_grid_type;
}

static void
dia_canvas_grid_class_init (DiaCanvasGridClass *klass)
{
  GObjectClass *object_class;
  DiaCanvasItemClass *item_class;

  object_class = G_OBJECT_CLASS(klass);
  item_class = DIA_CANVAS_ITEM_CLASS(klass);

  grand_parent_class = g_type_class_ref(DIA_TYPE_CANVAS_ITEM);
  parent_class = g_type_class_ref(DIA_TYPE_CANVAS_GROUP);

  object_class->set_property = dia_canvas_grid_set_property;
  object_class->get_property = dia_canvas_grid_get_property;

  item_class->render           = dia_canvas_grid_real_render;
  item_class->attract          = dia_canvas_grid_real_attract;
  item_class->connection_moved = dia_canvas_grid_real_connection_moved;

  g_object_class_install_property (object_class,
				   PROP_SPACING,
				   g_param_spec_double ("spacing", _("Grid spacing"),
							_("The grid spacing between dots"),
							-G_MAXDOUBLE,
							G_MAXDOUBLE,
							0.0,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_COLOR,
				   g_param_spec_boxed ("color", _("Drawing color"),
						       _("The color used to draw the dot"),
						       GDK_TYPE_COLOR,
						       G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_SNAPPING,
				   g_param_spec_boolean ("snapping", _("Snapping on/off"),
							 _("Turn the snapping mode on or off"),
							 TRUE,
							 G_PARAM_READWRITE));
}

static void
dia_canvas_grid_init (DiaCanvasGrid *rect)
{
  GdkColor color = { 0, 0x0000, 0x0000, 0x00000 };

  memcpy(&(rect->color),&color,sizeof(GdkColor));
  rect->spacing = 50;
  rect->snapping = TRUE;
}

static void
dia_canvas_grid_set_property (GObject *object, guint property_id,
				   const GValue *value, GParamSpec *pspec)
{
  DiaCanvasGrid *rect = DIA_CANVAS_GRID(object);
  gdouble val;
  //GdkColor *color;

  switch (property_id)
    {
    case PROP_SPACING:
      val = g_value_get_double(value);
      if (val != rect->spacing) {
	rect->spacing = val;
	dia_canvas_item_request_update(DIA_CANVAS_ITEM(rect));
      }
      break;
    case PROP_COLOR:
      g_warning("dia_canvas_grid_set_property: implement PROP_COLOR");
      break;
    case PROP_SNAPPING:
      val = g_value_get_boolean(value);
      rect->snapping = val;
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
dia_canvas_grid_get_property (GObject *object, guint property_id,
				   GValue *value, GParamSpec *pspec)
{
  DiaCanvasGrid *rect = DIA_CANVAS_GRID(object);

  switch (property_id)
    {
    case PROP_SPACING:
      g_value_set_double(value, rect->spacing);
      break;
    case PROP_COLOR:
      g_warning("dia_canvas_grid_get_property: PROP_COLOR");
      break;
    case PROP_SNAPPING:
      g_value_set_boolean(value, rect->snapping);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
dia_canvas_grid_real_render (DiaCanvasItem *item, DiaRenderer *renderer,
			     DiaRectangle *rectangle)
{
  gint i, j, x, y;
  DiaCanvasGrid *grid = DIA_CANVAS_GRID(item);
  DiaPoint pt;

  if (rectangle == NULL) {
    g_warning("RECTANGLE NULL");
    return;
  }

  dia_renderer_set_color(renderer, &(grid->color));
  dia_renderer_set_line_width(renderer, 1);
  x = floor(rectangle->left/grid->spacing);
  y = floor(rectangle->top/grid->spacing);

  for( i=x; (i*grid->spacing)<rectangle->right; i++ ) {
    pt.x = i*grid->spacing;
    for( j=y; (j*grid->spacing)<rectangle->bottom; j++ ) {
      pt.y = j*grid->spacing;
      dia_renderer_draw_point(renderer,&pt);
    }
  }
  parent_class->parent_class.render(item,renderer,rectangle);
}

static gdouble
dia_canvas_grid_real_attract(DiaCanvasItem *item,
			     gdouble *x, gdouble *y)
{
  gdouble ddx,ddy;
  DiaCanvasGrid *grid = DIA_CANVAS_GRID(item);
  GList *tmp, *best;
  gdouble x2, y2;
  gdouble signaled, result;
  static guint event_signal_id = 0;
  DiaCanvasGroup *group = DIA_CANVAS_GROUP(item);
  guint i;

  if (event_signal_id == 0)
    event_signal_id = gtk_signal_lookup("attract", DIA_TYPE_CANVAS_ITEM);

  result = G_MAXDOUBLE;
  best = NULL;
  i = 0;

  for (tmp = group->children; tmp != NULL; tmp = tmp->next) {
    DiaCanvasItem *child = tmp->data;
    x2 = *x;
    y2 = *y;
    g_signal_emit(child, event_signal_id, 0, &x2, &y2, &signaled);
    if( result >= signaled ) {
      best = tmp;
      result = signaled;
    }
  }

  if( grid->snapping ) {
    ddx = rint((*x)/grid->spacing)*grid->spacing;
    ddy = rint((*y)/grid->spacing)*grid->spacing;
    ddx = ddx - *x;
    ddy = ddy - *y;
  } else {
    ddx = 0;
    ddy = 0;
  }
  signaled = ((ddx)*(ddx)+(ddy)*(ddy));

  if( (result < 400) && !(signaled < result) ) {
    g_signal_emit(best->data, event_signal_id, 0, x, y, &signaled);
  } else {
    *x += ddx;
    *y += ddy;
  }

  return signaled;
}

static void
dia_canvas_grid_real_connection_moved (DiaCanvasItem *item,
				       DiaCanvasConnection *connection,
				       gdouble dx, gdouble dy)
{
}

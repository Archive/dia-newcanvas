/* dia-canvas-group.c
 * Copyright (C) 2000  James Henstridge
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gtk/gtk.h>
#include "dia-canvas-item.h"
#include "dia-canvas-group.h"
#include "dia-canvas-rectangle.h"
#include "dia-canvas-line.h"
//#include "dia-conn.h"

static void dia_canvas_group_init	(DiaCanvasGroup	     *canvas_group);
static void dia_canvas_group_class_init	(DiaCanvasGroupClass *klass);

static void dia_canvas_group_finalize(GObject *object);

static void    dia_canvas_group_real_render  (DiaCanvasItem *item,
					      DiaRenderer *renderer,
					      DiaRectangle *rect);
static void    dia_canvas_group_real_update  (DiaCanvasItem *item);
static void    dia_canvas_group_real_move    (DiaCanvasItem *item,
					      gdouble dx, gdouble dy);
static gdouble dia_canvas_group_real_snapmove (DiaCanvasItem *item,
					       gdouble *dx, gdouble *dy);
static gdouble dia_canvas_group_real_attract  (DiaCanvasItem *item,
					       gdouble *dx, gdouble *dy);

static void    dia_canvas_group_real_connect  (DiaCanvasItem *item,
					       DiaCanvasConnection *connection);
static void    dia_canvas_group_real_connection  (DiaCanvasItem *item);
static void    dia_canvas_group_foreach_function (gpointer data,
						  gpointer user_data);

static DiaCanvasItemClass *parent_class = NULL;


GtkType
dia_canvas_group_get_type (void)
{
  static GtkType canvas_group_type = 0;

  if (!canvas_group_type)
    {
      static const GTypeInfo canvas_group_info =
      {
	sizeof (DiaCanvasGroupClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) dia_canvas_group_class_init,
	(GClassFinalizeFunc) NULL,
	NULL,
	
	sizeof (DiaCanvasGroup),
	0, /* n_preallocs */
	(GInstanceInitFunc) dia_canvas_group_init,
      };

      canvas_group_type = g_type_register_static (DIA_TYPE_CANVAS_ITEM,
						  "DiaCanvasGroup",
						  &canvas_group_info, 0);
    }

  return canvas_group_type;
}

static void
dia_canvas_group_class_init (DiaCanvasGroupClass *klass)
{
  GObjectClass *object_class;
  DiaCanvasItemClass *item_class;

  object_class = G_OBJECT_CLASS(klass);
  item_class = DIA_CANVAS_ITEM_CLASS(klass);

  parent_class = g_type_class_ref (DIA_TYPE_CANVAS_ITEM);

  object_class->finalize = dia_canvas_group_finalize;

  item_class->render     = dia_canvas_group_real_render;
  item_class->update     = dia_canvas_group_real_update;
  item_class->attract    = dia_canvas_group_real_attract;
  item_class->move       = dia_canvas_group_real_move;
  item_class->snapmove   = dia_canvas_group_real_snapmove;
  item_class->connect    = dia_canvas_group_real_connect;
  item_class->connection = dia_canvas_group_real_connection;
}


static void
dia_canvas_group_init (DiaCanvasGroup *canvas_group)
{
  
}

static void
dia_canvas_group_finalize(GObject *object)
{
  GList *list;
  DiaCanvasGroup *group;

  g_return_if_fail(object != NULL);
  g_return_if_fail(DIA_IS_CANVAS_GROUP(object));

  group = DIA_CANVAS_GROUP(object);

  /* unref all children */

  list = group->children;

  while( list != NULL ) {
    group->children = g_list_remove_link(group->children,list);
    g_object_unref(list->data);
    list = group->children;
  }

  if (G_OBJECT_CLASS(parent_class)->finalize)
    (* G_OBJECT_CLASS(parent_class)->finalize) (object);
}

/*****************************************************************************
 ******************************* S I G N A L S *******************************
 *****************************************************************************/

static void
dia_canvas_group_real_render(DiaCanvasItem *item, DiaRenderer *renderer,
			DiaRectangle *rect)
{
  DiaCanvasGroup *group;
  GList *tmp;
  static guint render_signal_id = 0;

  g_return_if_fail(item != NULL);
  g_return_if_fail(DIA_IS_CANVAS_GROUP(item));
  g_return_if_fail(rect != NULL);

  group = DIA_CANVAS_GROUP(item);

  /* if we are not visible, don't do anything */
  if (!item->visible)
    return;

  if (!render_signal_id)
    render_signal_id = g_signal_lookup("render", DIA_TYPE_CANVAS_ITEM);

  for (tmp = group->children; tmp != NULL; tmp = tmp->next) {
    DiaCanvasItem *child = tmp->data;
    DiaRectangle intersect;

    /* if child is visible, and its bouding box intersects the redraw region */
    if (child->visible &&
	dia_rectangle_intersect(rect, &child->bounds, &intersect))
      g_signal_emit(child, render_signal_id, 0, renderer, &intersect);
  }
}

static void
dia_canvas_group_real_update(DiaCanvasItem *item)
{
  DiaCanvasGroup *group;
  DiaRectangle new_bounds = { G_MAXDOUBLE, G_MAXDOUBLE,
			      G_MINDOUBLE, G_MINDOUBLE };
  gboolean set = FALSE;
  static guint update_signal_id = 0;
  GList *tmp;

  g_return_if_fail(item != NULL);
  g_return_if_fail(DIA_IS_CANVAS_GROUP(item));

  group = DIA_CANVAS_GROUP(item);

  if (!update_signal_id)
    update_signal_id = g_signal_lookup("update", DIA_TYPE_CANVAS_ITEM);

  for (tmp = group->children; tmp != NULL; tmp = tmp->next) {
    DiaCanvasItem *child = tmp->data;

    if (child->need_update) {
      child->need_update = FALSE;
      g_signal_emit(child, update_signal_id, 0);
    }

    if (child->visible) {
      set = TRUE;
      new_bounds.left = MIN(new_bounds.left, child->bounds.left);
      new_bounds.top = MIN(new_bounds.top, child->bounds.top);
      new_bounds.right = MAX(new_bounds.right, child->bounds.right);
      new_bounds.bottom = MAX(new_bounds.bottom, child->bounds.bottom);
    }
  }
  if (!set) {
    new_bounds.left = 0.0;
    new_bounds.top = 0.0;
    new_bounds.right = 0.0;
    new_bounds.bottom = 0.0;
  }
  item->bounds = new_bounds;
}

static void
dia_canvas_group_real_move (DiaCanvasItem *item,
			    gdouble dx, gdouble dy)
{
  GList *tmp;
  static guint event_signal_id = 0;
  DiaCanvasGroup *group = DIA_CANVAS_GROUP(item);

  if (event_signal_id == 0)
    event_signal_id = gtk_signal_lookup("move", DIA_TYPE_CANVAS_ITEM);

  for (tmp = group->children; tmp != NULL; tmp = tmp->next) {
    g_signal_emit(tmp->data, event_signal_id, 0, dx, dy);
  }
}

static gdouble
dia_canvas_group_real_snapmove (DiaCanvasItem *item,
				gdouble *dx, gdouble *dy)
{
  gdouble result, best;
  gdouble dx2, dy2;
  gdouble dx_best, dy_best;
  GList *tmp;
  static guint event_signal_id = 0;
  DiaCanvasGroup *group = DIA_CANVAS_GROUP(item);

  if (event_signal_id == 0)
    event_signal_id = gtk_signal_lookup("snapmove", DIA_TYPE_CANVAS_ITEM);

  best = G_MAXDOUBLE;
  dx_best = *dx;
  dy_best = *dy;
  item->attract_source = TRUE;
  for (tmp = group->children; tmp != NULL; tmp = tmp->next) {
    dx2 = *dx;
    dy2 = *dy;
    g_signal_emit(tmp->data, event_signal_id, 0, &dx2, &dy2, &result);
    if( result < best ) {
      best = result;
      dx_best = dx2;
      dy_best = dy2;
    }
  }
  item->attract_source = FALSE;

  *dx = dx_best;
  *dy = dy_best;
  return best;
}

static gdouble
dia_canvas_group_real_attract (DiaCanvasItem *item,
			       gdouble *dx, gdouble *dy)
{
  GList *tmp, *best;
  gdouble x, y, x_save, y_save;
  gdouble signaled, result;
  static guint event_signal_id = 0;
  DiaCanvasGroup *group = DIA_CANVAS_GROUP(item);

  if (event_signal_id == 0)
    event_signal_id = gtk_signal_lookup("attract", DIA_TYPE_CANVAS_ITEM);

  result = G_MAXDOUBLE;
  best = NULL;
  x_save = *dx;
  y_save = *dy;

  if( item->attract_source == FALSE ) {
    for (tmp = group->children; tmp != NULL; tmp = tmp->next) {
      DiaCanvasItem *child = tmp->data;
      x = *dx;
      y = *dy;
      g_signal_emit(child, event_signal_id, 0, &x, &y, &signaled);
      if( result >= signaled ) {
	best = tmp;
	result = signaled;
	x_save = x;
	y_save = y;
      }
    }
  }

  *dx = x_save;
  *dy = y_save;

  return result;
}

static void
dia_canvas_group_real_connect (DiaCanvasItem *item,
			       DiaCanvasConnection *connection)
{
  GList *tmp;
  static guint event_signal_id = 0;
  DiaCanvasGroup *group = DIA_CANVAS_GROUP(item);

  if (event_signal_id == 0)
    event_signal_id = gtk_signal_lookup("connect", DIA_TYPE_CANVAS_ITEM);

  if( item->connect_source == FALSE ) {
    for (tmp = group->children; tmp != NULL; tmp = tmp->next) {
      g_signal_emit(tmp->data, event_signal_id, 0, connection);
    }
  }
}


static void
dia_canvas_group_real_connection (DiaCanvasItem *item)
{
  GList *tmp;
  static guint event_signal_id = 0;
  DiaCanvasGroup *group = DIA_CANVAS_GROUP(item);

  if (event_signal_id == 0)
    event_signal_id = gtk_signal_lookup("connection", DIA_TYPE_CANVAS_ITEM);

  item->connect_source = TRUE;
  for (tmp = group->children; tmp != NULL; tmp = tmp->next) {
    g_signal_emit(tmp->data, event_signal_id, 0);
  }
  item->connect_source = FALSE;
}

/*****************************************************************************
 ***************************** F O N C T I O N S *****************************
 *****************************************************************************/

/**
 * dia_canvas_group_add_item:
 * @group: a #DiaCanvasGroup.
 * @item: the #DiaCanvasItem to add.
 *
 * Adds a #DiaCanvasItem to the @group.
 **/
void
dia_canvas_group_add_item (DiaCanvasGroup *group, DiaCanvasItem *item)
{
  g_return_if_fail(group != NULL);
  g_return_if_fail(DIA_IS_CANVAS_GROUP(group));
  g_return_if_fail(item != NULL);
  g_return_if_fail(DIA_IS_CANVAS_ITEM(item));
  g_return_if_fail(item->parent == NULL);

  g_object_ref(G_OBJECT(item));
  dia_canvas_item_sink(item);
  group->children = g_list_append(group->children, item);
  item->parent = group;
  item->canvas = DIA_CANVAS_ITEM(group)->canvas;
  g_object_notify(G_OBJECT(item), "parent");
  dia_canvas_item_request_update (item);
}

/**
 * dia_canvas_group_remove_item:
 * @group: a #DiaCanvasGroup.
 * @item: the #DiaCanvasItem to remove.
 *
 * Removes a #DiaCanvasItem from the @group.
 **/
void
dia_canvas_group_remove_item (DiaCanvasGroup *group, DiaCanvasItem *item)
{
  g_return_if_fail(group != NULL);
  g_return_if_fail(DIA_IS_CANVAS_GROUP(group));
  g_return_if_fail(item != NULL);
  g_return_if_fail(DIA_IS_CANVAS_ITEM(item));
  g_return_if_fail(item->parent == group);

  if( item->canvas != NULL ) {
    /* if the item to be removed is currently focused, unfocus it */
    if (item->canvas->focus_item == item) {
      /* if a view is focused, synthesise the focus_out and in events */
      if (item->canvas->focus_view != NULL) {
	DiaCanvasView *focus_view = item->canvas->focus_view;
	GdkEventFocus fevent;

	/* synthesise focus leaving the focused view */
	fevent.type = GDK_FOCUS_CHANGE;
	fevent.window = GTK_WIDGET(focus_view)->window;
	fevent.in = FALSE;
	gtk_widget_event(GTK_WIDGET(focus_view), (GdkEvent *)&fevent);

	item->canvas->focus_item = NULL;

	/* synthesise focus entering the view again */
	fevent.type = GDK_FOCUS_CHANGE;
	fevent.window = GTK_WIDGET(focus_view)->window;
	fevent.in = TRUE;
	gtk_widget_event(GTK_WIDGET(focus_view), (GdkEvent *)&fevent);
      } else {
	/* else, just unset the focus_item */
	item->canvas->focus_item = NULL;
      }
    }
  }

  if( g_list_find(group->children, item) == NULL ) {
    return;
  }

  group->children = g_list_remove(group->children, item);

  item->parent = NULL;
  if( item->canvas != NULL ) {
    dia_canvas_dirty_region(item->canvas, &item->bounds);
  }
  item->canvas = NULL;
  g_object_notify(G_OBJECT(item), "parent");
  g_object_unref(G_OBJECT(item));
  //dia_canvas_item_request_update(DIA_CANVAS_ITEM(group));
}

static void
dia_canvas_group_foreach_function (gpointer data,
				   gpointer user_data)
{
  GFunc function = ((gpointer*)user_data)[0];

  if( DIA_IS_CANVAS_GROUP(data) ) {
    dia_canvas_group_foreach(DIA_CANVAS_GROUP(data),((gpointer*)user_data)[0],((gpointer*)user_data)[1]);
  }
  function(data,((gpointer*)user_data)[1]);
}

void
dia_canvas_group_foreach (DiaCanvasGroup *group,
			  GFunc function,
			  gpointer user_data)
{
  gpointer hack[2];

  g_return_if_fail(DIA_IS_CANVAS_GROUP(group));

  hack[0] = function;
  hack[1] = user_data;
  g_list_foreach(group->children,dia_canvas_group_foreach_function,&hack);
  function(group,user_data);
}

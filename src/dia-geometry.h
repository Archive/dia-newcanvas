/* dia-renderer.h
 * Copyright (C) 2000  James Henstridge, Alexander Larsson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __DIA_GEOMETRY_H__
#define __DIA_GEOMETRY_H__

#include <glib.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef struct _DiaPoint     DiaPoint;
typedef struct _DiaRectangle DiaRectangle;
typedef struct _DiaSnapPoint DiaSnapPoint;

struct _DiaRectangle {
  gdouble left, top, right, bottom;
};

struct _DiaPoint {
  gdouble x, y;
};

struct _DiaSnapPoint {
  gdouble  x, y;
  gint     flag;
};

gboolean dia_rectangle_intersect (DiaRectangle *src1,
				  DiaRectangle *src2,
				  DiaRectangle *dest);
gdouble  distance_line_point(const DiaPoint *line_start,
			     const DiaPoint *line_end,
			     gdouble line_width,
			     const DiaPoint *point);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __DIA_GEOMETRY_H__ */

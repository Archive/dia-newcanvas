/* dia-canvas-item.h
 * Copyright (C) 2000  James Henstridge
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __DIA_CANVAS_ITEM_H__
#define __DIA_CANVAS_ITEM_H__

#include <gtk/gtk.h>
#include "dia-canvas.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define DIA_TYPE_CANVAS_ITEM            (dia_canvas_item_get_type ())
#define DIA_CANVAS_ITEM(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), DIA_TYPE_CANVAS_ITEM, DiaCanvasItem))
#define DIA_CANVAS_ITEM_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  DIA_TYPE_CANVAS_ITEM, DiaCanvasItemClass))
#define DIA_IS_CANVAS_ITEM(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), DIA_TYPE_CANVAS_ITEM))
#define DIA_IS_CANVAS_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj),    DIA_TYPE_CANVAS_ITEM))
#define DIA_CANVAS_ITEM_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj),   DIA_TYPE_CANVAS_ITEM, DiaCanvasItemClass))

/* forward declaration of DiaCanvasItem in dia-canvas.h */
/* typedef struct _DiaCanvasItem       DiaCanvasItem; */
typedef struct _DiaCanvasItemClass  DiaCanvasItemClass;

struct _DiaCanvasItem
{
  GObject object;

  DiaCanvas *canvas;
  DiaCanvasGroup *parent;

  /* Bounds in which the item fits */
  DiaRectangle bounds;

  guint floating : 1;
  guint visible : 1;
  guint need_update : 1;

  /* Does it moves with the group or alone ? */
  guint independant_move : 1;

  /* is it the source of the attract ? */
  guint attract_source : 1;

  /* is it the source of the connect ? */
  guint connect_source : 1;

  /* Does it issue the connection move ? */
  guint connection_moved : 1;

  /* is the item in the selection ? */
  guint selected : 1;
};

struct _DiaCanvasItemClass
{
  GObjectClass parent_class;

  void     (*render)	 (DiaCanvasItem *item,
			  DiaRenderer *renderer,
			  DiaRectangle *rect);

  /* Computes the distance */
  gdouble  (* distance)  (DiaCanvasItem *item,
			  gdouble x,
			  gdouble y);

  void     (* update)    (DiaCanvasItem *item);

  gboolean (* event)     (DiaCanvasItem *item,
			  GdkEvent *event,
			  DiaCanvasView *view);

  /* Signal for simple movement */
  void     (* move)      (DiaCanvasItem *item,
			  gdouble dx,
			  gdouble dy);

  /* (dx,dy) to add to move to the nearest attraction point */
  gdouble  (* attract)   (DiaCanvasItem *item,
			  gdouble *x,
		 	  gdouble *y);

  /* Try a move with snap constraints */
  gdouble  (* snapmove)  (DiaCanvasItem *item,
			  gdouble *dx,
			  gdouble *dy);

  /* Try to see if connection can be established */
  void     (* connect) (DiaCanvasItem *item,
			DiaCanvasConnection *connection);

  /* Ask the item to connect other items */
  void     (* connection) (DiaCanvasItem *item);

  /* Received when a connected object has moved */
  void     (* connection_moved) (DiaCanvasItem *item,
				 DiaCanvasConnection *connection,
				 gdouble dx, gdouble dy);

  void     (* select)           (DiaCanvasItem *item,
				 gboolean selected);

};


GType dia_canvas_item_get_type (void);
void  dia_canvas_item_request_update (DiaCanvasItem *item);
void  dia_canvas_item_sink (DiaCanvasItem *item);

void  dia_canvas_item_show (DiaCanvasItem *item);
void  dia_canvas_item_hide (DiaCanvasItem *item);

void    dia_canvas_item_move (DiaCanvasItem *item,
			      gdouble dx,
			      gdouble dy);

gdouble dia_canvas_item_snapmove (DiaCanvasItem *item,
				  gdouble *dx,
				  gdouble *dy);

void    dia_canvas_item_make_connection (DiaCanvasItem *item);

void  dia_canvas_item_select   (DiaCanvasItem *item);
void  dia_canvas_item_unselect (DiaCanvasItem *item);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __DIA_CANVAS_ITEM_H__ */

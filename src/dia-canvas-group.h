/* dia-canvas-group.h
 * Copyright (C) 2001 Xavier Ordoquy
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __DIA_CANVAS_GROUP_H__
#define __DIA_CANVAS_GROUP_H__

#include <gtk/gtk.h>
#include "dia-canvas.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define DIA_TYPE_CANVAS_GROUP			(dia_canvas_group_get_type ())
#define DIA_CANVAS_GROUP(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), DIA_TYPE_CANVAS_GROUP, DiaCanvasGroup))
#define DIA_CANVAS_GROUP_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST ((klass), DIA_TYPE_CANVAS_GROUP, DiaCanvasGroupClass))
#define DIA_IS_CANVAS_GROUP(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), DIA_TYPE_CANVAS_GROUP))
#define DIA_IS_CANVAS_GROUP_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((obj), DIA_TYPE_CANVAS_GROUP))
#define DIA_CANVAS_GROUP_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), DIA_TYPE_CANVAS_GROUP, DiaCanvasGroupClass))

//typedef struct _DiaCanvasGroup       DiaCanvasGroup;
typedef struct _DiaCanvasGroupClass  DiaCanvasGroupClass;

struct _DiaCanvasGroup
{
  DiaCanvasItem parent;

  GList *children;
};

struct _DiaCanvasGroupClass
{
  DiaCanvasItemClass parent_class;
};


GType  dia_canvas_group_get_type    (void);

void   dia_canvas_group_add_item    (DiaCanvasGroup *group,
				     DiaCanvasItem *item);
void   dia_canvas_group_remove_item (DiaCanvasGroup *group,
				     DiaCanvasItem *item);

void   dia_canvas_group_foreach     (DiaCanvasGroup *group,
				     GFunc function,
 				     gpointer user_data);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __DIA_CANVAS_ITEM_H__ */

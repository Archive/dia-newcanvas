/* dia-canvas-view-aa.c
 * Copyright (C) 2000  James Henstridge
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gtk/gtk.h>
#include "dia-canvas-view-aa.h"
static void dia_canvas_view_aa_init		(DiaCanvasViewAA		 *canvas_view_aa);
static void dia_canvas_view_aa_class_init	(DiaCanvasViewAAClass	 *klass);


static DiaCanvasViewClass *parent_class = NULL;


GtkType
dia_canvas_view_aa_get_type (void)
{
  static GtkType canvas_view_aa_type = 0;

  if (!canvas_view_aa_type)
    {
      static const GtkTypeInfo canvas_view_aa_info =
      {
        "DiaCanvasViewAA",
        sizeof (DiaCanvasViewAA),
        sizeof (DiaCanvasViewAAClass),
        (GtkClassInitFunc) dia_canvas_view_aa_class_init,
        (GtkObjectInitFunc) dia_canvas_view_aa_init,
        /* reserved_1 */ NULL,
        /* reserved_2 */ NULL,
        (GtkClassInitFunc) NULL,
      };

      canvas_view_aa_type = gtk_type_unique (dia_canvas_view_get_type (), &canvas_view_aa_info);
    }

  return canvas_view_aa_type;
}

static void
dia_canvas_view_aa_class_init (DiaCanvasViewAAClass *klass)
{
  GtkObjectClass *object_class;

  object_class = (GtkObjectClass*) klass;

  parent_class = gtk_type_class (dia_canvas_view_get_type ());

}


static void
dia_canvas_view_aa_init (DiaCanvasViewAA *canvas_view_aa)
{
  
}



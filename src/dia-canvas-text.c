/* dia-canvas-text.c
 * Copyright (C) 2001 Xavier Ordoquy
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "dia-canvas-text.h"
#include "dia-canvas-group.h"

#include <string.h>

#define _(s) (s)
#define sqr(x) ((x)*(x))

/* properties */
enum {
  PROP_NONE,
  PROP_X,
  PROP_Y,
  PROP_TEXT,
  PROP_TEXT_COLOR,
  PROP_TEXT_WIDTH,
  PROP_LAST
};

static void dia_canvas_text_init         (DiaCanvasText *text);
static void dia_canvas_text_class_init   (DiaCanvasTextClass *klass);

static void   dia_canvas_text_set_property (GObject *object,
					    guint property_id,
					    const GValue *value,
					    GParamSpec *pspec);
static void   dia_canvas_text_get_property (GObject *object,
					    guint property_id,
					    GValue *value,
					    GParamSpec *pspec);

static void    dia_canvas_text_real_render    (DiaCanvasItem *item,
					       DiaRenderer *renderer,
					       DiaRectangle *rect);
static gdouble dia_canvas_text_real_distance  (DiaCanvasItem *item,
					       gdouble x, gdouble y);
static void    dia_canvas_text_real_update    (DiaCanvasItem *item);
static void    dia_canvas_text_real_move      (DiaCanvasItem *item,
					       gdouble dx, gdouble dy);

static GObjectClass *parent_class = NULL;

GType
dia_canvas_text_get_type (void)
{
  static GtkType canvas_text_type = 0;

  if (!canvas_text_type)
    {
      static const GTypeInfo canvas_text_info =
      {
        sizeof (DiaCanvasTextClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) dia_canvas_text_class_init,
        (GClassFinalizeFunc) NULL,
        NULL,
        
        sizeof (DiaCanvasText),
        0, /* n_preallocs */
        (GInstanceInitFunc) dia_canvas_text_init,
      };

      canvas_text_type = g_type_register_static (DIA_TYPE_CANVAS_ITEM,
						 "DiaCanvasText",
						 &canvas_text_info,
						 0);
    }
  return canvas_text_type;
}

static void
dia_canvas_text_class_init (DiaCanvasTextClass *klass)
{
  GObjectClass *object_class;
  DiaCanvasItemClass *item_class;

  object_class = G_OBJECT_CLASS(klass);
  item_class   = DIA_CANVAS_ITEM_CLASS(klass);
  parent_class = g_type_class_ref(DIA_TYPE_CANVAS_ITEM);

  object_class->set_property = dia_canvas_text_set_property;
  object_class->get_property = dia_canvas_text_get_property;

  item_class->render     = dia_canvas_text_real_render;
  item_class->update     = dia_canvas_text_real_update;
  item_class->distance   = dia_canvas_text_real_distance;
  item_class->move       = dia_canvas_text_real_move;

  g_object_class_install_property (object_class,
				   PROP_X,
				   g_param_spec_double ("x", _("Position"),
							_("The position of the text"),
							-G_MAXDOUBLE,
							G_MAXDOUBLE,
							0.0,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_Y,
				   g_param_spec_double ("y", _("Position"),
							_("The position of the text"),
							-G_MAXDOUBLE,
							G_MAXDOUBLE,
							0,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_TEXT,
				   g_param_spec_string ("text", _("The text"),
							_("The text"),
							"",
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_TEXT_COLOR,
				   g_param_spec_boxed ("text_color", _("Text color"),
						       _("The text color of the text"),
						       GDK_TYPE_COLOR,
						       G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_TEXT_WIDTH,
				   g_param_spec_double ("text_width", _("Text width"),
							_("The thickness of text"),
							0, G_MAXDOUBLE, 0,
							G_PARAM_READWRITE));
}

static void
dia_canvas_text_init (DiaCanvasText *text)
{
  GdkColor red   = { 0, 0xffff, 0, 0 };

  text->text_color = red;
  text->text_width = 1;
}

static void
dia_canvas_text_set_property (GObject *object, guint property_id,
			      const GValue *value, GParamSpec *pspec)
{
  DiaCanvasText *text = DIA_CANVAS_TEXT(object);
  gdouble   val;
  GdkColor *color;

  switch (property_id)
    {
    case PROP_X:
      val = g_value_get_double(value);
      if (val != text->point.x) {
	text->point.x = val;
	dia_canvas_item_request_update(DIA_CANVAS_ITEM(text));
      }
      break;
    case PROP_Y:
      val = g_value_get_double(value);
      if (val != text->point.y) {
	text->point.y = val;
	dia_canvas_item_request_update(DIA_CANVAS_ITEM(text));
      }
      break;
    case PROP_TEXT:
      if( text->text != NULL ) {
	g_free(text->text);
      }
      text->text = strdup(g_value_get_string(value));
      dia_canvas_item_request_update(DIA_CANVAS_ITEM(text));
      break;
    case PROP_TEXT_COLOR:
      color = g_value_get_boxed(value);
      text->text_color = *color;
      dia_canvas_item_request_update(DIA_CANVAS_ITEM(text));
      break;
    case PROP_TEXT_WIDTH:
      val = g_value_get_double(value);
      text->text_width = val;
      dia_canvas_item_request_update(DIA_CANVAS_ITEM(text));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
dia_canvas_text_get_property (GObject *object, guint property_id,
				   GValue *value, GParamSpec *pspec)
{
  DiaCanvasText *text = DIA_CANVAS_TEXT(object);

  switch (property_id)
    {
    case PROP_X:
      g_value_set_double(value, text->point.x);
      break;
    case PROP_Y:
      g_value_set_double(value, text->point.y);
      break;
    case PROP_TEXT:
      g_value_set_string(value, text->text);
      break;
    case PROP_TEXT_COLOR:
      g_value_set_boxed(value, &text->text_color);
      break;
    case PROP_TEXT_WIDTH:
      g_value_set_double(value, text->text_width);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
dia_canvas_text_real_render (DiaCanvasItem *item, DiaRenderer *renderer,
			     DiaRectangle *rect)
{
  DiaPoint        point;
  DiaCanvasText *text = DIA_CANVAS_TEXT(item);

  dia_renderer_get_text_size(renderer, &point, text->text);

  item->bounds.left   = text->point.x;
  item->bounds.top    = text->point.y;
  item->bounds.right  = text->point.x + point.x;
  item->bounds.bottom = text->point.y + point.y;

  dia_renderer_set_color(renderer, &text->text_color);
  dia_renderer_set_line_width(renderer, text->text_width);
  dia_renderer_draw_text(renderer, &text->point, text->text);
}

static void
dia_canvas_text_real_update (DiaCanvasItem *item)
{
  DiaPoint        point;
  DiaCanvasText  *text = DIA_CANVAS_TEXT(item);

  /* the text may have been moved, so dirty its former location */
  dia_canvas_dirty_region(item->canvas, &item->bounds);

  item->bounds.left   = text->point.x;
  item->bounds.top    = text->point.y;
  item->bounds.right  = text->point.x+1;
  item->bounds.bottom = text->point.y+1;

  if( (item->canvas->focus_view != NULL) &&
      (DIA_IS_RENDERER(item->canvas->focus_view)) ) {

    dia_renderer_get_text_size(DIA_RENDERER(item->canvas->focus_view),
			       &point, text->text);

    item->bounds.left   = text->point.x;
    item->bounds.top    = text->point.y;
    item->bounds.right  = text->point.x + point.x;
    item->bounds.bottom = text->point.y + point.y;
  }

  /* dirty the new location to redraw ... */
  dia_canvas_dirty_region(item->canvas, &item->bounds);
}

static gdouble
dia_canvas_text_real_distance (DiaCanvasItem *item, gdouble x,gdouble y)
{
  //DiaCanvasText *text = DIA_CANVAS_TEXT(item);
  double dx = 0.0, dy = 0.0;

  /* not the exact distance, but close enough for our checks */
  if (x < item->bounds.left)
    dx = item->bounds.left - x;
  else if (x > item->bounds.right)
    dx = x - item->bounds.right;

  if (y < item->bounds.top)
    dy = item->bounds.top - y;
  else if (y > item->bounds.bottom)
    dy = y - item->bounds.bottom;

  return dx*dx + dy*dy;
}

static void
dia_canvas_text_real_move (DiaCanvasItem *item,
			   gdouble dx, gdouble dy)
{
  DiaCanvasText *text = DIA_CANVAS_TEXT(item);

  text->point.x += dx;
  text->point.y += dy;

  dia_canvas_item_request_update(DIA_CANVAS_ITEM(text));
}

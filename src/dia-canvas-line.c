/* dia-canvas-line.c
 * Copyright (C) 2001 Xavier Ordoquy
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "dia-canvas-line.h"
#include "dia-canvas-group.h"

#define _(s) (s)
#define sqr(x) ((x)*(x))

/* properties */
enum {
  PROP_NONE,
  PROP_X1,
  PROP_Y1,
  PROP_X2,
  PROP_Y2,
  PROP_LINE_COLOR,
  PROP_LINE_WIDTH,
  PROP_LAST
};

static void dia_canvas_line_init         (DiaCanvasLine *line);
static void dia_canvas_line_class_init   (DiaCanvasLineClass *klass);

static void   dia_canvas_line_set_property (GObject *object,
					    guint property_id,
					    const GValue *value,
					    GParamSpec *pspec);
static void   dia_canvas_line_get_property (GObject *object,
					    guint property_id,
					    GValue *value,
					    GParamSpec *pspec);

static void    dia_canvas_line_real_render    (DiaCanvasItem *item,
					       DiaRenderer *renderer,
					       DiaRectangle *rect);
static gdouble dia_canvas_line_real_distance  (DiaCanvasItem *item,
					       gdouble x, gdouble y);
static void    dia_canvas_line_real_update    (DiaCanvasItem *item);
static void    dia_canvas_line_real_move      (DiaCanvasItem *item,
					       gdouble dx, gdouble dy);

static GObjectClass *parent_class = NULL;

GType
dia_canvas_line_get_type (void)
{
  static GtkType canvas_line_type = 0;

  if (!canvas_line_type)
    {
      static const GTypeInfo canvas_line_info =
      {
        sizeof (DiaCanvasLineClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) dia_canvas_line_class_init,
        (GClassFinalizeFunc) NULL,
        NULL,
        
        sizeof (DiaCanvasLine),
        0, /* n_preallocs */
        (GInstanceInitFunc) dia_canvas_line_init,
      };

      canvas_line_type = g_type_register_static (DIA_TYPE_CANVAS_ITEM,
						      "DiaCanvasLine",
						      &canvas_line_info,
						      0);
    }
  return canvas_line_type;
}

static void
dia_canvas_line_class_init (DiaCanvasLineClass *klass)
{
  GObjectClass *object_class;
  DiaCanvasItemClass *item_class;

  object_class = G_OBJECT_CLASS(klass);
  item_class   = DIA_CANVAS_ITEM_CLASS(klass);
  parent_class = g_type_class_ref(DIA_TYPE_CANVAS_ITEM);

  object_class->set_property = dia_canvas_line_set_property;
  object_class->get_property = dia_canvas_line_get_property;

  item_class->render     = dia_canvas_line_real_render;
  item_class->update     = dia_canvas_line_real_update;
  item_class->distance   = dia_canvas_line_real_distance;
  item_class->move       = dia_canvas_line_real_move;

  g_object_class_install_property (object_class,
				   PROP_X1,
				   g_param_spec_double ("x1", _("First edge"),
							_("The position of the first edge of the line"),
							-G_MAXDOUBLE,
							G_MAXDOUBLE,
							0.0,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_Y1,
				   g_param_spec_double ("y1", _("First edge"),
							_("The position of the first edge of the line"),
							-G_MAXDOUBLE,
							G_MAXDOUBLE,
							0,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_X2,
				   g_param_spec_double ("x2", _("Second edge"),
							_("The position of the second edge of the line"),
							-G_MAXDOUBLE,
							G_MAXDOUBLE,
							0,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_Y2,
				   g_param_spec_double ("y2", _("Second edge"),
							_("The position of the second edge of the line"),
							-G_MAXDOUBLE,
							G_MAXDOUBLE,
							0,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_LINE_COLOR,
				   g_param_spec_boxed ("line_color", _("Line color"),
						       _("The line color of the line"),
						       GDK_TYPE_COLOR,
						       G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_LINE_WIDTH,
				   g_param_spec_double ("line_width", _("Line width"),
							_("The thickness of line"),
							0, G_MAXDOUBLE, 0,
							G_PARAM_READWRITE));
}

static void
dia_canvas_line_init (DiaCanvasLine *line)
{
  GdkColor red   = { 0, 0xffff, 0, 0 };

  line->line_color = red;
  line->line_width = 1;
}

static void
dia_canvas_line_set_property (GObject *object, guint property_id,
			      const GValue *value, GParamSpec *pspec)
{
  DiaCanvasLine *line = DIA_CANVAS_LINE(object);
  gdouble val;
  GdkColor *color;

  switch (property_id)
    {
    case PROP_X1:
      val = g_value_get_double(value);
      if (val != line->point1.x) {
	line->point1.x = val;
	dia_canvas_item_request_update(DIA_CANVAS_ITEM(line));
      }
      break;
    case PROP_Y1:
      val = g_value_get_double(value);
      if (val != line->point1.y) {
	line->point1.y = val;
	dia_canvas_item_request_update(DIA_CANVAS_ITEM(line));
      }
      break;
    case PROP_X2:
      val = g_value_get_double(value);
      if (val != line->point2.x) {
	line->point2.x = val;
	dia_canvas_item_request_update(DIA_CANVAS_ITEM(line));
      }
      break;
    case PROP_Y2:
      val = g_value_get_double(value);
      if (val != line->point2.y) {
	line->point2.y = val;
	dia_canvas_item_request_update(DIA_CANVAS_ITEM(line));
      }
      break;
    case PROP_LINE_COLOR:
      color = g_value_get_boxed(value);
      line->line_color = *color;
      dia_canvas_item_request_update(DIA_CANVAS_ITEM(line));
      break;
    case PROP_LINE_WIDTH:
      val = g_value_get_double(value);
      line->line_width = val;
      dia_canvas_item_request_update(DIA_CANVAS_ITEM(line));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
dia_canvas_line_get_property (GObject *object, guint property_id,
				   GValue *value, GParamSpec *pspec)
{
  DiaCanvasLine *line = DIA_CANVAS_LINE(object);

  switch (property_id)
    {
    case PROP_X1:
      g_value_set_double(value, line->point1.x);
      break;
    case PROP_Y1:
      g_value_set_double(value, line->point1.y);
      break;
    case PROP_X2:
      g_value_set_double(value, line->point2.x);
      break;
    case PROP_Y2:
      g_value_set_double(value, line->point2.y);
      break;
    case PROP_LINE_COLOR:
      g_value_set_boxed(value, &line->line_color);
      break;
    case PROP_LINE_WIDTH:
      g_value_set_double(value, line->line_width);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
dia_canvas_line_real_render (DiaCanvasItem *item, DiaRenderer *renderer,
			     DiaRectangle *rect)
{
  DiaCanvasLine *line = DIA_CANVAS_LINE(item);

  dia_renderer_set_color(renderer, &line->line_color);
  dia_renderer_set_line_width(renderer, line->line_width);
  dia_renderer_draw_line(renderer, &line->point1, &line->point2);
}

static void
dia_canvas_line_real_update (DiaCanvasItem *item)
{
  DiaCanvasLine *line = DIA_CANVAS_LINE(item);
  gdouble half_width, tmp;

  /* the line may have been moved, so dirty its former location */
  dia_canvas_dirty_region(item->canvas, &item->bounds);

  item->bounds.left   = line->point1.x;
  item->bounds.top    = line->point1.y;
  item->bounds.right  = line->point2.x;
  item->bounds.bottom = line->point2.y;

  if( item->bounds.left > item->bounds.right ) {
    tmp = item->bounds.left;
    item->bounds.left = item->bounds.right;
    item->bounds.right = tmp;
  }
  if( item->bounds.top > item->bounds.bottom ) {
    tmp = item->bounds.top;
    item->bounds.top = item->bounds.bottom;
    item->bounds.bottom = tmp;
  }
    
  half_width = line->line_width / 2;

  item->bounds.left   -= half_width;
  item->bounds.top    -= half_width;
  item->bounds.right  += half_width;
  item->bounds.bottom += half_width;

  /* dirty the new location to redraw ... */
  dia_canvas_dirty_region(item->canvas, &item->bounds);
}

static gdouble
dia_canvas_line_real_distance (DiaCanvasItem *item, gdouble x,gdouble y)
{
  DiaCanvasLine *line = DIA_CANVAS_LINE(item);
  gdouble d1,d2,d3;
  gdouble scal;

  /* not the exact distance, but close enough for our checks */
  d1 = sqr(item->bounds.left-x)  + sqr(item->bounds.top-y);
  d2 = sqr(item->bounds.right-x) + sqr(item->bounds.bottom-y);

  scal = ((item->bounds.right - item->bounds.left)*(x - item->bounds.left)) +
    ((item->bounds.bottom - item->bounds.top)*(y - item->bounds.top));

  scal = scal/(sqr(item->bounds.right - item->bounds.left)+sqr(item->bounds.bottom - item->bounds.top));

  d3 = sqr(scal*(item->bounds.right - item->bounds.left)+item->bounds.left-x) +
    sqr(scal*(item->bounds.bottom - item->bounds.top)+item->bounds.top-y);
  scal = sqr((line->line_width)/2.0);

  d1 -= scal;
  d2 -= scal;
  d3 -= scal;
  if( d1 < 0 )
    d1 = 0;
  if( d2 < 0 )
    d2 = 0;
  if( d3 < 0 )
    d3 = 0;

  if( d1 < d2 ) {
    if( d1 < d3 ) {
      return d1;
    } else {
      return d3;
    }
  } else {
    if( d2 < d3 ) {
      return d2;
    } else {
      return d3;
    }
  }
}

static void
dia_canvas_line_real_move (DiaCanvasItem *item,
			   gdouble dx, gdouble dy)
{
  DiaCanvasLine *line = DIA_CANVAS_LINE(item);

  line->point1.x += dx;
  line->point1.y += dy;
  line->point2.x += dx;
  line->point2.y += dy;

  dia_canvas_item_request_update(DIA_CANVAS_ITEM(line));
}

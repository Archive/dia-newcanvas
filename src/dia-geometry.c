/* dia-renderer.c
 * Copyright (C) 2000  James Henstridge, Alexander Larsson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "dia-geometry.h"
#define sqr(x) ((x)*(x))
#include <math.h>

/**
 * dia_rectangle_intersect:
 * @src1: the first #DiaRectangle.
 * @src2: the second #DiaRectangle.
 * @dest: the intersection.
 *
 * Make @dest the intersection between @src1 and @src2. If there's
 * no intersection, return FALSE.
 *
 * Return value: TRUE if there is an intersection.
 **/
gboolean
dia_rectangle_intersect (DiaRectangle *src1,
			 DiaRectangle *src2,
			 DiaRectangle *dest)
{
  DiaRectangle *temp;
  gboolean return_val;

  g_return_val_if_fail (src1 != NULL, FALSE);
  g_return_val_if_fail (src2 != NULL, FALSE);
  g_return_val_if_fail (dest != NULL, FALSE);

  return_val = FALSE;

  if (src2->left < src1->left)
    {
      temp = src1;
      src1 = src2;
      src2 = temp;
    }
  dest->left = src2->left;

  if (src2->left < src1->right)
    {
      if (src1->right < src2->right)
        dest->right = src1->right;
      else
        dest->right = src2->right;

      if (src2->top < src1->top)
        {
          temp = src1;
          src1 = src2;
          src2 = temp;
        }
      dest->top = src2->top;

      if (src2->top < src1->bottom)
        {
          return_val = TRUE;

          if (src1->bottom < src2->bottom)
            dest->bottom = src1->bottom;
          else
            dest->bottom = src2->bottom;

          if (dest->top == dest->bottom)
            return_val = FALSE;
          if (dest->left == dest->right)
            return_val = FALSE;
        }
    }

  return return_val;
}

gdouble
distance_line_point(const DiaPoint *line_start, const DiaPoint *line_end,
		    gdouble line_width, const DiaPoint *point)
{
  DiaPoint v1, v2;
  gdouble v1_lensq;
  gdouble projlen;
  gdouble perp_dist;

  v1.x = line_end->x - line_start->x;
  v1.y = line_end->y - line_start->y;

  v2.x = point->x - line_start->x;
  v2.y = point->y - line_start->y;

  v1_lensq = sqr(v1.x) + sqr(v1.y);

  if (v1_lensq<0.000001) {
    return sqrt(sqr(v2.x) + sqr(v2.y));
  }

  projlen = (v1.x*v2.x + v1.y*v2.y) / v1_lensq;
  
  if (projlen<0.0) {
    return sqrt(sqr(v2.x) + sqr(v2.y));
  }
  
  if (projlen>1.0) {
    v2.x = point->x - line_end->x;
    v2.y = point->y - line_end->y;

    return sqrt(sqr(v2.x)+sqr(v2.y));
  }


  v1.x *= projlen;
  v1.y *= projlen;

  v1.x -= v2.x;
  v1.y -= v2.y;

  perp_dist = sqrt(sqr(v1.x)+sqr(v1.y));

  perp_dist -= line_width / 2.0;
  if (perp_dist < 0.0) {
    perp_dist = 0.0;
  }
  
  return perp_dist;
}

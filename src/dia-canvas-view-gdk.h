/* dia-canvas-view-gdk.h
 * Copyright (C) 2000  James Henstridge.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __DIA_CANVAS_VIEW_GDK_H__
#define __DIA_CANVAS_VIEW_GDK_H__

#include <gtk/gtk.h>
#include "dia-canvas-view.h"

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define DIA_TYPE_CANVAS_VIEW_GDK			(dia_canvas_view_gdk_get_type ())
#define DIA_CANVAS_VIEW_GDK(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), DIA_TYPE_CANVAS_VIEW_GDK, DiaCanvasViewGdk))
#define DIA_CANVAS_VIEW_GDK_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST ((klass), DIA_TYPE_CANVAS_VIEW_GDK, DiaCanvasViewGdkClass))
#define DIA_IS_CANVAS_VIEW_GDK(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), DIA_TYPE_CANVAS_VIEW_GDK))
#define DIA_IS_CANVAS_VIEW_GDK_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((obj), DIA_TYPE_CANVAS_VIEW_GDK))
#define DIA_CANVAS_VIEW_GDK_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), DIA_TYPE_CANVAS_VIEW_GDK, DiaCanvasViewGdkClass))

typedef struct _DiaCanvasViewGdk       DiaCanvasViewGdk;
typedef struct _DiaCanvasViewGdkClass  DiaCanvasViewGdkClass;

struct _DiaCanvasViewGdk
{
  DiaCanvasView parent;

  GdkGC *gc;
  /* saved values for the GC -- to convert sets to noops when no
   * change is made */
  GdkFillRule fill_rule;
  GdkJoinStyle join_style;
  GdkCapStyle cap_style;
  int line_width;
  GdkLineStyle line_style;
  gdouble *dashes;
  int ndashes;

  GdkColor color;

  /* handle font stuff as well */
};
struct _DiaCanvasViewGdkClass
{
  DiaCanvasViewClass parent_class;
};


GType    dia_canvas_view_gdk_get_type (void);
GtkWidget *dia_canvas_view_gdk_new      (DiaCanvas *canvas);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __DIA_CANVAS_VIEW_GDK_H__ */


#include <gtk/gtk.h>
#include "dia-canvas.h"
#include "dia-canvas-view.h"
#include "dia-canvas-view-gdk.h"
#include "dia-canvas-rectangle.h"
#include "dia-canvas-ellipse.h"
#include "dia-canvas-arc.h"
#include "dia-canvas-text.h"
#include "dia-canvas-polygon.h"
#include "dia-geometry.h"

static gint
event(DiaCanvasItem *item, GdkEvent *event, DiaCanvasView *view)
{
  GdkColor red   = { 0, 0xffff, 0, 0 };
  GdkColor green = { 0, 0, 0xffff, 0 };
  GdkColor blue  = { 0, 0, 0, 0xffff };
  static gboolean dragging = FALSE;
  static DiaPoint DaPoint;
  DiaPoint newPoint;

  switch (event->type) {
  case GDK_BUTTON_PRESS:
    g_message("button press %p", item);
    if( DIA_IS_CANVAS_TEXT(item) ) {
      g_object_set(item,
		   "text_color", &red,
		   NULL);
    } else {
      g_object_set(item,
		   "line_color", &red,
		   NULL);
    }
    DaPoint.x = event->button.x;
    DaPoint.y = event->button.y;
    dragging = TRUE;
    dia_canvas_view_grab(view,TRUE);
    return TRUE;

  case GDK_BUTTON_RELEASE:
    g_message("button release %p", item);
    if( DIA_IS_CANVAS_TEXT(item) ) {
      g_object_set(item,
		   "text_color", &green,
		   NULL);
    } else {
      g_object_set(item,
		   "line_color", &green,
		   NULL);
    }
    dragging = FALSE;
    dia_canvas_view_ungrab(view);
    return TRUE;

  case GDK_MOTION_NOTIFY:
    if (dragging && (event->motion.state & GDK_BUTTON1_MASK)) {
      newPoint.x = event->button.x - DaPoint.x;
      newPoint.y = event->button.y - DaPoint.y;
      dia_canvas_item_snapmove (item, &(newPoint.x), &(newPoint.y));
      dia_canvas_item_move (item, newPoint.x, newPoint.y);
      DaPoint.x += newPoint.x;
      DaPoint.y += newPoint.y;
    }
    return TRUE;

  case GDK_ENTER_NOTIFY:
    g_message("enter item %p", item);
    if( DIA_IS_CANVAS_TEXT(item) ) {
      g_object_set(item,
		   "text_color", &blue,
		   NULL);
    } else {
      g_object_set(item,
		   "line_color", &blue,
		   NULL);
    }
    return TRUE;

  case GDK_LEAVE_NOTIFY:
    g_message("exit item %p", item);
    if( DIA_IS_CANVAS_TEXT(item) ) {
      g_object_set(item,
		   "text_color", &green,
		   NULL);
    } else {
      g_object_set(item,
		   "line_color", &green,
		   NULL);
    }
    return TRUE;

  default:
    return FALSE;
  }
  return FALSE;
}

static void create_view(DiaCanvas *canvas);

int
main(int argc, char **argv)
{
  DiaCanvas *canvas;
  DiaCanvasItem *item;
  DiaRectangle extents = { 0,0, 200,200 };
  //GdkColor red   = { 0, 0xffff, 0, 0 };
  GdkColor green = { 0, 0, 0xffff, 0 };
  GdkColor cyan  = { 0, 0, 0xffff, 0xffff };

  gtk_init(&argc, &argv);

  canvas = dia_canvas_new();
  dia_canvas_set_extents(canvas, &extents);

  item = g_object_new(DIA_TYPE_CANVAS_RECTANGLE,
		      "parent", canvas->root,
		      "x1", 10.0, "y1", 10.0,
		      "x2", 130.0, "y2", 130.0,
		      "fill_color", &cyan,
		      "line_color", &green,
		      "line_width", 8.0,
		      NULL);
  g_signal_connect(item, "event", G_CALLBACK(event), NULL);

  item = g_object_new(DIA_TYPE_CANVAS_ELLIPSE,
		      "parent", canvas->root,
		      "x1", 100.0, "y1", 60.0,
		      "x2", 180.0, "y2", 180.0,
		      "fill_color", &cyan,
		      "line_color", &green,
		      "line_width", 8.0,
		      NULL);
  g_signal_connect(item, "event", G_CALLBACK(event), NULL);

  item = g_object_new(DIA_TYPE_CANVAS_ARC,
		      "parent", canvas->root,
		      "x", 130.0, "y", 150.0,
		      "width", 120.0, "height", 100.0,
		      "angle1", 45.0, "angle2", 170.0,
		      "fill_color", &cyan,
		      "line_color", &green,
		      "line_width", 8.0,
		      NULL);
  g_signal_connect(item, "event", G_CALLBACK(event), NULL);

  item = g_object_new(DIA_TYPE_CANVAS_POLYGON,
		      "parent", canvas->root,
		      "x", 50.0, "y", 50.0,
		      "point", "(25.0,25.0)",
		      "point", "(0.0,50.0)",
		      "point", "(-25.0,25.0)",
		      "point", "(-25.0,-25.0)",
		      "point", "(25.0,-25.0)",
		      "fill_color", &cyan,
		      "line_color", &green,
		      "line_width", 8.0,
		      NULL);
  g_signal_connect(item, "event", G_CALLBACK(event), NULL);

  item = g_object_new(DIA_TYPE_CANVAS_TEXT,
		      "parent", canvas->root,
		      "x", 50.0, "y", 50.0,
		      "text_color", &green,
		      "text_width", 8.0,
		      "text","TOTO",
		      NULL);
  g_signal_connect(item, "event", G_CALLBACK(event), NULL);

  create_view(canvas);

  g_object_unref(G_OBJECT(canvas));

  gtk_main();

  return 0;
}

/* code for creating a new view of the canvas */

static gint view_count = 0;

static void
view_destroy(GtkWidget *widget)
{
  view_count--;
  if (view_count == 0)
    gtk_main_quit();
}

static void
rezoom(GtkAdjustment *adj, DiaCanvasView *view)
{
  dia_canvas_view_set_zoom(view, adj->value);
}

static void
new_view_callback(GtkWidget *button, DiaCanvas *canvas)
{
  create_view(canvas);
}

static void
create_view(DiaCanvas *canvas)
{
  GtkWidget *window;
  GtkWidget *vbox, *hbox;
  GtkAdjustment *adj;
  GtkWidget *label, *zoom, *button;
  GtkWidget *swin;
  GtkWidget *view;

  view_count++;

  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_container_set_border_width(GTK_CONTAINER(window), 2);
  gtk_window_set_title(GTK_WINDOW(window), "Canvas Test");
  gtk_window_set_default_size(GTK_WINDOW(window), 200, 230);
  g_signal_connect(window, "destroy", G_CALLBACK(view_destroy), NULL);

  vbox = gtk_vbox_new(FALSE, 2);
  gtk_container_add(GTK_CONTAINER(window), vbox);

  hbox = gtk_hbox_new(FALSE, 2);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, TRUE, 0);

  label = gtk_label_new("Zoom:");
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);

  adj = GTK_ADJUSTMENT(gtk_adjustment_new(1.0, 0.0675,256, 0.25,1.0,1.0));
  zoom = gtk_spin_button_new(adj, 0.0, 4);
  gtk_box_pack_start(GTK_BOX(hbox), zoom, FALSE, FALSE, 0);

  button = gtk_button_new_with_label("New view");
  g_signal_connect(button, "clicked", G_CALLBACK(new_view_callback), canvas);
  gtk_box_pack_start(GTK_BOX(hbox), button, FALSE, FALSE, 0);

  swin = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(swin),
				 GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_box_pack_start(GTK_BOX(vbox), swin, TRUE, TRUE, 0);

  view = dia_canvas_view_gdk_new(canvas);
  gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(swin), view);

  g_signal_connect(adj, "value_changed", G_CALLBACK(rezoom), view);

  gtk_widget_show_all(window);
}

/* dia-canvas-connection.c
 * Copyright (C) 2001 Xavier Ordoquy
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "dia-canvas-connection.h"
//#include "dia-canvas.h"
#include "dia-canvas-group.h"

#define _(s) (s)

/* properties */
enum {
  PROP_NONE,
  PROP_X,
  PROP_Y,
  PROP_FILL_COLOR,
  PROP_TYPE,
  PROP_LAST
};

static void dia_canvas_connection_init       (DiaCanvasConnection *connection);
static void dia_canvas_connection_class_init (DiaCanvasConnectionClass *klass);
static void dia_canvas_connection_finalize   (GObject *object);

static void dia_canvas_connection_set_property     (GObject *object,
						    guint property_id,
						    const GValue *value,
						    GParamSpec *pspec);
static void     dia_canvas_connection_get_property (GObject *object,
						    guint property_id,
						    GValue *value,
						    GParamSpec *pspec);

static void     dia_canvas_connection_real_render    (DiaCanvasItem *item,
						      DiaRenderer *renderer,
						      DiaRectangle *rectangle);
static void     dia_canvas_connection_real_update    (DiaCanvasItem *item);
static gdouble  dia_canvas_connection_real_distance  (DiaCanvasItem *item,
						      gdouble x, gdouble y);
static void     dia_canvas_connection_real_move      (DiaCanvasItem *item,
						      gdouble dx, gdouble dy);
static void    dia_canvas_connection_real_connect    (DiaCanvasItem *item,
						      DiaCanvasConnection *connection);
static void    dia_canvas_connection_real_connection (DiaCanvasItem *item);

static GObjectClass *parent_class = NULL;


GType
dia_canvas_connection_get_type (void)
{
  static GtkType canvas_connection_type = 0;

  if (!canvas_connection_type)
    {
      static const GTypeInfo canvas_connection_info =
      {
        sizeof (DiaCanvasConnectionClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) dia_canvas_connection_class_init,
        (GClassFinalizeFunc) NULL,
        NULL,
        
        sizeof (DiaCanvasConnection),
        0, /* n_preallocs */
        (GInstanceInitFunc) dia_canvas_connection_init,
      };

      canvas_connection_type = g_type_register_static (DIA_TYPE_CANVAS_ITEM,
						       "DiaCanvasConnection",
						       &canvas_connection_info,
						       0);
    }
  return canvas_connection_type;
}

static void
dia_canvas_connection_class_init (DiaCanvasConnectionClass *klass)
{
  GObjectClass *object_class;
  DiaCanvasItemClass *item_class;

  object_class = G_OBJECT_CLASS(klass);
  item_class = DIA_CANVAS_ITEM_CLASS(klass);
  parent_class = g_type_class_ref(DIA_TYPE_CANVAS_ITEM);

  object_class->set_property = dia_canvas_connection_set_property;
  object_class->get_property = dia_canvas_connection_get_property;
  object_class->finalize     = dia_canvas_connection_finalize;

  item_class->render           = dia_canvas_connection_real_render;
  item_class->update           = dia_canvas_connection_real_update;
  item_class->distance         = dia_canvas_connection_real_distance;
  item_class->move             = dia_canvas_connection_real_move;
  item_class->connect          = dia_canvas_connection_real_connect;
  item_class->connection       = dia_canvas_connection_real_connection;

  g_object_class_install_property (object_class,
				   PROP_X,
				   g_param_spec_double ("x", _("Position"),
							_("The position of the connection point"),
							-G_MAXDOUBLE,
							G_MAXDOUBLE,
							0.0,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_Y,
				   g_param_spec_double ("y", _("Position"),
							_("The position of the connection point"),
							-G_MAXDOUBLE,
							G_MAXDOUBLE,
							0,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_FILL_COLOR,
				   g_param_spec_boxed ("fill_color", _("Fill color"),
						       _("The fill color of the connection (or NULL for empty)"),
						       GDK_TYPE_COLOR,
						       G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_TYPE,
				   g_param_spec_int ("type", _("Type"),
						     _("Type of the connection point"),
						     -G_MAXINT,
						     G_MAXINT,
						     0,
						     G_PARAM_READWRITE));
}

static void
dia_canvas_connection_init (DiaCanvasConnection *connection)
{
  DiaCanvasItem *item = DIA_CANVAS_ITEM(connection);

  item->independant_move  = FALSE;
  connection->connections = NULL;
  connection->type        = UNKNOWN;
}

static void
dia_canvas_connection_finalize(GObject *object)
{
  DiaCanvasConnection *connection;
  DiaCanvasItem *item;
  GList *tmp, *next;

  g_return_if_fail(object != NULL);
  g_return_if_fail(DIA_IS_CANVAS_CONNECTION(object));

  connection = DIA_CANVAS_CONNECTION(object);
  item = DIA_CANVAS_ITEM(object);

  tmp = connection->connections;
  while( tmp != NULL ) {
    next = tmp->next;
    ((DiaCanvasConnection*)tmp->data)->connections = g_list_remove(((DiaCanvasConnection*)tmp->data)->connections,connection);
    connection->connections = g_list_remove(connection->connections,((DiaCanvasConnection*)tmp->data));
    tmp = next;
  }

  if (parent_class->finalize)
    (* parent_class->finalize) (object);
}

static void
dia_canvas_connection_set_property (GObject *object, guint property_id,
				    const GValue *value, GParamSpec *pspec)
{
  DiaCanvasConnection *connection = DIA_CANVAS_CONNECTION(object);
  gdouble val;
  GdkColor *color;

  switch (property_id)
    {

    case PROP_X:
      val = g_value_get_double(value);
      if (val != connection->pos.x) {
	connection->pos.x = val;
	dia_canvas_item_request_update(DIA_CANVAS_ITEM(connection));
      }
      break;

    case PROP_Y:
      val = g_value_get_double(value);
      if (val != connection->pos.y) {
	connection->pos.y = val;
	dia_canvas_item_request_update(DIA_CANVAS_ITEM(connection));
      }
      break;

    case PROP_FILL_COLOR:
      color = g_value_get_boxed(value);
      connection->fill_color = *color;
      dia_canvas_item_request_update(DIA_CANVAS_ITEM(connection));
      break;

    case PROP_TYPE:
      connection->type = g_value_get_int(value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
dia_canvas_connection_get_property (GObject *object, guint property_id,
				    GValue *value, GParamSpec *pspec)
{
  DiaCanvasConnection *connection = DIA_CANVAS_CONNECTION(object);

  switch (property_id)
    {
    case PROP_X:
      g_value_set_double(value, connection->pos.x);
      break;
    case PROP_Y:
      g_value_set_double(value, connection->pos.y);
      break;
    case PROP_FILL_COLOR:
      g_value_set_boxed(value, &connection->fill_color);
      break;
    case PROP_TYPE:
      g_value_set_int(value, connection->type);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

/*****************************************************************************
 ******************************* S I G N A L S *******************************
 *****************************************************************************/

static void
dia_canvas_connection_real_render (DiaCanvasItem *item,
				   DiaRenderer *renderer,
				   DiaRectangle *rectangle)
{
  DiaCanvasConnection *connection = DIA_CANVAS_CONNECTION(item);
  DiaPoint p1,p2;

  p1.x = connection->pos.x - 5;
  p1.y = connection->pos.y - 5;
  p2.x = connection->pos.x + 5;
  p2.y = connection->pos.y + 5;
  dia_renderer_set_color(renderer, &connection->fill_color);
  dia_renderer_draw_rectangle(renderer, TRUE, &p1, &p2);
}

static void
dia_canvas_connection_real_update (DiaCanvasItem *item)
{
  DiaCanvasConnection *connection = DIA_CANVAS_CONNECTION(item);

  /* the connection may have been moved, so dirty its former location */
  dia_canvas_dirty_region(item->canvas, &item->bounds);

  item->bounds.left   = connection->pos.x - 5;
  item->bounds.top    = connection->pos.y - 5;
  item->bounds.right  = connection->pos.x + 5;
  item->bounds.bottom = connection->pos.y + 5;

  /* dirty the new location to redraw ... */
  dia_canvas_dirty_region(item->canvas, &item->bounds);
}

static gdouble
dia_canvas_connection_real_distance (DiaCanvasItem *item,
				     gdouble x,gdouble y)
{
  //DiaCanvasConnection *connection = DIA_CANVAS_CONNECTION(item);
  double dx = 0.0, dy = 0.0;

  /* not the exact distance, but close enough for our checks */
  if (x < item->bounds.left)
    dx = item->bounds.left - x;
  else if (x > item->bounds.right)
    dx = x - item->bounds.right;

  if (y < item->bounds.top)
    dy = item->bounds.top - y;
  else if (y > item->bounds.bottom)
    dy = y - item->bounds.bottom;

  return dx*dx + dy*dy;
}

static void
dia_canvas_connection_real_move (DiaCanvasItem *item,
				 gdouble dx, gdouble dy)
{
  static guint connection_moved_signal_id = 0;
  DiaCanvasConnection *connection = DIA_CANVAS_CONNECTION(item);
  GList *tmp;

  connection->pos.x += dx;
  connection->pos.y += dy;

  dia_canvas_item_request_update(DIA_CANVAS_ITEM(connection));

  if( !item->connection_moved ) {
    if (connection_moved_signal_id == 0)
      connection_moved_signal_id = gtk_signal_lookup("connection_moved", DIA_TYPE_CANVAS_ITEM);

    tmp = connection->connections;
    while( tmp != NULL ) {
      DiaCanvasConnection *save;

      save = tmp->data;
      tmp = tmp->next;
      DIA_CANVAS_ITEM(save)->connection_moved = TRUE;
      g_signal_emit(DIA_CANVAS_ITEM(save), connection_moved_signal_id, 0, connection, dx, dy);
      DIA_CANVAS_ITEM(save)->connection_moved = FALSE;
    }
  }
}

static void
dia_canvas_connection_real_connect (DiaCanvasItem *item,
				    DiaCanvasConnection *connection)
{
  DiaCanvasConnection *this;
  GList *tmp;

  g_return_if_fail( item != NULL );
  g_return_if_fail( connection != NULL );

  if( item == DIA_CANVAS_ITEM(connection) )
    return;

  this = DIA_CANVAS_CONNECTION(item);

  if( (this->pos.x == connection->pos.x) &&
      (this->pos.y == connection->pos.y) ) {

    /*
     * Check that the connection doesn't already exists.
     */
    tmp = this->connections;
    while( tmp != NULL ) {
      if( tmp->data == connection )
	return;
      tmp = tmp->next;
    }

    this->connections       = g_list_append(this->connections,connection);
    connection->connections = g_list_append(connection->connections,this);
  }
}

static void
dia_canvas_connection_real_connection (DiaCanvasItem *item)
{
  g_return_if_fail( item != NULL );

  dia_canvas_connect( item->canvas, DIA_CANVAS_CONNECTION(item) );
}

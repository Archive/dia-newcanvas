
#include <gtk/gtk.h>
#include "dia-canvas.h"
#include "dia-canvas-view.h"
#include "dia-canvas-view-gdk.h"
#include "dia-canvas-line.h"
#include "dia-canvas-rectangle.h"
#include "dia-canvas-ellipse.h"
#include "dia-geometry.h"
#include "dia-canvas-group.h"
#include "dia-canvas-connection.h"
#include "dia-canvas-attract.h"
#include "dia-canvas-attracted.h"

static gint
event(DiaCanvasItem *item, GdkEvent *event, DiaCanvasView *view)
{
  static gboolean dragging = FALSE;
  static DiaPoint DaPoint;
  DiaPoint newPoint;

  switch (event->type) {
  case GDK_BUTTON_PRESS:
    g_message("button press %p", item);
    DaPoint.x = event->button.x;
    DaPoint.y = event->button.y;
    dragging = TRUE;
    dia_canvas_view_grab(view,TRUE);
    return TRUE;

  case GDK_BUTTON_RELEASE:
    g_message("button release %p", item);
    dragging = FALSE;
    dia_canvas_view_ungrab(view);
    dia_canvas_item_make_connection(item);
    return TRUE;

  case GDK_MOTION_NOTIFY:
    if (dragging && (event->motion.state & GDK_BUTTON1_MASK)) {
      newPoint.x = event->button.x - DaPoint.x;
      newPoint.y = event->button.y - DaPoint.y;
      dia_canvas_item_snapmove (item, &(newPoint.x), &(newPoint.y));
      dia_canvas_item_move (item, newPoint.x, newPoint.y);
      DaPoint.x += newPoint.x;
      DaPoint.y += newPoint.y;
    }
    return TRUE;

  case GDK_ENTER_NOTIFY:
    g_message("enter item %p", item);
    return TRUE;

  case GDK_LEAVE_NOTIFY:
    g_message("exit item %p", item);
    return TRUE;

  default:
    return FALSE;
  }
  return FALSE;
}

static void
component_connection_moved (DiaCanvasItem *item,
			    DiaCanvasConnection *connection,
			    gdouble dx, gdouble dy)
{
  DiaCanvasConnection *dest = DIA_CANVAS_CONNECTION(item);

  dest->connections = g_list_remove(dest->connections,connection);
  connection->connections = g_list_remove(connection->connections,dest);
}

static void
line_connection_moved (DiaCanvasItem *item,
		       DiaCanvasConnection *connection,
		       gdouble dx, gdouble dy)
{
  dia_canvas_item_move(item,dx,dy);
}

static void create_view(DiaCanvas *canvas);

int
main(int argc, char **argv)
{
  DiaCanvas      *canvas;
  DiaCanvasItem  *group;
  DiaCanvasItem  *item;
  DiaRectangle extents = { 0,0, 200,200 };
  GdkColor line_color = { 0, 0x8000, 0x8000, 0x8000 };
  GdkColor fill_color = { 0, 0xffff, 0xffff, 0x99ff };

  gtk_init(&argc, &argv);

  canvas = dia_canvas_new();
  dia_canvas_set_extents(canvas, &extents);

  /*
   * The rectangle group.
   */
  group = g_object_new(DIA_TYPE_CANVAS_GROUP,
		       "parent", canvas->root,
		       "move_indep", TRUE,
		       NULL);
  g_signal_connect(group, "event", G_CALLBACK(event), NULL);

  g_object_new(DIA_TYPE_CANVAS_RECTANGLE,
	       "parent", group,
	       "x1", 10.0, "y1", 10.0,
	       "x2", 130.0, "y2", 130.0,
	       "fill_color", &fill_color,
	       "line_color", &line_color,
	       "line_width", 5.0,
	       "move_indep", FALSE,
	       NULL);

  item = g_object_new(DIA_TYPE_CANVAS_CONNECTION,
		      "parent", group,
		      "x", 10.0, "y", 10.0,
		      "move_indep", FALSE,
		      NULL);
  g_signal_connect_after(item, "connection_moved", G_CALLBACK(component_connection_moved), NULL);

  item = g_object_new(DIA_TYPE_CANVAS_CONNECTION,
		      "parent", group,
		      "x", 130.0, "y", 10.0,
		      "move_indep", FALSE,
		      NULL);
  g_signal_connect_after(item, "connection_moved", G_CALLBACK(component_connection_moved), NULL);

  item = g_object_new(DIA_TYPE_CANVAS_CONNECTION,
		      "parent", group,
		      "x", 10.0, "y", 130.0,
		      "move_indep", FALSE,
		      NULL);
  g_signal_connect(item, "connection_moved", G_CALLBACK(component_connection_moved), NULL);

  item = g_object_new(DIA_TYPE_CANVAS_CONNECTION,
		      "parent", group,
		      "x", 130.0, "y", 130.0,
		      "move_indep", FALSE,
		      NULL);
  g_signal_connect(item, "connection_moved", G_CALLBACK(component_connection_moved), NULL);

  g_object_new(DIA_TYPE_CANVAS_ATTRACT,
	       "parent", group,
	       "x", 10.0, "y", 10.0,
	       "move_indep", FALSE,
	       NULL);

  g_object_new(DIA_TYPE_CANVAS_ATTRACT,
	       "parent", group,
	       "x", 130.0, "y", 10.0,
	       "move_indep", FALSE,
	       NULL);

  g_object_new(DIA_TYPE_CANVAS_ATTRACT,
	       "parent", group,
	       "x", 10.0, "y", 130.0,
	       "move_indep", FALSE,
	       NULL);

  g_object_new(DIA_TYPE_CANVAS_ATTRACT,
	       "parent", group,
	       "x", 130.0, "y", 130.0,
	       "move_indep", FALSE,
	       NULL);

  g_object_new(DIA_TYPE_CANVAS_ATTRACTED,
	       "parent", group,
	       "x", 10.0, "y", 10.0,
	       "move_indep", FALSE,
	       NULL);

  g_object_new(DIA_TYPE_CANVAS_ATTRACTED,
	       "parent", group,
	       "x", 130.0, "y", 10.0,
	       "move_indep", FALSE,
	       NULL);

  g_object_new(DIA_TYPE_CANVAS_ATTRACTED,
	       "parent", group,
	       "x", 10.0, "y", 130.0,
	       "move_indep", FALSE,
	       NULL);

  g_object_new(DIA_TYPE_CANVAS_ATTRACTED,
	       "parent", group,
	       "x", 130.0, "y", 130.0,
	       "move_indep", FALSE,
	       NULL);

  /*
   * The line group.
   */
  group = g_object_new(DIA_TYPE_CANVAS_GROUP,
		       "parent", canvas->root,
		       "move_indep", TRUE,
		       NULL);
  g_signal_connect(group, "event", G_CALLBACK(event), NULL);
  g_signal_connect(group, "connection_moved", G_CALLBACK(line_connection_moved), NULL);

  g_object_new(DIA_TYPE_CANVAS_LINE,
	       "parent", group,
	       "x1", 130.0, "y1", 130.0,
	       "x2", 220.0, "y2", 220.0,
	       "line_color", &line_color,
	       "line_width", 8.0,
	       "move_indep", FALSE,
	       NULL);

  item = g_object_new(DIA_TYPE_CANVAS_CONNECTION,
		      "parent", group,
		      "x", 130.0, "y", 130.0,
		      "move_indep", FALSE,
		      NULL);

  item = g_object_new(DIA_TYPE_CANVAS_CONNECTION,
		      "parent", group,
		      "x", 220.0, "y", 220.0,
		      "move_indep", FALSE,
		      NULL);

  g_object_new(DIA_TYPE_CANVAS_ATTRACT,
	       "parent", group,
	       "x", 130.0, "y", 130.0,
	       "move_indep", FALSE,
	       NULL);

  g_object_new(DIA_TYPE_CANVAS_ATTRACT,
	       "parent", group,
	       "x", 220.0, "y", 220.0,
	       "move_indep", FALSE,
	       NULL);

  g_object_new(DIA_TYPE_CANVAS_ATTRACTED,
	       "parent", group,
	       "x", 130.0, "y", 130.0,
	       "move_indep", FALSE,
	       NULL);

  g_object_new(DIA_TYPE_CANVAS_ATTRACTED,
	       "parent", group,
	       "x", 220.0, "y", 220.0,
	       "move_indep", FALSE,
	       NULL);

  create_view(canvas);

  g_object_unref(G_OBJECT(canvas));

  gtk_main();

  return 0;
}

/* code for creating a new view of the canvas */

static gint view_count = 0;

static void
view_destroy(GtkWidget *widget)
{
  view_count--;
  if (view_count == 0)
    gtk_main_quit();
}

static void
rezoom(GtkAdjustment *adj, DiaCanvasView *view)
{
  dia_canvas_view_set_zoom(view, adj->value);
}

static void
new_view_callback(GtkWidget *button, DiaCanvas *canvas)
{
  create_view(canvas);
}

static void
create_view(DiaCanvas *canvas)
{
  GtkWidget *window;
  GtkWidget *vbox, *hbox;
  GtkAdjustment *adj;
  GtkWidget *label, *zoom, *button;
  GtkWidget *swin;
  GtkWidget *view;

  view_count++;

  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_container_set_border_width(GTK_CONTAINER(window), 2);
  gtk_window_set_title(GTK_WINDOW(window), "Canvas Test");
  gtk_window_set_default_size(GTK_WINDOW(window), 200, 230);
  g_signal_connect(window, "destroy", G_CALLBACK(view_destroy), NULL);

  vbox = gtk_vbox_new(FALSE, 2);
  gtk_container_add(GTK_CONTAINER(window), vbox);

  hbox = gtk_hbox_new(FALSE, 2);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, TRUE, 0);

  label = gtk_label_new("Zoom:");
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);

  adj = GTK_ADJUSTMENT(gtk_adjustment_new(1.0, 0.0675,256, 0.25,1.0,1.0));
  zoom = gtk_spin_button_new(adj, 0.0, 4);
  gtk_box_pack_start(GTK_BOX(hbox), zoom, FALSE, FALSE, 0);

  button = gtk_button_new_with_label("New view");
  g_signal_connect(button, "clicked", G_CALLBACK(new_view_callback), canvas);
  gtk_box_pack_start(GTK_BOX(hbox), button, FALSE, FALSE, 0);

  swin = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(swin),
				 GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_box_pack_start(GTK_BOX(vbox), swin, TRUE, TRUE, 0);

  view = dia_canvas_view_gdk_new(canvas);
  gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(swin), view);

  g_signal_connect(adj, "value_changed", G_CALLBACK(rezoom), view);

  gtk_widget_show_all(window);
}

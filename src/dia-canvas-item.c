/* dia-canvas-item.c
 * Copyright (C) 2000  James Henstridge
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gtk/gtk.h>
#include "dia-canvas-item.h"
#include "dia-canvas-group.h"
#include "dia-canvas-view.h"
#include "dia-canvas-connection.h"
#include "dia-marshal.h"

#define _(s) (s)

/* signals */
enum {
  RENDER,
  DISTANCE,
  UPDATE,
  EVENT,
  MOVE,
  ATTRACT,
  SNAPMOVE,
  CONNECT,
  CONNECTION,
  CONNECTION_MOVED,
  SELECT,
  LAST_SIGNAL
};

/* properties */
enum {
  PROP_NONE,
  PROP_PARENT,
  PROP_MOVE_INDEP,
  PROP_VISIBLE
};

static void dia_canvas_item_init	(DiaCanvasItem	    *item);
static void dia_canvas_item_class_init	(DiaCanvasItemClass *klass);
static void dia_canvas_item_finalize    (GObject *object);

static void dia_canvas_item_set_property (GObject *object,
					  guint property_id,
					  const GValue *value,
					  GParamSpec *pspec);
static void dia_canvas_item_get_property (GObject *object,
					  guint property_id,
					  GValue *value,
					  GParamSpec *pspec);
static void dia_canvas_item_real_render	(DiaCanvasItem *item,
					 DiaRenderer *renderer,
					 DiaRectangle *rect);

static gdouble dia_canvas_item_real_distance   (DiaCanvasItem *item,
						gdouble x, gdouble y);

static void    dia_canvas_item_real_move       (DiaCanvasItem *item,
						gdouble dx, gdouble dy);
static gdouble dia_canvas_item_real_attract    (DiaCanvasItem *item,
						gdouble *dx, gdouble *dy);
static gdouble dia_canvas_item_real_snapmove   (DiaCanvasItem *item,
						gdouble *dx, gdouble *dy);
static void    dia_canvas_item_real_connect    (DiaCanvasItem *item,
						DiaCanvasConnection *connection);
static void    dia_canvas_item_real_connection (DiaCanvasItem *item);
static void    dia_canvas_item_real_connection_moved (DiaCanvasItem *item,
						      DiaCanvasConnection *connection,
						      gdouble dx, gdouble dy);

static void    dia_canvas_item_real_select (DiaCanvasItem *item, gboolean selected);

static GObjectClass *parent_class = NULL;
static guint canvas_item_signals[LAST_SIGNAL] = { 0 };


GType
dia_canvas_item_get_type (void)
{
  static GtkType canvas_item_type = 0;

  if (!canvas_item_type)
    {
      static const GTypeInfo canvas_item_info =
      {
	sizeof (DiaCanvasItemClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) dia_canvas_item_class_init,
	(GClassFinalizeFunc) NULL,
	NULL,
	
	sizeof (DiaCanvasItem),
	0, /* n_preallocs */
	(GInstanceInitFunc) dia_canvas_item_init,
      };

      canvas_item_type = g_type_register_static (G_TYPE_OBJECT,
						 "DiaCanvasItem",
						 &canvas_item_info, 0);
    }

  return canvas_item_type;
}

static void
dia_canvas_item_class_init (DiaCanvasItemClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS(klass);
  parent_class = g_type_class_ref (G_TYPE_OBJECT);

  object_class->set_property = dia_canvas_item_set_property;
  object_class->get_property = dia_canvas_item_get_property;
  object_class->finalize     = dia_canvas_item_finalize;

  klass->render           = dia_canvas_item_real_render;
  klass->distance         = dia_canvas_item_real_distance;
  klass->move             = dia_canvas_item_real_move;
  klass->attract          = dia_canvas_item_real_attract;
  klass->snapmove         = dia_canvas_item_real_snapmove;
  klass->connect          = dia_canvas_item_real_connect;
  klass->connection       = dia_canvas_item_real_connection;
  klass->connection_moved = dia_canvas_item_real_connection_moved;
  klass->select           = dia_canvas_item_real_select;

  canvas_item_signals[RENDER] = 
    g_signal_new ("render",
		   G_OBJECT_CLASS_TYPE(klass),
		   G_SIGNAL_RUN_FIRST,
		   G_STRUCT_OFFSET (DiaCanvasItemClass, render), NULL, NULL,
		   dia_marshal_VOID__OBJECT_POINTER,
		   G_TYPE_NONE, 2,
		   DIA_TYPE_RENDERER, G_TYPE_POINTER /* rectangle */);
  canvas_item_signals[DISTANCE] =
    g_signal_new ("distance",
		   G_OBJECT_CLASS_TYPE(klass),
		   G_SIGNAL_RUN_LAST,
		   G_STRUCT_OFFSET (DiaCanvasItemClass, distance), NULL, NULL,
		   dia_marshal_DOUBLE__DOUBLE_DOUBLE,
		   G_TYPE_DOUBLE, 2,
		   G_TYPE_DOUBLE, G_TYPE_DOUBLE);
  canvas_item_signals[UPDATE] =
    g_signal_new ("update",
		   G_OBJECT_CLASS_TYPE(klass),
		   G_SIGNAL_RUN_FIRST,
		   G_STRUCT_OFFSET(DiaCanvasItemClass, update), NULL, NULL,
		   gtk_marshal_VOID__VOID,
		   G_TYPE_NONE, 0);
  canvas_item_signals[EVENT] =
    g_signal_new ("event",
		   G_OBJECT_CLASS_TYPE(klass),
		   G_SIGNAL_RUN_LAST,
		   G_STRUCT_OFFSET(DiaCanvasItemClass, event), NULL, NULL,
		   dia_marshal_BOOLEAN__BOXED_OBJECT,
		   G_TYPE_BOOLEAN, 2,
		   GDK_TYPE_EVENT, DIA_TYPE_CANVAS_VIEW);

  canvas_item_signals[MOVE] =
    g_signal_new ("move",
		   G_OBJECT_CLASS_TYPE(klass),
		   G_SIGNAL_RUN_LAST,
		   G_STRUCT_OFFSET (DiaCanvasItemClass, move), NULL, NULL,
		   dia_marshal_VOID__DOUBLE_DOUBLE,
		   G_TYPE_NONE, 2,
		   G_TYPE_DOUBLE, G_TYPE_DOUBLE);

  canvas_item_signals[ATTRACT] =
    g_signal_new ("attract",
		   G_OBJECT_CLASS_TYPE(klass),
		   G_SIGNAL_RUN_LAST,
		   G_STRUCT_OFFSET (DiaCanvasItemClass, attract), NULL, NULL,
		   dia_marshal_DOUBLE__POINTER_POINTER,
		   G_TYPE_DOUBLE, 2,
		   G_TYPE_POINTER, G_TYPE_POINTER);

  canvas_item_signals[SNAPMOVE] =
    g_signal_new ("snapmove",
		   G_OBJECT_CLASS_TYPE(klass),
		   G_SIGNAL_RUN_LAST,
		   G_STRUCT_OFFSET (DiaCanvasItemClass, snapmove), NULL, NULL,
		   dia_marshal_DOUBLE__POINTER_POINTER,
		   G_TYPE_DOUBLE, 2,
		   G_TYPE_POINTER, G_TYPE_POINTER);

  canvas_item_signals[CONNECT] = 
    g_signal_new ("connect",
		   G_OBJECT_CLASS_TYPE(klass),
		   G_SIGNAL_RUN_LAST,
		   G_STRUCT_OFFSET (DiaCanvasItemClass, connect), NULL, NULL,
		   dia_marshal_VOID__OBJECT,
		   G_TYPE_NONE, 1,
		   DIA_TYPE_CANVAS_CONNECTION);

  canvas_item_signals[CONNECTION] = 
    g_signal_new ("connection",
		   G_OBJECT_CLASS_TYPE(klass),
		   G_SIGNAL_RUN_LAST,
		   G_STRUCT_OFFSET (DiaCanvasItemClass, connection), NULL, NULL,
		   dia_marshal_VOID__VOID,
		   G_TYPE_NONE, 0);

  canvas_item_signals[CONNECTION_MOVED] = 
    g_signal_new ("connection_moved",
		   G_OBJECT_CLASS_TYPE(klass),
		   G_SIGNAL_RUN_LAST,
		   G_STRUCT_OFFSET (DiaCanvasItemClass, connection_moved), NULL, NULL,
		   dia_marshal_VOID__OBJECT_DOUBLE_DOUBLE,
		   G_TYPE_NONE, 3,
		   DIA_TYPE_CANVAS_CONNECTION, G_TYPE_DOUBLE, G_TYPE_DOUBLE);

  canvas_item_signals[SELECT] = 
    g_signal_new ("select",
		   G_OBJECT_CLASS_TYPE(klass),
		   G_SIGNAL_RUN_LAST,
		   G_STRUCT_OFFSET (DiaCanvasItemClass, select), NULL, NULL,
		   dia_marshal_VOID__BOOLEAN,
		   G_TYPE_NONE, 1,
		   G_TYPE_BOOLEAN);

  g_object_class_install_property (object_class,
				   PROP_PARENT,
				   g_param_spec_object ("parent", _("Parent item"),
							_("The parent group of this canvas item"),
							DIA_TYPE_CANVAS_GROUP,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_VISIBLE,
				   g_param_spec_boolean ("visible", _("Visible"),
							 _("Whether the canvas item is visible"),
							 TRUE,
							 G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_MOVE_INDEP,
				   g_param_spec_boolean ("move_indep", _("Independant move"),
							 _("Whether the item moves does not move the all group"),
							 TRUE,
							 G_PARAM_READWRITE));
}


static void
dia_canvas_item_init (DiaCanvasItem *item)
{
  item->canvas           = NULL;
  item->parent           = NULL;
  item->floating         = TRUE;
  item->visible          = TRUE;
  item->independant_move = TRUE;
  item->attract_source   = FALSE;
  item->connect_source   = FALSE;
  item->connection_moved = FALSE;
  item->selected         = FALSE;
}

static void
dia_canvas_item_finalize(GObject *object)
{
  DiaCanvasItem *item;

  g_return_if_fail(object != NULL);
  g_return_if_fail(DIA_IS_CANVAS_ITEM(object));

  item = DIA_CANVAS_ITEM(object);

  if( (item->canvas != NULL) &&
      (item == item->canvas->focus_item) ) {
    item->canvas->focus_item = NULL;
  }

  if( (item->canvas != NULL) &&
      (item->canvas->focus_view != NULL) &&
      (item == item->canvas->focus_view->event_item) ) {
    item->canvas->focus_view->event_item = NULL;
  }

  if( item->canvas != NULL ) {
    dia_canvas_dirty_region(item->canvas, &item->bounds);
  }

  if (item->parent != NULL) {
    dia_canvas_group_remove_item(item->parent,item);
  }

  /* TODO: remove the item of the selection if it is selected */

  if (parent_class->finalize)
    (* parent_class->finalize) (object);
}

static void
dia_canvas_item_set_property (GObject *object, guint property_id,
			      const GValue *value, GParamSpec *pspec)
{
  DiaCanvasItem *item = DIA_CANVAS_ITEM(object);
  DiaCanvasGroup *parent;

  switch (property_id)
    {
    case PROP_PARENT:
      /* this won't destroy item, as g_object_set_prop refs the object */
      if (item->parent != NULL)
	dia_canvas_group_remove_item(item->parent, item);
      
      parent = DIA_CANVAS_GROUP(g_value_get_object(value));
      if (parent)
	dia_canvas_group_add_item(parent, item);
      break;
    case PROP_VISIBLE:
      if (g_value_get_boolean(value))
	dia_canvas_item_show(item);
      else
	dia_canvas_item_hide(item);
      break;
    case PROP_MOVE_INDEP:
      if (g_value_get_boolean(value))
	item->independant_move = TRUE;
      else
	item->independant_move = FALSE;
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
dia_canvas_item_get_property (GObject *object, guint property_id,
			      GValue *value, GParamSpec *pspec)
{
  DiaCanvasItem *item = DIA_CANVAS_ITEM(object);

  switch (property_id)
    {
    case PROP_PARENT:
      if (item->parent)
	g_value_set_object(value, G_OBJECT(item->parent));
      else
	g_value_set_object(value, NULL);
      break;
    case PROP_VISIBLE:
      g_value_set_boolean(value, item->visible);
      break;
    case PROP_MOVE_INDEP:
       g_value_set_boolean(value, item->independant_move);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

/*****************************************************************************
 ******************************* S I G N A L S *******************************
 *****************************************************************************/

static void
dia_canvas_item_real_render (DiaCanvasItem *item,
			     DiaRenderer *renderer, DiaRectangle *rect)
{
}

static gdouble
dia_canvas_item_real_distance (DiaCanvasItem *item,
			       gdouble x, gdouble y)
{
  return G_MAXDOUBLE;
}

static void
dia_canvas_item_real_move (DiaCanvasItem *item,
			   gdouble dx, gdouble dy)
{
}

static gdouble
dia_canvas_item_real_attract (DiaCanvasItem *item,
 			      gdouble *x, gdouble *y)
{
  return G_MAXDOUBLE;
}

static gdouble
dia_canvas_item_real_snapmove (DiaCanvasItem *item,
			       gdouble *x, gdouble *y)
{
  return G_MAXDOUBLE;
}

static void
dia_canvas_item_real_connect (DiaCanvasItem *item,
			      DiaCanvasConnection *connection)
{
}

static void
dia_canvas_item_real_connection (DiaCanvasItem *item)
{
}

static void
dia_canvas_item_real_connection_moved (DiaCanvasItem *item,
				       DiaCanvasConnection *connection,
				       gdouble dx, gdouble dy)
{
  static guint connection_moved_signal_id = 0;

  if (connection_moved_signal_id == 0)
    connection_moved_signal_id = gtk_signal_lookup("connection_moved", DIA_TYPE_CANVAS_ITEM);

  g_signal_emit(item->parent, connection_moved_signal_id, 0, connection, dx, dy);
}

static void
dia_canvas_item_real_select (DiaCanvasItem *item, gboolean selected)
{
  item->selected = selected;
}

/*****************************************************************************
 ***************************** F O N C T I O N S *****************************
 *****************************************************************************/

/**
 * dia_canvas_item_request_update:
 * @item: a #DiaCanvasItem.
 *
 * ??
 **/
void
dia_canvas_item_request_update(DiaCanvasItem *item)
{
  g_return_if_fail(item != NULL);
  g_return_if_fail(DIA_IS_CANVAS_ITEM(item));

  if (item->canvas == NULL) {
    /* an update will be scheduled when the item is actually added to
     * the canvas */
    return;
  }

  if (item->canvas->in_update) {
    g_warning("recursive update requested.  not good");
    return;
  }

  if (item->need_update)
    return;

  item->need_update = TRUE;

  /* propagate update up tree ... */
  if (item->parent)
    dia_canvas_item_request_update(DIA_CANVAS_ITEM(item->parent));
  else  /* top of tree ... */
    dia_canvas_schedule_update(item->canvas);
}

/**
 * dia_canvas_item_sink:
 * @item: a #DiaCanvasItem.
 *
 * ??
 **/
void
dia_canvas_item_sink (DiaCanvasItem *item)
{
  g_return_if_fail(item != NULL);
  g_return_if_fail(DIA_IS_CANVAS_ITEM(item));

  if (item->floating) {
    g_object_unref(G_OBJECT(item));
  }
  item->floating = FALSE;
}

/**
 * dia_canvas_item_show:
 * @item: a #DiaCanvasItem.
 *
 * Shows the @item.
 **/
void
dia_canvas_item_show (DiaCanvasItem *item)
{
  g_return_if_fail(item != NULL);
  g_return_if_fail(DIA_IS_CANVAS_ITEM(item));

  if (item->visible)
    return;

  item->visible = TRUE;
  g_object_notify(G_OBJECT(item), "visible");
  if (item->canvas != NULL)
    dia_canvas_dirty_region(item->canvas, &item->bounds);
}

/**
 * dia_canvas_item_hide:
 * @item: a #DiaCanvasItem.
 *
 * Hide the @item.
 **/
void
dia_canvas_item_hide (DiaCanvasItem *item)
{
  g_return_if_fail(item != NULL);
  g_return_if_fail(DIA_IS_CANVAS_ITEM(item));

  if (!item->visible)
    return;

  item->visible = FALSE;
  g_object_notify(G_OBJECT(item), "visible");
  if (item->canvas != NULL)
    dia_canvas_dirty_region(item->canvas, &item->bounds);
}

/**
 * dia_canvas_item_move:
 * @item: a #DiaCanvasItem.
 * @dx: the x difference.
 * @dy: the y difference.
 *
 * Moves the @item from (@dx,@dy).
 * Does the snapping if requiered.
 **/
void
dia_canvas_item_move (DiaCanvasItem *item,
		      gdouble dx, gdouble dy)
{
  DiaCanvasItem *moved;
  static guint event_signal_id = 0;

  g_return_if_fail(item != NULL);

  if (event_signal_id == 0)
    event_signal_id = gtk_signal_lookup("move", DIA_TYPE_CANVAS_ITEM);
  moved = item;
  while( !moved->independant_move ) {
    moved = DIA_CANVAS_ITEM(moved->parent);
  }
  g_signal_emit(moved, event_signal_id, 0, dx, dy);
}

/**
 * dia_canvas_item_snapmove:
 * @item: a #DiaCanvasItem.
 * @dx: the x difference.
 * @dy: the y difference.
 *
 * Gets the best (x,y) from the
 * snap of the item BUT DO NOT MOVE.
 *
 * Returns: distance.
 **/
gdouble
dia_canvas_item_snapmove (DiaCanvasItem *item,
			  gdouble *dx, gdouble *dy)
{
  gdouble result;
  DiaCanvasItem *moved;
  static guint event_signal_id = 0;

  g_return_val_if_fail( item != NULL, G_MAXDOUBLE );
  g_return_val_if_fail(   dx != NULL, G_MAXDOUBLE );
  g_return_val_if_fail(   dy != NULL, G_MAXDOUBLE );

  if (event_signal_id == 0)
    event_signal_id = gtk_signal_lookup("snapmove", DIA_TYPE_CANVAS_ITEM);

  /*
   * Get the topmost item not moving independantly.
   */
  moved = item;
  while( !moved->independant_move ) {
    moved = DIA_CANVAS_ITEM(moved->parent);
  }
  g_signal_emit(moved, event_signal_id, 0, dx, dy, &result);

  return result;
}

/**
 * dia_canvas_item_make_connection:
 * @item: a #DiaCanvasItem.
 *
 * Test if connection are possible and do them.
 **/
void
dia_canvas_item_make_connection (DiaCanvasItem *item)
{
  DiaCanvasItem *connect;
  static guint connection_signal_id = 0;

  g_return_if_fail( item != NULL );

  if (connection_signal_id == 0)
    connection_signal_id = gtk_signal_lookup("connection", DIA_TYPE_CANVAS_ITEM);

  connect = item;
  while( !connect->independant_move ) {
    connect = DIA_CANVAS_ITEM(connect->parent);
  }
  g_signal_emit(connect, connection_signal_id, 0);
}

void
dia_canvas_item_select (DiaCanvasItem *item)
{
  static guint item_select_signal_id = 0;

  if (item_select_signal_id == 0)
    item_select_signal_id = gtk_signal_lookup("select", DIA_TYPE_CANVAS_ITEM);

  g_signal_emit(item, item_select_signal_id, 0, TRUE);
}

void
dia_canvas_item_unselect (DiaCanvasItem *item)
{
  static guint item_select_signal_id = 0;

  if (item_select_signal_id == 0)
    item_select_signal_id = gtk_signal_lookup("select", DIA_TYPE_CANVAS_ITEM);

  g_signal_emit(item, item_select_signal_id, 0, FALSE);
}

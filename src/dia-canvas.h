/* dia-canvas.h
 * Copyright (C) 2000  James Henstridge
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __DIA_CANVAS_H__
#define __DIA_CANVAS_H__

#include <gtk/gtk.h>
#include "dia-geometry.h"
#include "dia-renderer.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define DIA_TYPE_CANVAS			(dia_canvas_get_type ())
#define DIA_CANVAS(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), DIA_TYPE_CANVAS, DiaCanvas))
#define DIA_CANVAS_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST ((klass), DIA_TYPE_CANVAS, DiaCanvasClass))
#define DIA_IS_CANVAS(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), DIA_TYPE_CANVAS))
#define DIA_IS_CANVAS_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), DIA_TYPE_CANVAS))
#define DIA_CANVAS_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), DIA_TYPE_CANVAS, DiaCanvasClass))

typedef struct _DiaCanvas       DiaCanvas;
typedef struct _DiaCanvasClass  DiaCanvasClass;

/* forward declarations ... */
typedef struct _DiaCanvasItem       DiaCanvasItem;
typedef struct _DiaCanvasConnection DiaCanvasConnection;
typedef struct _DiaCanvasGroup      DiaCanvasGroup;
typedef struct _DiaCanvasView       DiaCanvasView;


struct _DiaCanvas
{
  GObject parent;

  /* the size of the canvas */
  DiaRectangle extents;

  /* the root item in the canvas -- is a group */
  DiaCanvasItem *root;

  /* this is a group of selected items */
  GList *selection;


  /* views of the canvas */
  GList *views;

  guint update_id;
  gboolean in_update; /* flag used to catch recursive updates */

  /* current focused item (where keyboard input goes) */
  DiaCanvasItem *focus_item;
  /* the current view that holds the focus (if any) */
  DiaCanvasView *focus_view;
};

struct _DiaCanvasClass
{
  GObjectClass parent_class;

  /* Signals go here */
  void (*render)	  (DiaCanvas *canvas, DiaRenderer *renderer,
			   DiaRectangle *rect);
  void (*dirty_region)	  (DiaCanvas *canvas, DiaRectangle *rect);
  void (*extents_changed) (DiaCanvas *canvas);
};


GType    dia_canvas_get_type (void);
DiaCanvas *dia_canvas_new      (void);

void dia_canvas_set_extents(DiaCanvas *canvas, DiaRectangle *extents);
void dia_canvas_render (DiaCanvas *canvas, DiaRenderer *renderer,
			DiaRectangle *rect);

/* for use by canvas views */
void dia_canvas_add_view(DiaCanvas *canvas, DiaCanvasView *view);
void dia_canvas_remove_view(DiaCanvas *canvas, DiaCanvasView *view);

DiaCanvasItem *dia_canvas_get_item_at(DiaCanvas *canvas, gdouble x, gdouble y,
				      gdouble close_enough);

/* for use by canvas items */
void dia_canvas_schedule_update(DiaCanvas *canvas);
void dia_canvas_ensure_update(DiaCanvas *canvas);
void dia_canvas_dirty_region(DiaCanvas *canvas, DiaRectangle *rect);
gdouble dia_canvas_snap(DiaCanvas *canvas, gdouble *x, gdouble *y);
void dia_canvas_connect(DiaCanvas *canvas, DiaCanvasConnection *connection);

void dia_canvas_add_selection(DiaCanvas *canvas, DiaCanvasItem *item);
void dia_canvas_remove_from_selection(DiaCanvas *canvas, DiaCanvasItem *item);
void dia_canvas_clear_selection(DiaCanvas *canvas);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __DIA_CANVAS_H__ */

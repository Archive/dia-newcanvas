/* dia-newcanvas.h
 * Copyright (C) 2001  Xavier Ordoquy
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <dia-newcanvas/dia-canvas-arc.h>
#include <dia-newcanvas/dia-canvas-attract.h>
#include <dia-newcanvas/dia-canvas-ellipse.h>
#include <dia-newcanvas/dia-canvas-item.h>
#include <dia-newcanvas/dia-canvas-view-aa.h>
#include <dia-newcanvas/dia-canvas.h>
#include <dia-newcanvas/dia-canvas-attracted.h>
#include <dia-newcanvas/dia-canvas-grid.h>
#include <dia-newcanvas/dia-canvas-line.h>
#include <dia-newcanvas/dia-canvas-view-gdk.h>
#include <dia-newcanvas/dia-geometry.h>
#include <dia-newcanvas/dia-renderer.h>
#include <dia-newcanvas/dia-canvas-connection.h>
#include <dia-newcanvas/dia-canvas-group.h>
#include <dia-newcanvas/dia-canvas-rectangle.h>
#include <dia-newcanvas/dia-canvas-view.h>
#include <dia-newcanvas/dia-marshal.h>
#include <dia-newcanvas/dia-canvas-point.h>
#include <dia-newcanvas/dia-canvas-polygon.h>
#include <dia-newcanvas/dia-canvas-text.h>

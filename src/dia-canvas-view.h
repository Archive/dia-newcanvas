/* dia-canvas-view.h
 * Copyright (C) 2000  James Henstridge
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __DIA_CANVAS_VIEW_H__
#define __DIA_CANVAS_VIEW_H__

#include <gtk/gtk.h>

#include "dia-canvas.h"
#include "dia-renderer.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define DIA_TYPE_CANVAS_VIEW			(dia_canvas_view_get_type ())
#define DIA_CANVAS_VIEW(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), DIA_TYPE_CANVAS_VIEW, DiaCanvasView))
#define DIA_CANVAS_VIEW_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST ((klass), DIA_TYPE_CANVAS_VIEW, DiaCanvasViewClass))
#define DIA_IS_CANVAS_VIEW(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), DIA_TYPE_CANVAS_VIEW))
#define DIA_IS_CANVAS_VIEW_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((obj), DIA_TYPE_CANVAS_VIEW))
#define DIA_CANVAS_VIEW_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), DIA_TYPE_CANVAS_VIEW, DiaCanvasViewClass))

/* declaration in dia-canvas.h */
/* typedef struct _DiaCanvasView       DiaCanvasView; */
typedef struct _DiaCanvasViewClass  DiaCanvasViewClass;

struct _DiaCanvasView
{
  GtkWidget widget; /* consider changing this to GtkFixed to do widget items */

  DiaCanvas *canvas;

  gdouble zoom_factor;

  /* button/motion event related members */
  DiaCanvasItem *event_item;
  DiaCanvasItem *point_item;
  GdkModifierType button_mask;
};

struct _DiaCanvasViewClass
{
  GtkWidgetClass parent_class;

  /* max size of update regions for this view type */
  guint image_width, image_height;

  /* Signals go here */
  void (*render_rect)	(DiaCanvasView *canvas_view, GdkRectangle *grect,
			 DiaRectangle *crect);
  void (*set_zoom)      (DiaCanvasView *canvas_view, gdouble zoom);
};


GType dia_canvas_view_get_type (void);

/* set the zoom factor for canvas (1.0 == 1 pixel/canvas unit) */
void  dia_canvas_view_set_zoom(DiaCanvasView *view, gdouble zoom);

/* intended for canvas view subclasses.  Users of the canvas shouldn't call
 * this */
void  dia_canvas_view_set_canvas(DiaCanvasView *view, DiaCanvas *canvas);

/* this grabs the current selection */
void  dia_canvas_view_grab(DiaCanvasView *view, gboolean exclusive);
/* this ungrabs the current selection */
void  dia_canvas_view_ungrab(DiaCanvasView *view);

void dia_canvas_view_real_to_canvas( DiaCanvasView *view, gdouble *x, gdouble *y );

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __DIA_CANVAS_VIEW_H__ */

/* dia-renderer.c
 * Copyright (C) 2000  James Henstridge
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "dia-renderer.h"

GType
dia_renderer_get_type (void)
{
  static GType renderer_type = 0;

  if (!renderer_type)
    {
      static const GTypeInfo renderer_info =
      {
	sizeof (DiaRendererClass),  /* class_size */
	NULL,                       /* base_init */
	NULL,                       /* base_finalize */
      };

      renderer_type = g_type_register_static (G_TYPE_INTERFACE, "DiaRenderer",
					      &renderer_info, 0);
      g_type_interface_add_prerequisite (renderer_type, G_TYPE_OBJECT);
    }

  return renderer_type;
}

void
dia_renderer_set_join_style (DiaRenderer *renderer, GdkJoinStyle style)
{
  g_return_if_fail (DIA_IS_RENDERER(renderer));

  DIA_RENDERER_GET_CLASS(renderer)->set_join_style (renderer, style);
}

void
dia_renderer_set_cap_style (DiaRenderer *renderer, GdkCapStyle style)
{
  g_return_if_fail (DIA_IS_RENDERER(renderer));

  DIA_RENDERER_GET_CLASS(renderer)->set_cap_style (renderer, style);
}

void
dia_renderer_set_line_width (DiaRenderer *renderer, gdouble width)
{
  g_return_if_fail (DIA_IS_RENDERER(renderer));

  DIA_RENDERER_GET_CLASS(renderer)->set_line_width (renderer, width);
}

void
dia_renderer_set_line_style (DiaRenderer *renderer, GdkLineStyle style)
{
  g_return_if_fail (DIA_IS_RENDERER(renderer));

  DIA_RENDERER_GET_CLASS(renderer)->set_line_style (renderer, style);
}

void
dia_renderer_set_dash_style (DiaRenderer *renderer, gdouble *dash_len,
			     gint nlen)
{
  g_return_if_fail (DIA_IS_RENDERER(renderer));

  DIA_RENDERER_GET_CLASS(renderer)->set_dash_style (renderer, dash_len, nlen);
}

void
dia_renderer_set_color (DiaRenderer *renderer, GdkColor *color)
{
  g_return_if_fail (DIA_IS_RENDERER(renderer));

  DIA_RENDERER_GET_CLASS(renderer)->set_color (renderer, color);
}

void
dia_renderer_set_font (DiaRenderer *renderer, const gchar *font_desc,
		       gdouble size)
{
  g_return_if_fail (DIA_IS_RENDERER(renderer));

  DIA_RENDERER_GET_CLASS(renderer)->set_font (renderer, font_desc, size);
}

/* Primitives */
void
dia_renderer_draw_point (DiaRenderer *renderer, DiaPoint *point)
{
  g_return_if_fail (DIA_IS_RENDERER(renderer));

  DIA_RENDERER_GET_CLASS(renderer)->draw_point (renderer, point);
}

void
dia_renderer_draw_line (DiaRenderer *renderer, DiaPoint *start, DiaPoint *end)
{
  g_return_if_fail (DIA_IS_RENDERER(renderer));

  DIA_RENDERER_GET_CLASS(renderer)->draw_line (renderer, start, end);
}

void
dia_renderer_draw_rectangle (DiaRenderer *renderer, gboolean filled,
			     DiaPoint *ul_corner, DiaPoint *lr_corner)
{
  g_return_if_fail (DIA_IS_RENDERER(renderer));

  DIA_RENDERER_GET_CLASS(renderer)->draw_rectangle (renderer, filled,
						    ul_corner, lr_corner);
}

void
dia_renderer_draw_polyline (DiaRenderer *renderer,
			    DiaPoint *points, gint npoints)
{
  g_return_if_fail (DIA_IS_RENDERER(renderer));

  DIA_RENDERER_GET_CLASS(renderer)->draw_polyline (renderer, points, npoints);
}

void
dia_renderer_draw_polygon (DiaRenderer *renderer, gboolean filled,
			   GList /*DiaPoint*/ *points)
{
  g_return_if_fail (DIA_IS_RENDERER(renderer));

  DIA_RENDERER_GET_CLASS(renderer)->draw_polygon (renderer, filled, points);
}

void
dia_renderer_draw_ellipse (DiaRenderer *renderer, gboolean filled,
			   DiaPoint *centre, gdouble width, gdouble height)
{
  g_return_if_fail (DIA_IS_RENDERER(renderer));

  DIA_RENDERER_GET_CLASS(renderer)->draw_ellipse (renderer, filled, centre,
						  width, height);
}
			   
void
dia_renderer_draw_arc (DiaRenderer *renderer, gboolean filled,
		       DiaPoint *centre,
		       gdouble width, gdouble height,
		       gdouble angle1, gdouble angle2)
{
  g_return_if_fail (DIA_IS_RENDERER(renderer));

  DIA_RENDERER_GET_CLASS(renderer)->draw_arc (renderer, filled, centre,
					      width, height, angle1, angle2);
}

void
dia_renderer_draw_bezier (DiaRenderer *renderer, gboolean filled,
			  DiaBezierPath *points, gint npoints)
{
  g_return_if_fail (DIA_IS_RENDERER(renderer));

  DIA_RENDERER_GET_CLASS(renderer)->draw_bezier (renderer, filled,
						 points, npoints);
}

void
dia_renderer_draw_pixbuf (DiaRenderer *renderer,
			  DiaPoint *pos, gdouble width, gdouble height,
			  gdouble angle, GdkPixbuf *src)
{
  g_return_if_fail (DIA_IS_RENDERER(renderer));

  DIA_RENDERER_GET_CLASS(renderer)->draw_pixbuf (renderer, pos, width,
						 height, angle, src);
}

void
dia_renderer_draw_text (DiaRenderer *renderer,
			DiaPoint *point, const gchar *text)
{
  g_return_if_fail (DIA_IS_RENDERER(renderer));

  DIA_RENDERER_GET_CLASS(renderer)->draw_text (renderer, point, text);
}

void
dia_renderer_get_text_size (DiaRenderer *renderer,
			    DiaPoint *point, const gchar *text)
{
  g_return_if_fail (DIA_IS_RENDERER(renderer));

  DIA_RENDERER_GET_CLASS(renderer)->get_text_size (renderer, point, text);
}


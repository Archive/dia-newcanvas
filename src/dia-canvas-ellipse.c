/* dia-canvas-ellipse.c
 * Copyright (C) 2000  James Henstridge
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <math.h>
#include "dia-canvas-ellipse.h"

static void dia_canvas_ellipse_init         (DiaCanvasEllipse *rect);
static void dia_canvas_ellipse_class_init   (DiaCanvasEllipseClass *klass);

GType
dia_canvas_ellipse_get_type (void)
{
  static GtkType canvas_ellipse_type = 0;
  
  if (!canvas_ellipse_type)
    {
      static const GTypeInfo canvas_ellipse_info =
      {
	sizeof (DiaCanvasEllipseClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) dia_canvas_ellipse_class_init,
	(GClassFinalizeFunc) NULL,
	NULL,

	sizeof (DiaCanvasEllipse),
	0, /* n_preallocs */
	(GInstanceInitFunc) dia_canvas_ellipse_init,
      };

      canvas_ellipse_type = g_type_register_static (DIA_TYPE_CANVAS_RECTANGLE,
						    "DiaCanvasEllipse",
						    &canvas_ellipse_info,
						    0);
    }
  return canvas_ellipse_type;
}

static void   dia_canvas_ellipse_render   (DiaCanvasItem *item,
					   DiaRenderer *renderer,
					   DiaRectangle *rectangle);
static double dia_canvas_ellipse_distance (DiaCanvasItem *item,
					   gdouble x, gdouble y);

static void
dia_canvas_ellipse_class_init (DiaCanvasEllipseClass *klass)
{
  DiaCanvasItemClass *item_class;

  item_class = DIA_CANVAS_ITEM_CLASS(klass);
  item_class->render   = dia_canvas_ellipse_render;
  item_class->distance = dia_canvas_ellipse_distance;
}

static void
dia_canvas_ellipse_init (DiaCanvasEllipse *ellipse)
{

}

static void
dia_canvas_ellipse_render (DiaCanvasItem *item, DiaRenderer *renderer,
			   DiaRectangle *rectangle)
{
  DiaCanvasRectangle *rect = DIA_CANVAS_RECTANGLE(item);
  DiaPoint centre;
  gdouble width, height;

  centre.x = (rect->topleft.x + rect->bottomright.x) / 2;
  centre.y = (rect->topleft.y + rect->bottomright.y) / 2;
  width  = rect->bottomright.x - rect->topleft.x;
  height = rect->bottomright.y - rect->topleft.y;

  if (rect->fill) {
    dia_renderer_set_color(renderer, &rect->fill_color);
    dia_renderer_draw_ellipse(renderer, TRUE, &centre, width, height);
  }
  if (rect->line) {
    dia_renderer_set_color(renderer, &rect->line_color);
    dia_renderer_set_line_width(renderer, rect->line_width);
    dia_renderer_draw_ellipse(renderer, FALSE, &centre, width, height);
  }
}

static double
dia_canvas_ellipse_distance (DiaCanvasItem *item, gdouble x,gdouble y)
{
  DiaCanvasRectangle *rect = DIA_CANVAS_RECTANGLE(item);
  gdouble cx, cy, width, height;
  /* computed values */
  gdouble w2, h2, scale, rad, dist, px, py, half_width;

  cx = (rect->topleft.x + rect->bottomright.x) / 2;
  cy = (rect->topleft.y + rect->bottomright.y) / 2;
  width  = rect->bottomright.x - rect->topleft.x;
  height = rect->bottomright.y - rect->topleft.y;
  w2 = width*width;
  h2 = height*height;
  half_width = rect->line ? rect->line_width / 2 : 0.0;

  /* find the point of intersection between line (x=cx+(px-cx)t; y=cy+(py-cy)t)
   * and ellipse ((x-cx)^2)/(w/2)^2 + ((y-cy)^2)/(h/2)^2 = 1 */
  /* radius along ray is sqrt((px-cx)^2 * t^2 + (py-cy)^2 * t^2) */

  px = x - cx; px *= px;
  py = y - cy; py *= py;
  scale = w2 * h2 / (4*h2*px + 4*w2*py);

  rad = sqrt((px + py) * scale);
  dist = sqrt(px + py);

  if (dist > rad + half_width)
    return dist - (rad + half_width);

  /* if the ellipse is not filled, then check for distance from edge */
  if (!rect->fill) {
    if (dist < rad - half_width)
      return (rad - half_width) - dist;
  }

  /* the point is in the ellipse. */
  return 0.0;
}

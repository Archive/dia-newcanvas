/* dia-canvas-line.h
 * Copyright (C) 2001  Xavier Ordoquy
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __DIA_CANVAS_LINE_H__
#define __DIA_CANVAS_LINE_H__

#include <gtk/gtk.h>
#include "dia-canvas-item.h"

#define DIA_TYPE_CANVAS_LINE		(dia_canvas_line_get_type ())
#define DIA_CANVAS_LINE(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), DIA_TYPE_CANVAS_LINE, DiaCanvasLine))
#define DIA_CANVAS_LINE_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), DIA_TYPE_CANVAS_LINE, DiaCanvasLineClass))
#define DIA_IS_CANVAS_LINE(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), DIA_TYPE_CANVAS_LINE))
#define DIA_IS_CANVAS_LINE_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((obj), DIA_TYPE_CANVAS_LINE))
#define DIA_CANVAS_LINE_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), DIA_TYPE_CANVAS_LINE, DiaCanvasLineClass))

typedef struct _DiaCanvasLine DiaCanvasLine;
typedef struct _DiaCanvasLineClass DiaCanvasLineClass;

struct _DiaCanvasLine
{
  DiaCanvasItem item;

  DiaPoint point1, point2;

  gdouble line_width;
  GdkColor line_color;
};

struct _DiaCanvasLineClass
{
  DiaCanvasItemClass parent_class;
};

GType dia_canvas_line_get_type (void);

#endif

/* dia-canvas-grid.h
 * Copyright (C) 2000  James Henstridge
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __DIA_CANVAS_GRID_H__
#define __DIA_CANVAS_GRID_H__

#include <gtk/gtk.h>
#include "dia-canvas-item.h"
#include "dia-canvas-group.h"

#define DIA_TYPE_CANVAS_GRID            (dia_canvas_grid_get_type ())
#define DIA_CANVAS_GRID(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), DIA_TYPE_CANVAS_GRID, DiaCanvasGrid))
#define DIA_CANVAS_GRID_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), DIA_TYPE_CANVAS_GRID, DiaCanvasGridClass))
#define DIA_IS_CANVAS_GRID(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), DIA_TYPE_CANVAS_GRID))
#define DIA_IS_CANVAS_GRID_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj), DIA_TYPE_CANVAS_GRID))
#define DIA_CANVAS_GRID_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj), DIA_TYPE_CANVAS_GRID, DiaCanvasGridClass))

typedef struct _DiaCanvasGrid DiaCanvasGrid;
typedef struct _DiaCanvasGridClass DiaCanvasGridClass;

struct _DiaCanvasGrid
{
  DiaCanvasGroup group;

  gdouble  spacing;
  GdkColor color;
  gboolean snapping;
};

struct _DiaCanvasGridClass
{
  DiaCanvasGroupClass parent_class;
};

GType dia_canvas_grid_get_type (void);

#endif

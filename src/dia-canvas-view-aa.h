/* dia-canvas-view-aa.h
 * Copyright (C) 2000  James Henstridge
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __DIA_CANVAS_VIEW_AA_H__
#define __DIA_CANVAS_VIEW_AA_H__

#include <gtk/gtk.h>
#include "dia-canvas-view.h"

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define GTK_TYPE_DIA_CANVAS_VIEW_AA		(dia_canvas_view_aa_get_type ())
#define DIA_CANVAS_VIEW_AA(obj)			(GTK_CHECK_CAST ((obj), GTK_TYPE_DIA_CANVAS_VIEW_AA, DiaCanvasViewAA))
#define DIA_CANVAS_VIEW_AA_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), GTK_TYPE_DIA_CANVAS_VIEW_AA, DiaCanvasViewAAClass))
#define DIA_IS_CANVAS_VIEW_AA(obj)			(GTK_CHECK_TYPE ((obj), GTK_TYPE_DIA_CANVAS_VIEW_AA))
#define DIA_IS_CANVAS_VIEW_AA_CLASS(klass)		(GTK_CHECK_CLASS_TYPE ((obj), GTK_TYPE_DIA_CANVAS_VIEW_AA))


typedef struct _DiaCanvasViewAA       DiaCanvasViewAA;
typedef struct _DiaCanvasViewAAClass  DiaCanvasViewAAClass;

struct _DiaCanvasViewAA
{
  DiaCanvasView parent;

  /* Put your own, widget-specific fields here */

};
struct _DiaCanvasViewAAClass
{
  DiaCanvasViewClass parent_class;
};


GtkType    dia_canvas_view_aa_get_type (void);
GtkWidget *dia_canvas_view_aa_new      (void);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __DIA_CANVAS_VIEW_AA_H__ */

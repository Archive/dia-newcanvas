/* dia-canvas-view-gdk.c
 * Copyright (C) 2000  James Henstridge
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <math.h>
#include <string.h>

#include <gtk/gtk.h>
#include "dia-canvas-view-gdk.h"


static void dia_canvas_view_gdk_init	    (DiaCanvasViewGdk *canvas_view);
static void dia_canvas_view_gdk_class_init  (DiaCanvasViewGdkClass *klass);
static void dia_canvas_view_gdk_renderer_init (DiaRendererClass *vtable);

static void dia_canvas_view_gdk_destroy     (GtkObject *object);
static void dia_canvas_view_gdk_realize     (GtkWidget *widget);
static void dia_canvas_view_gdk_unrealize   (GtkWidget *widget);
static void dia_canvas_view_gdk_render_rect (DiaCanvasView *view,
					     GdkRectangle *grect,
					     DiaRectangle *crect);
static void dia_canvas_view_gdk_set_zoom    (DiaCanvasView *view,
					     gdouble zoom);

static DiaCanvasViewClass *parent_class = NULL;

#define TRANS_X(x) (DIA_CANVAS_VIEW(canvas_view)->canvas->extents.left + (x) * DIA_CANVAS_VIEW(canvas_view)->zoom_factor)
#define TRANS_Y(y) (DIA_CANVAS_VIEW(canvas_view)->canvas->extents.top + (y) * DIA_CANVAS_VIEW(canvas_view)->zoom_factor)
#define TRANS_LEN(l) ((l) * DIA_CANVAS_VIEW(canvas_view)->zoom_factor)

GtkType
dia_canvas_view_gdk_get_type (void)
{
  static GtkType canvas_view_gdk_type = 0;

  if (!canvas_view_gdk_type)
    {
      static const GTypeInfo canvas_view_gdk_info =
      {
	sizeof (DiaCanvasViewGdkClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) dia_canvas_view_gdk_class_init,
	(GClassFinalizeFunc) NULL,
	NULL,
	
	sizeof (DiaCanvasViewGdk),
	0, /* n_preallocs */
	(GInstanceInitFunc) dia_canvas_view_gdk_init,
      };
      static const GInterfaceInfo renderer_info =
      {
	(GInterfaceInitFunc) dia_canvas_view_gdk_renderer_init,
	(GInterfaceFinalizeFunc) NULL,
	NULL,
      };

      canvas_view_gdk_type = g_type_register_static (DIA_TYPE_CANVAS_VIEW,
						     "DiaCanvasViewGdk",
						     &canvas_view_gdk_info, 0);
      g_type_add_interface_static (canvas_view_gdk_type,
				   DIA_TYPE_RENDERER,
				   &renderer_info);
    }

  return canvas_view_gdk_type;
}

static void
dia_canvas_view_gdk_class_init (DiaCanvasViewGdkClass *klass)
{
  GObjectClass *object_class;
  GtkObjectClass *gtkobject_class;
  GtkWidgetClass *widget_class;
  DiaCanvasViewClass *view_class;

  object_class = G_OBJECT_CLASS(klass);
  gtkobject_class = GTK_OBJECT_CLASS(klass);
  widget_class = GTK_WIDGET_CLASS(klass);
  view_class = DIA_CANVAS_VIEW_CLASS(klass);

  parent_class = gtk_type_class (DIA_TYPE_CANVAS_VIEW);

  gtkobject_class->destroy = dia_canvas_view_gdk_destroy;
  widget_class->realize = dia_canvas_view_gdk_realize;
  widget_class->unrealize = dia_canvas_view_gdk_unrealize;
  view_class->render_rect = dia_canvas_view_gdk_render_rect;
  view_class->set_zoom = dia_canvas_view_gdk_set_zoom;
}


static void
dia_canvas_view_gdk_init (DiaCanvasViewGdk *canvas_view)
{
  canvas_view->gc = NULL;
  canvas_view->dashes = NULL;
}

/**
 * dia_canvas_view_gdk_new:
 * @canvas: a #DiaCanvas.
 *
 * Create a #DiaCanvasViewGdk linked to @canvas.
 *
 * Return value: a GtkWidget.
 **/
GtkWidget *
dia_canvas_view_gdk_new(DiaCanvas *canvas)
{
  DiaCanvasViewGdk *view;

  g_return_val_if_fail(canvas != NULL, NULL);
  g_return_val_if_fail(DIA_IS_CANVAS(canvas), NULL);

  view = g_object_new(DIA_TYPE_CANVAS_VIEW_GDK, NULL);
  dia_canvas_view_set_canvas(DIA_CANVAS_VIEW(view), canvas);

  return GTK_WIDGET(view);
}

static void
dia_canvas_view_gdk_destroy (GtkObject *object)
{
  DiaCanvasViewGdk *canvas_view = DIA_CANVAS_VIEW_GDK(object);

  if (canvas_view->gc) {
    g_object_unref(G_OBJECT(canvas_view->gc));
    canvas_view->gc = NULL;
  }
  if (canvas_view->dashes) {
    g_free(canvas_view->dashes);
    canvas_view->dashes = NULL;
  }

  if (GTK_OBJECT_CLASS(parent_class)->destroy)
    (* GTK_OBJECT_CLASS(parent_class)->destroy) (object);
}

static void
dia_canvas_view_gdk_realize (GtkWidget *widget)
{
  DiaCanvasViewGdk *canvas_view = DIA_CANVAS_VIEW_GDK(widget);
  GdkWindowAttr attributes;
  gint attributes_mask;

  GTK_WIDGET_SET_FLAGS(widget, GTK_REALIZED);
  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.x = widget->allocation.x;
  attributes.y = widget->allocation.y;
  attributes.width = widget->allocation.width;
  attributes.height = widget->allocation.height;
  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes.visual = gtk_widget_get_visual(widget);
  attributes.colormap = gtk_widget_get_colormap(widget);
  attributes.event_mask = gtk_widget_get_events(widget) | GDK_EXPOSURE_MASK;

  attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;

  widget->window = gdk_window_new (gtk_widget_get_parent_window(widget),
				   &attributes, attributes_mask);
  gdk_window_set_user_data(widget->window, widget);

  widget->style = gtk_style_attach (widget->style, widget->window);
  gtk_style_set_background (widget->style, widget->window, GTK_STATE_NORMAL);

  /* now that it is realized, we can create our GC */
  canvas_view->gc = gdk_gc_new(widget->window);
  canvas_view->fill_rule = GDK_SOLID;
  canvas_view->join_style = GDK_JOIN_MITER;
  canvas_view->cap_style = GDK_CAP_BUTT;
  canvas_view->line_width = 0;
  canvas_view->line_style = GDK_LINE_SOLID;
  if (canvas_view->dashes) {
    g_free(canvas_view->dashes);
    canvas_view->dashes = NULL;
  }

  canvas_view->color.red = 0;
  canvas_view->color.green = 0;
  canvas_view->color.blue = 0;
  gdk_colormap_alloc_color (gtk_widget_get_colormap(widget),
			    &canvas_view->color, FALSE, TRUE);
  gdk_gc_set_foreground (canvas_view->gc, &canvas_view->color);
}

static void
dia_canvas_view_gdk_unrealize (GtkWidget *widget)
{
  DiaCanvasViewGdk *canvas_view = DIA_CANVAS_VIEW_GDK(widget);

  if (canvas_view->gc) {
    g_object_unref(G_OBJECT(canvas_view->gc));
    canvas_view->gc = NULL;
  }
  if (canvas_view->dashes) {
    g_free(canvas_view->dashes);
    canvas_view->dashes = NULL;
  }

  if (GTK_WIDGET_CLASS(parent_class)->unrealize)
    (* GTK_WIDGET_CLASS(parent_class)->unrealize) (widget);
}

static void
dia_canvas_view_gdk_render_rect(DiaCanvasView *view, GdkRectangle *grect,
				DiaRectangle *crect)
{
  dia_canvas_render (view->canvas, DIA_RENDERER(view), crect);
}

static void
dia_canvas_view_gdk_set_zoom (DiaCanvasView *view, gdouble zoom)
{
  DiaCanvasViewGdk *cvg = DIA_CANVAS_VIEW_GDK(view);

  if (DIA_CANVAS_VIEW_CLASS(parent_class)->set_zoom)
    (* DIA_CANVAS_VIEW_CLASS(parent_class)->set_zoom) (view, zoom);

  if (cvg->gc) {
    /* reset the line style with new zoom factor */
    gdk_gc_set_line_attributes(cvg->gc,
	cvg->line_width * view->zoom_factor,
	cvg->line_style, cvg->cap_style, cvg->join_style);
    if (cvg->dashes) {
      gint8 *dash_list;
      gint i;

      /* reset dashes with new size */
      dash_list = g_new(gint8, cvg->ndashes);
      for (i = 0; i < cvg->ndashes; i++) {
	int length = (int)floor(cvg->dashes[i] * view->zoom_factor + 0.5);

	length = MAX(length, 0);
	dash_list[i] = MIN(length, 255);
      }
      gdk_gc_set_dashes(cvg->gc, 0, dash_list, cvg->ndashes);
      g_free(dash_list);
    }
  }
}

/* -------- the renderer implementation -------- */

static void
set_fill_rule (DiaRenderer *renderer, GdkFillRule rule)
{
  DiaCanvasViewGdk *canvas_view = DIA_CANVAS_VIEW_GDK(renderer);

  g_return_if_fail(canvas_view->gc != NULL);

  if (rule != canvas_view->fill_rule) {
    canvas_view->fill_rule = rule;
    gdk_gc_set_fill(canvas_view->gc, canvas_view->fill_rule);
  }
}

static void
set_join_style (DiaRenderer *renderer, GdkJoinStyle style)
{
  DiaCanvasViewGdk *canvas_view = DIA_CANVAS_VIEW_GDK(renderer);

  g_return_if_fail(canvas_view->gc != NULL);

  if (style != canvas_view->join_style) {
    canvas_view->join_style = style;
    gdk_gc_set_line_attributes(canvas_view->gc,
	canvas_view->line_width * DIA_CANVAS_VIEW(canvas_view)->zoom_factor,
	canvas_view->line_style, canvas_view->cap_style,
	canvas_view->join_style);
  }
}

static void
set_cap_style (DiaRenderer *renderer, GdkCapStyle style)
{
  DiaCanvasViewGdk *canvas_view = DIA_CANVAS_VIEW_GDK(renderer);

  g_return_if_fail(canvas_view->gc != NULL);

  if (style != canvas_view->cap_style) {
    canvas_view->cap_style = style;
    gdk_gc_set_line_attributes(canvas_view->gc,
	canvas_view->line_width * DIA_CANVAS_VIEW(canvas_view)->zoom_factor,
	canvas_view->line_style, canvas_view->cap_style,
	canvas_view->join_style);
  }
}

static void
set_line_width (DiaRenderer *renderer, gdouble width)
{
  DiaCanvasViewGdk *canvas_view = DIA_CANVAS_VIEW_GDK(renderer);

  g_return_if_fail(canvas_view->gc != NULL);

  if (width != canvas_view->line_width) {
    canvas_view->line_width = width;
    gdk_gc_set_line_attributes(canvas_view->gc,
	canvas_view->line_width * DIA_CANVAS_VIEW(canvas_view)->zoom_factor,
	canvas_view->line_style, canvas_view->cap_style,
	canvas_view->join_style);
  }
}

static void
set_line_style (DiaRenderer *renderer, GdkLineStyle style)
{
  DiaCanvasViewGdk *canvas_view = DIA_CANVAS_VIEW_GDK(renderer);

  g_return_if_fail(canvas_view->gc != NULL);

  if (style != canvas_view->join_style) {
    canvas_view->line_style = style;
    gdk_gc_set_line_attributes(canvas_view->gc,
	canvas_view->line_width * DIA_CANVAS_VIEW(canvas_view)->zoom_factor,
	canvas_view->line_style, canvas_view->cap_style,
	canvas_view->join_style);
  }
}

static void
set_dash_style (DiaRenderer *renderer, gdouble *dash_len, gint nlen)
{
  DiaCanvasViewGdk *canvas_view = DIA_CANVAS_VIEW_GDK(renderer);
  gint8 *dash_list;
  int i;

  g_return_if_fail(canvas_view->gc != NULL);

  if (canvas_view->dashes != NULL)
    g_free(canvas_view->dashes);
  canvas_view->dashes = g_memdup(dash_len, sizeof(gdouble) * nlen);
  canvas_view->ndashes = nlen;

  dash_list = g_new(gint8, nlen);
  for (i = 0; i < nlen; i++) {
    int length = (int)floor(dash_len[i] *
			    DIA_CANVAS_VIEW(renderer)->zoom_factor + 0.5);

    length = MAX(length, 0);
    dash_list[i] = MIN(length, 255);
  }
  gdk_gc_set_dashes(canvas_view->gc, 0, dash_list, nlen);
  g_free(dash_list);
}

static void
set_color (DiaRenderer *renderer, GdkColor *color)
{
  DiaCanvasViewGdk *canvas_view = DIA_CANVAS_VIEW_GDK(renderer);

  g_return_if_fail(canvas_view->gc != NULL);

  if (color->red != canvas_view->color.red ||
      color->green != canvas_view->color.green ||
      color->blue != canvas_view->color.blue) {
    canvas_view->color = *color;
    gdk_colormap_alloc_color (gtk_widget_get_colormap(GTK_WIDGET(canvas_view)),
			      &canvas_view->color, FALSE, TRUE);

    gdk_gc_set_foreground(canvas_view->gc, &canvas_view->color);
  }
}

static void
set_font (DiaRenderer *renderer, const gchar *font_desc, gdouble size)
{
}

static void
draw_point (DiaRenderer *renderer, DiaPoint *point)
{
  DiaCanvasViewGdk *canvas_view = DIA_CANVAS_VIEW_GDK(renderer);

  g_return_if_fail(canvas_view->gc != NULL);

  gdk_draw_point (GTK_WIDGET(canvas_view)->window, canvas_view->gc,
		  TRANS_X(point->x), TRANS_Y(point->y));
}

static void
draw_line (DiaRenderer *renderer, DiaPoint *start, DiaPoint *end)
{
  DiaCanvasViewGdk *canvas_view = DIA_CANVAS_VIEW_GDK(renderer);

  g_return_if_fail(canvas_view->gc != NULL);

  gdk_draw_line (GTK_WIDGET(canvas_view)->window, canvas_view->gc,
		 TRANS_X(start->x), TRANS_Y(start->y),
		 TRANS_X(end->x), TRANS_Y(end->y));
}

static void
draw_rectangle (DiaRenderer *renderer, gboolean filled,
		DiaPoint *ul_corner, DiaPoint *lr_corner)
{
  DiaCanvasViewGdk *canvas_view = DIA_CANVAS_VIEW_GDK(renderer);
  gint top, bottom, left, right;

  g_return_if_fail(canvas_view->gc != NULL);

  left   = TRANS_X(ul_corner->x);
  top    = TRANS_Y(ul_corner->y);
  right  = TRANS_X(lr_corner->x);
  bottom = TRANS_Y(lr_corner->y);

  if (left > right || top > bottom)
    return;

  gdk_draw_rectangle (GTK_WIDGET(canvas_view)->window,
		      canvas_view->gc, filled,
		      left, top, (right - left), (bottom - top));
}

static void
draw_polyline (DiaRenderer *renderer, DiaPoint *points, gint npoints)
{
  DiaCanvasViewGdk *canvas_view = DIA_CANVAS_VIEW_GDK(renderer);
  GdkPoint *gdk_points;
  gint i;

  g_return_if_fail(canvas_view->gc != NULL);

  gdk_points = g_new(GdkPoint, npoints);
  for (i = 0; i < npoints; i++) {
    gdk_points[i].x = TRANS_X(points[i].x);
    gdk_points[i].y = TRANS_X(points[i].y);
  }

  gdk_draw_lines (GTK_WIDGET(canvas_view)->window, canvas_view->gc,
		  gdk_points, npoints);
  g_free(gdk_points);
}

static void
draw_polygon (DiaRenderer *renderer, gboolean filled, GList/*DiaPoint*/ *points)
{
  DiaCanvasViewGdk *canvas_view = DIA_CANVAS_VIEW_GDK(renderer);
  GdkPoint *gdk_points;
  gint i, npoints;

  g_return_if_fail(canvas_view->gc != NULL);

  npoints = g_list_length(points);

  if( npoints == 0 )
    return;

  gdk_points = g_new0(GdkPoint, npoints);
  for (i = 0; i < npoints; i++) {
    DiaPoint *point;

    point = g_list_nth_data(points,i);
    gdk_points[i].x = TRANS_X(point->x);
    gdk_points[i].y = TRANS_X(point->y);
  }

  gdk_draw_polygon (GTK_WIDGET(canvas_view)->window, canvas_view->gc,
		    filled, gdk_points, npoints);
  g_free(gdk_points);
}

static void
draw_arc (DiaRenderer *renderer, gboolean filled, DiaPoint *centre,
	  gdouble width, gdouble height, gdouble angle1, gdouble angle2)
{
  DiaCanvasViewGdk *canvas_view = DIA_CANVAS_VIEW_GDK(renderer);
  gint top, bottom, left, right;
  gdouble dangle;

  g_return_if_fail(canvas_view->gc != NULL);

  left   = TRANS_X(centre->x - width/2);
  top    = TRANS_Y(centre->y - height/2);
  right  = TRANS_X(centre->x + width/2);
  bottom = TRANS_Y(centre->y + height/2);
  
  if (left > right || top > bottom)
    return;

  dangle = angle2 - angle1;
  if (dangle < 0)
    dangle += 360.0;

  gdk_draw_arc (GTK_WIDGET(canvas_view)->window, canvas_view->gc,
		filled, left, top, (right - left), (bottom - top),
		(int) (angle1 * 64), (int) (dangle * 64));
}

static void
draw_ellipse (DiaRenderer *renderer, gboolean filled,
	      DiaPoint *centre, gdouble width, gdouble height)
{
  draw_arc (renderer, filled, centre, width, height, 0.0, 360.0);
}

static void
draw_bezier (DiaRenderer *renderer, gboolean filled,
	     DiaBezierPath *points, gint npoints)
{
}

static void
draw_pixbuf (DiaRenderer *renderer, DiaPoint *pos, gdouble width,
	     gdouble height, gdouble angle, GdkPixbuf *src)
{
}

static void
draw_text (DiaRenderer *renderer, DiaPoint *point, const gchar *text)
{
  gint                  fontsize;
  PangoContext         *pango_context;
  PangoLayout          *pango_layout;
  PangoFontDescription *pango_font_desc;
  DiaCanvasViewGdk     *canvas_view = DIA_CANVAS_VIEW_GDK(renderer);

  g_return_if_fail(canvas_view->gc != NULL);

  pango_context         = gtk_widget_get_pango_context (GTK_WIDGET(renderer));
  pango_font_desc       = pango_font_description_copy(pango_context_get_font_description(pango_context));
  fontsize = pango_font_description_get_size(pango_font_desc);
  pango_font_description_set_size(pango_font_desc,rint(fontsize*DIA_CANVAS_VIEW(canvas_view)->zoom_factor));
  pango_layout          = pango_layout_new(pango_context);
  pango_layout_set_font_description(pango_layout,pango_font_desc);

  pango_layout_set_text(pango_layout,text,strlen(text));


  gdk_draw_layout(GTK_WIDGET(renderer)->window,
		  canvas_view->gc,
		  TRANS_X(point->x), TRANS_X(point->y),
		  pango_layout);

  g_object_unref(pango_layout);
}

static void
get_text_size (DiaRenderer *renderer, DiaPoint *point, const gchar *text)
{
  gint                  fontsize;
  PangoContext         *pango_context;
  PangoLayout          *pango_layout;
  PangoFontDescription *pango_font_desc;
  PangoRectangle        ink, logical;
  DiaCanvasViewGdk     *canvas_view = DIA_CANVAS_VIEW_GDK(renderer);

  pango_context    = gtk_widget_get_pango_context (GTK_WIDGET(renderer));
  pango_font_desc  = pango_font_description_copy(pango_context_get_font_description(pango_context));
  fontsize = pango_font_description_get_size(pango_font_desc);
  pango_font_description_set_size(pango_font_desc,rint(fontsize*DIA_CANVAS_VIEW(canvas_view)->zoom_factor));
  pango_layout     = pango_layout_new(pango_context);
  pango_layout_set_font_description(pango_layout,pango_font_desc);

  pango_layout_set_text(pango_layout,text,strlen(text));

  pango_layout_get_pixel_extents(pango_layout,&ink,&logical);

  /*
  point->x = (ink.width  - ink.x)/DIA_CANVAS_VIEW(canvas_view)->zoom_factor;
  point->y = (ink.height - ink.y)/DIA_CANVAS_VIEW(canvas_view)->zoom_factor;
  */

  point->x = (logical.width  - logical.x)/DIA_CANVAS_VIEW(canvas_view)->zoom_factor;
  point->y = (logical.height - logical.y)/DIA_CANVAS_VIEW(canvas_view)->zoom_factor;

  g_object_unref(pango_layout);
}

static void
dia_canvas_view_gdk_renderer_init (DiaRendererClass *vtable)
{
  vtable->set_fill_rule  = set_fill_rule;
  vtable->set_join_style = set_join_style;
  vtable->set_cap_style  = set_cap_style;
  vtable->set_line_width = set_line_width;
  vtable->set_line_style = set_line_style;
  vtable->set_dash_style = set_dash_style;
  vtable->set_color     = set_color;
  vtable->set_font       = set_font;

  vtable->draw_point     = draw_point;
  vtable->draw_line      = draw_line;
  vtable->draw_rectangle = draw_rectangle;
  vtable->draw_polyline  = draw_polyline;
  vtable->draw_polygon   = draw_polygon;
  vtable->draw_ellipse   = draw_ellipse;
  vtable->draw_arc       = draw_arc;
  vtable->draw_bezier    = draw_bezier;
  vtable->draw_pixbuf    = draw_pixbuf;
  vtable->draw_text      = draw_text;

  vtable->get_text_size  = get_text_size;
}

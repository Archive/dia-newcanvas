/* dia-renderer.h
 * Copyright (C) 2000  James Henstridge, Alexander Larsson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __DIA_RENDERER_H__
#define __DIA_RENDERER_H__

#include <gtk/gtk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include "dia-geometry.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define DIA_TYPE_RENDERER	       (dia_renderer_get_type ())
#define DIA_RENDERER(obj)              (G_TYPE_CHECK_INSTANCE_CAST ((obj), DIA_TYPE_RENDERER, DiaRenderer))
#define DIA_RENDERER_CLASS(vtable)     (G_TYPE_CHECK_CLASS_CAST ((vtable), DIA_TYPE_RENDERER, DiaRendererClass)
#define DIA_IS_RENDERER(obj)           (G_TYPE_CHECK_INSTANCE_TYPE ((obj), DIA_TYPE_RENDERER))
#define DIA_IS_RENDERER_CLASS(vtable)  (G_TYPE_CHECK_CLASS_TYPE ((vtable), DIA_TYPE_RENDERER))
#define DIA_RENDERER_GET_CLASS(inst)   (G_TYPE_INSTANCE_GET_INTERFACE ((inst), DIA_TYPE_RENDERER, DiaRendererClass))

typedef struct _DiaRenderer       DiaRenderer;
typedef struct _DiaRendererClass  DiaRendererClass;

typedef enum
{
  DIA_BEZ_MOVE_TO,
  DIA_BEZ_LINE_TO,
  DIA_BEZ_CURVE_TO
} DiaBezierType;

typedef struct _DiaBezierPath     DiaBezierPath;
struct _DiaBezierPath {
  DiaBezierType type;
  DiaPoint p1, p2, p3;
};

struct _DiaRendererClass
{
  GTypeInterface base_iface;

  /* State setting functions */
  void (*set_fill_rule)  (DiaRenderer *renderer, GdkFillRule rule);
  void (*set_join_style) (DiaRenderer *renderer, GdkJoinStyle style);
  void (*set_cap_style)  (DiaRenderer *renderer, GdkCapStyle style);
  void (*set_line_width) (DiaRenderer *renderer, gdouble width);
  void (*set_line_style) (DiaRenderer *renderer, GdkLineStyle style);
  void (*set_dash_style) (DiaRenderer *renderer, gdouble *dash_len, gint nlen);
  void (*set_color)      (DiaRenderer *renderer, GdkColor *color);
  /* the font size in font_desc is ignored, as we want something scalable */
  void (*set_font)       (DiaRenderer *renderer, const gchar *font_desc,
			  gdouble size);

  /* Primitives */
  void (* draw_point)     (DiaRenderer *renderer,
			   DiaPoint *point);
  void (* draw_line)      (DiaRenderer *renderer,
			   DiaPoint *start, DiaPoint *end);
  void (* draw_rectangle) (DiaRenderer *renderer, gboolean filled,
			   DiaPoint *ul_corner, DiaPoint *lr_corner);

  void (* draw_polyline)  (DiaRenderer *renderer,
			   DiaPoint *points, gint npoints);
  void (* draw_polygon)   (DiaRenderer *renderer, gboolean filled,
			   GList /*DiaPoint*/ *points);

  void (* draw_ellipse)   (DiaRenderer *renderer, gboolean filled,
			   DiaPoint *centre, gdouble width, gdouble height);
			   
  void (* draw_arc)       (DiaRenderer *renderer, gboolean filled,
			   DiaPoint *centre,
			   gdouble width, gdouble height,
			   gdouble angle1, gdouble angle2);

  void (* draw_bezier)    (DiaRenderer *renderer, gboolean filled,
			   DiaBezierPath *points, gint npoints);

  void (* draw_pixbuf)    (DiaRenderer *renderer,
			   DiaPoint *pos, gdouble width, gdouble height,
			   gdouble angle, GdkPixbuf *src);

  void (* draw_text)      (DiaRenderer *renderer,
			   DiaPoint *point, const gchar *text);
  void (* get_text_size)  (DiaRenderer *renderer,
			   DiaPoint *point, const gchar *text);

};

GType   dia_renderer_get_type (void);

/* State setting functions */
void dia_renderer_set_fill_rule  (DiaRenderer *renderer, GdkFillRule rule);
void dia_renderer_set_join_style (DiaRenderer *renderer, GdkJoinStyle style);
void dia_renderer_set_cap_style  (DiaRenderer *renderer, GdkCapStyle style);
void dia_renderer_set_line_width (DiaRenderer *renderer, gdouble width);
void dia_renderer_set_line_style (DiaRenderer *renderer, GdkLineStyle style);
void dia_renderer_set_dash_style (DiaRenderer *renderer, gdouble *dash_len,
				  gint nlen);
void dia_renderer_set_color     (DiaRenderer *renderer, GdkColor *color);
/* the font size in font_desc is ignored, as we want something scalable */
void dia_renderer_set_font       (DiaRenderer *renderer,
				  const gchar *font_desc, gdouble size);

/* Primitives */
void dia_renderer_draw_point     (DiaRenderer *renderer,
				  DiaPoint *point);
void dia_renderer_draw_line      (DiaRenderer *renderer,
				  DiaPoint *start, DiaPoint *end);
void dia_renderer_draw_rectangle (DiaRenderer *renderer, gboolean filled,
				  DiaPoint *ul_corner, DiaPoint *lr_corner);

void dia_renderer_draw_polyline  (DiaRenderer *renderer,
				  DiaPoint *points, gint npoints);
void dia_renderer_draw_polygon   (DiaRenderer *renderer, gboolean filled,
				  GList /*DiaPoint*/ *points);

void dia_renderer_draw_ellipse   (DiaRenderer *renderer, gboolean filled,
				  DiaPoint *centre, gdouble width,
				  gdouble height);
			   
void dia_renderer_draw_arc       (DiaRenderer *renderer, gboolean filled,
				  DiaPoint *centre,
				  gdouble width, gdouble height,
				  gdouble angle1, gdouble angle2);

void dia_renderer_draw_bezier    (DiaRenderer *renderer, gboolean filled,
				  DiaBezierPath *points, gint npoints);

void dia_renderer_draw_pixbuf    (DiaRenderer *renderer,
				  DiaPoint *pos, gdouble width, gdouble height,
				  gdouble angle, GdkPixbuf *src);

void dia_renderer_draw_text      (DiaRenderer *renderer,
				  DiaPoint *point, const gchar *text);

/* Text size */
void dia_renderer_get_text_size  (DiaRenderer *renderer,
				  DiaPoint *point, const gchar *text);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __DIA_RENDERER_H__ */

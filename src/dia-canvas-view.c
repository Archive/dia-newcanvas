/* dia-canvas-view.c
 * Copyright (C) 2000  James Henstridge
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gtk/gtk.h>
#include "dia-canvas-view.h"
#include "dia-canvas-item.h"
#include "dia-marshal.h"

#define REDRAW_PRIORITY (G_PRIORITY_HIGH_IDLE + 40)
#define PICK_PRIORITY (G_PRIORITY_HIGH_IDLE + 35)

#define VIEWPORT_LEFT(v) ((v)->canvas->extents.left + \
  (v)->layout.hadjustment->value / (v)->zoom_factor)
#define VIEWPORT_TOP(v) ((v)->canvas->extents.top + \
  (v)->layout.vadjustment->value / (v)->zoom_factor)
#define VIEWPORT_WIDTH(v) ((v)->zoom_factor*GTK_WIDGET(v)->allocation.width)
#define VIEWPORT_HEIGHT(v) ((v)->zoom_factor*GTK_WIDGET(v)->allocation.height)

enum {
  RENDER_RECT,
  SET_ZOOM,
  LAST_SIGNAL
};

static void dia_canvas_view_init	    (DiaCanvasView *canvas_view);
static void dia_canvas_view_class_init      (DiaCanvasViewClass *klass);

static void dia_canvas_view_destroy         (GtkObject *object);
static void dia_canvas_view_size_request    (GtkWidget *widget,
					     GtkRequisition *requisition);
static void dia_canvas_view_size_allocate   (GtkWidget *widget,
					     GtkAllocation *allocation);
static gint dia_canvas_view_expose          (GtkWidget *widget,
					     GdkEventExpose *event);
static gint dia_canvas_view_focus_in        (GtkWidget *widget,
					     GdkEventFocus *event);
static gint dia_canvas_view_focus_out       (GtkWidget *widget,
					     GdkEventFocus *event);
static gint dia_canvas_view_key_press       (GtkWidget *widget,
					     GdkEventKey *event);
static gint dia_canvas_view_key_release     (GtkWidget *widget,
					     GdkEventKey *event);
static gint dia_canvas_view_button_press    (GtkWidget *widget,
					     GdkEventButton *event);
static gint dia_canvas_view_button_release  (GtkWidget *widget,
					     GdkEventButton *event);
static gint dia_canvas_view_motion_notify   (GtkWidget *widget,
					     GdkEventMotion *event);
static gint dia_canvas_view_enter_notify    (GtkWidget *widget,
					     GdkEventCrossing *event);
static gint dia_canvas_view_leave_notify    (GtkWidget *widget,
					     GdkEventCrossing *event);
static void dia_canvas_view_real_set_zoom   (DiaCanvasView *canvas_view,
					     gdouble zoom);


static GtkWidgetClass *parent_class = NULL;
static guint canvas_view_signals[LAST_SIGNAL] = { 0 };


GtkType
dia_canvas_view_get_type (void)
{
  static GtkType canvas_view_type = 0;

  if (!canvas_view_type)
    {
      static const GTypeInfo canvas_view_info =
      {
	sizeof (DiaCanvasViewClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) dia_canvas_view_class_init,
	(GClassFinalizeFunc) NULL,
	NULL,
	
	sizeof (DiaCanvasView),
	0, /* n_preallocs */
	(GInstanceInitFunc) dia_canvas_view_init,
      };

      canvas_view_type = g_type_register_static (GTK_TYPE_WIDGET,
						 "DiaCanvasView",
						 &canvas_view_info, 0);
    }

  return canvas_view_type;
}

static void
dia_canvas_view_class_init (DiaCanvasViewClass *klass)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;

  object_class = GTK_OBJECT_CLASS(klass);
  widget_class = GTK_WIDGET_CLASS(klass);

  parent_class = g_type_class_ref (GTK_TYPE_WIDGET);

  canvas_view_signals[RENDER_RECT] = 
    g_signal_new ("render_rect",
		   G_OBJECT_CLASS_TYPE(klass),
		   G_SIGNAL_RUN_FIRST,
		   G_STRUCT_OFFSET (DiaCanvasViewClass, render_rect),NULL,NULL,
		   dia_marshal_VOID__POINTER_POINTER,
		   G_TYPE_NONE, 2,
		   G_TYPE_POINTER /* GdkRectangle */,
		   G_TYPE_POINTER /* DiaRectangle */);
  canvas_view_signals[SET_ZOOM] =
    g_signal_new ("set_zoom",
		   G_OBJECT_CLASS_TYPE(klass),
		   G_SIGNAL_RUN_FIRST,
		   G_STRUCT_OFFSET (DiaCanvasViewClass, set_zoom), NULL, NULL,
		   dia_marshal_VOID__DOUBLE,
		   G_TYPE_NONE, 1,
		   G_TYPE_DOUBLE);


  klass->image_width = 512;
  klass->image_height = 512;

  object_class->destroy = dia_canvas_view_destroy;

  widget_class->size_request = dia_canvas_view_size_request;
  widget_class->size_allocate = dia_canvas_view_size_allocate;
  widget_class->expose_event = dia_canvas_view_expose;
  widget_class->focus_in_event = dia_canvas_view_focus_in;
  widget_class->focus_out_event = dia_canvas_view_focus_out;
  widget_class->key_press_event = dia_canvas_view_key_press;
  widget_class->key_release_event = dia_canvas_view_key_release;
  widget_class->button_press_event = dia_canvas_view_button_press;
  widget_class->button_release_event = dia_canvas_view_button_release;
  widget_class->motion_notify_event = dia_canvas_view_motion_notify;
  widget_class->enter_notify_event = dia_canvas_view_enter_notify;
  widget_class->leave_notify_event = dia_canvas_view_leave_notify;

  klass->set_zoom = dia_canvas_view_real_set_zoom;
}


static void
dia_canvas_view_init (DiaCanvasView *canvas_view)
{
  GTK_WIDGET_SET_FLAGS(canvas_view, GTK_CAN_FOCUS);

  gtk_widget_add_events(GTK_WIDGET(canvas_view),
			GDK_EXPOSURE_MASK |
			GDK_BUTTON_PRESS_MASK |
			GDK_BUTTON_RELEASE_MASK |
			GDK_POINTER_MOTION_MASK |
			GDK_POINTER_MOTION_MASK |
			GDK_KEY_PRESS_MASK |
			GDK_KEY_RELEASE_MASK |
			GDK_ENTER_NOTIFY_MASK |
			GDK_LEAVE_NOTIFY_MASK |
			GDK_FOCUS_CHANGE_MASK);

  canvas_view->zoom_factor = 1.0;
}

#if 0
static void
scroll_to(DiaCanvasView *view, gdouble x, gdouble y)
{
  gdouble view_left, view_top, view_width, view_height;
  gdouble scroll_width, scroll_height;
  gdouble right_limit, bottom_limit;
  gboolean changed, changed_x, changed_y;

  view_left   = VIEWPORT_LEFT(view);
  view_top    = VIEWPORT_TOP(view);
  view_width  = VIEWPORT_WIDTH(view);
  view_height = VIEWPORT_HEIGHT(view);

  scroll_width  = view->canvas->extents.right - view->canvas->extents.left;
  scroll_height = view->canvas->extents.bottom - view->canvas->extents.top;

  right_limit =  view->canvas->extents.right - view_width;
  bottom_limit = view->canvas->extents.bottom - view_height;

  if (x > right_limit)
    x = right_limit;
  if (y > bottom_limit)
    y = bottom_limit;
  if (x < view->canvas->extents.left)
    x = view->canvas->extents.left;
  if (y < view->canvas->extents.top)
    y = view->canvas->extents.top;

  changed_x = view_left != x;
  changed_y = view_top != y;
  changed = changed_x || changed_y;
  
  if (changed)
    gtk_layout_freeze(GTK_LAYOUT(view));

  if (view->layout.width != scroll_width * view->zoom_factor ||
      view->layout.height != scroll_height * view->zoom_factor)
    gtk_layout_set_size(GTK_LAYOUT(view),
			scroll_width * view->zoom_factor,
			scroll_height * view->zoom_factor);

  if (changed_x) {
    view->layout.hadjustment->value = x * view->zoom_factor;
    gtk_adjustment_value_changed(view->layout.hadjustment);
  }
  if (changed_y) {
    view->layout.vadjustment->value = y * view->zoom_factor;
    gtk_adjustment_value_changed(view->layout.vadjustment);
  }

  if (changed)
    gtk_layout_thaw(GTK_LAYOUT(view));
}
#endif

static gboolean
emit_event(DiaCanvasView *view, GdkEvent *event, DiaCanvasItem *item)
{
  GdkEvent ev;
  gdouble left = view->canvas->extents.left, top = view->canvas->extents.top;
  gdouble zf = view->zoom_factor;
  gboolean finished;
  DiaCanvasItem *parent;
  static guint event_signal_id = 0;

  /* do something about handling grabs ... */

  /* convert from view to canvas coordinates ... */
  ev = *event;
  switch(ev.type) {
  case GDK_ENTER_NOTIFY:
  case GDK_LEAVE_NOTIFY:
    ev.crossing.x = ev.crossing.x / zf + left;
    ev.crossing.y = ev.crossing.y / zf + top;
    break;
  case GDK_MOTION_NOTIFY:
    ev.motion.x = ev.motion.x / zf + left;
    ev.motion.y = ev.motion.y / zf + top;
    break;
  case GDK_BUTTON_PRESS:
  case GDK_2BUTTON_PRESS:
  case GDK_3BUTTON_PRESS:
  case GDK_BUTTON_RELEASE:
    ev.button.x = ev.button.x / zf + left;
    ev.button.y = ev.button.y / zf + top;
    break;
  default:
    break;
  }

  /* event propagation ... */
  finished = FALSE;
  if (event_signal_id == 0)
    event_signal_id = gtk_signal_lookup("event", DIA_TYPE_CANVAS_ITEM);
  while (item && !finished) {
    g_object_ref(G_OBJECT(item));

    g_signal_emit(item, event_signal_id, 0, &ev, view, &finished);

    if (item->parent)
      parent = DIA_CANVAS_ITEM(item->parent);
    else
      parent = NULL;
    g_object_unref(G_OBJECT(item));
    item = parent;
  }
  return finished;
}

static void
dia_canvas_view_destroy(GtkObject *object)
{
  DiaCanvasView *view;

  g_return_if_fail(object != NULL);
  g_return_if_fail(DIA_IS_CANVAS_VIEW(object));

  view = DIA_CANVAS_VIEW(object);

  /* disconnect the dirty_region handler */
  if (view->canvas) {
    g_signal_handlers_disconnect_matched(view->canvas, G_SIGNAL_MATCH_DATA,
					 0, 0, NULL, 0, view);
    dia_canvas_remove_view(view->canvas, view);
    g_object_unref(G_OBJECT(view->canvas));
  }
  view->canvas = NULL;

  if (GTK_OBJECT_CLASS(parent_class)->destroy)
    (* GTK_OBJECT_CLASS(parent_class)->destroy)(object);
}

static void
dia_canvas_view_size_request (GtkWidget *widget, GtkRequisition *requisition)
{
  DiaCanvasView *view;

  view = DIA_CANVAS_VIEW(widget);
  g_return_if_fail(view->canvas != NULL);

  requisition->width = (view->canvas->extents.right -
			view->canvas->extents.left) * view->zoom_factor;
  requisition->height = (view->canvas->extents.bottom -
			 view->canvas->extents.top) * view->zoom_factor;
}

static void
dia_canvas_view_size_allocate (GtkWidget *widget, GtkAllocation *allocation)
{
  DiaCanvasView *view;

  view = DIA_CANVAS_VIEW(widget);
  g_return_if_fail(view->canvas != NULL);

  /* do size allocate ... */
  if (GTK_WIDGET_CLASS(parent_class)->size_allocate)
    (* GTK_WIDGET_CLASS(parent_class)->size_allocate)(widget, allocation);
}

static gint
dia_canvas_view_expose(GtkWidget *widget, GdkEventExpose *event)
{
  DiaCanvasView *view;
  GdkRectangle grect;
  DiaRectangle crect;

  g_return_val_if_fail(widget != NULL, FALSE);
  g_return_val_if_fail(DIA_IS_CANVAS_VIEW(widget), FALSE);
  g_return_val_if_fail(event != NULL, FALSE);

  view = DIA_CANVAS_VIEW(widget);
  g_return_val_if_fail(view->canvas != NULL, FALSE);

  if (!GTK_WIDGET_DRAWABLE (widget) || event->window != widget->window)
    return FALSE;

  /* change this to use event->region at some point */
  grect = event->area;
  crect.left = grect.x / view->zoom_factor + view->canvas->extents.left;
  crect.right = crect.left + grect.width / view->zoom_factor;
  crect.top = grect.y / view->zoom_factor + view->canvas->extents.top;
  crect.bottom = crect.top + grect.height / view->zoom_factor;

  g_signal_emit(view, canvas_view_signals[RENDER_RECT], 0, &grect, &crect);

  return TRUE;
}

static gint
dia_canvas_view_focus_in (GtkWidget *widget, GdkEventFocus *event)
{
  DiaCanvasView *canvas_view = DIA_CANVAS_VIEW(widget);

  g_return_val_if_fail(canvas_view->canvas != NULL, FALSE);
  g_return_val_if_fail(DIA_IS_CANVAS(canvas_view->canvas), FALSE);

  /* update focus view */
  //canvas_view->canvas->focus_view = canvas_view;
  GTK_WIDGET_SET_FLAGS(widget, GTK_HAS_FOCUS);

  if (canvas_view->event_item)
    return emit_event(canvas_view, (GdkEvent *)event,
		      canvas_view->event_item);
  else
    return FALSE;
}

static gint
dia_canvas_view_focus_out (GtkWidget *widget, GdkEventFocus *event)
{
  DiaCanvasView *canvas_view = DIA_CANVAS_VIEW(widget);

  g_return_val_if_fail(canvas_view->canvas != NULL, FALSE);
  g_return_val_if_fail(DIA_IS_CANVAS(canvas_view->canvas), FALSE);

  //canvas_view->canvas->focus_view = NULL;
  GTK_WIDGET_UNSET_FLAGS(widget, GTK_HAS_FOCUS);

  if (canvas_view->event_item)
    return emit_event(canvas_view, (GdkEvent *)event,
		      canvas_view->event_item);
  else
    return FALSE;
}

static gint
dia_canvas_view_key_press (GtkWidget *widget, GdkEventKey *event)
{
  DiaCanvasView *canvas_view = DIA_CANVAS_VIEW(widget);

  g_return_val_if_fail(canvas_view->canvas != NULL, FALSE);
  g_return_val_if_fail(DIA_IS_CANVAS(canvas_view->canvas), FALSE);

  if (canvas_view->event_item)
    return emit_event(canvas_view, (GdkEvent *)event,
		      canvas_view->event_item);
  else
    return (* GTK_WIDGET_CLASS(parent_class)->key_press_event)(widget, event);
}

static gint
dia_canvas_view_key_release (GtkWidget *widget, GdkEventKey *event)
{
  DiaCanvasView *canvas_view = DIA_CANVAS_VIEW(widget);

  g_return_val_if_fail(canvas_view->canvas != NULL, FALSE);
  g_return_val_if_fail(DIA_IS_CANVAS(canvas_view->canvas), FALSE);

  if (canvas_view->event_item)
    return emit_event(canvas_view, (GdkEvent *)event,
		      canvas_view->event_item);
  else
    return (* GTK_WIDGET_CLASS(parent_class)->key_release_event)(widget,event);
}

static gint
dia_canvas_view_button_press (GtkWidget *widget, GdkEventButton *event)
{
  DiaCanvasView *view;
  DiaCanvas     *canvas;
  DiaCanvasItem *point_item;
  gdouble zf, x, y;

  view = DIA_CANVAS_VIEW(widget);
  canvas = view->canvas;
  g_return_val_if_fail(view->canvas != NULL, FALSE);
  g_return_val_if_fail(DIA_IS_CANVAS(view->canvas), FALSE);

  canvas = view->canvas;

  zf = view->zoom_factor;
  x = event->x / zf + canvas->extents.left;
  y = event->y / zf + canvas->extents.top;

  if (!GTK_WIDGET_HAS_GRAB(view)) {
    point_item = dia_canvas_get_item_at(view->canvas, x, y, 0);
    if (!point_item)
      point_item = canvas->root;

    view->event_item = point_item;
  }
  return emit_event(view, (GdkEvent *)event, view->event_item);
}

static gint
dia_canvas_view_button_release (GtkWidget *widget, GdkEventButton *event)
{
  DiaCanvasView *view;
  DiaCanvas     *canvas;
  DiaCanvasItem *point_item;
  gdouble zf, x, y;

  view  = DIA_CANVAS_VIEW(widget);
  canvas = view->canvas;
  g_return_val_if_fail(view->canvas != NULL, FALSE);
  g_return_val_if_fail(DIA_IS_CANVAS(view->canvas), FALSE);

  zf = view->zoom_factor;
  x = event->x / zf + canvas->extents.left;
  y = event->y / zf + canvas->extents.top;

  if (!GTK_WIDGET_HAS_GRAB(view)) {
    point_item = dia_canvas_get_item_at(view->canvas, x, y, 0);
    if (!point_item)
      point_item = canvas->root;

    view->event_item = point_item;
  }
  return emit_event(view, (GdkEvent *)event, view->event_item);
}

static gint
dia_canvas_view_motion_notify (GtkWidget *widget, GdkEventMotion *event)
{
  DiaCanvasView *view = DIA_CANVAS_VIEW(widget);
  DiaCanvas *canvas = view->canvas;
  DiaCanvasItem *point_item;
  gdouble zf = view->zoom_factor;
  gdouble x, y;
  gint x2, y2;
  GdkModifierType state;

  if( event->is_hint ) {
    gdk_window_get_pointer (event->window, &x2, &y2, &state);
    x = x2 / zf + canvas->extents.left;
    y = y2 / zf + canvas->extents.top;
  } else {
    x = event->x / zf + canvas->extents.left;
    y = event->y / zf + canvas->extents.top;
  }

  /* find the item at the point */
  point_item = dia_canvas_get_item_at(view->canvas, x, y, 0);
  if (!point_item)
    point_item = canvas->root;

  if ( (view->event_item != point_item) &&
       (!GTK_WIDGET_HAS_GRAB(view)) ) {
    GdkEventCrossing ev = { 0 };

    ev.type = GDK_LEAVE_NOTIFY;
    ev.window = event->window;
    ev.send_event = event->send_event;
    ev.subwindow = NULL;
    if( event->is_hint ) {
      ev.x = x2;
      ev.y = y2;
    } else {
      ev.x = event->x;
      ev.y = event->y;
    }
    ev.mode = GDK_CROSSING_NORMAL;
    ev.detail = GDK_NOTIFY_NONLINEAR;
    ev.focus = FALSE;
    ev.state = event->state;
    ev.x_root = event->x_root;
    ev.y_root = event->y_root;
    if (view->event_item != NULL) {
      /* create a leave_notify on the old current event */
      emit_event(view, (GdkEvent *)&ev, view->event_item);
    }
    ev.type = GDK_ENTER_NOTIFY;
    view->event_item = point_item;
    emit_event(view, (GdkEvent *)&ev, view->event_item);
  }

  /* now emit the motion_notify on the picked item */
  return emit_event(view, (GdkEvent *)event, view->event_item);
}

static gint
dia_canvas_view_enter_notify (GtkWidget *widget, GdkEventCrossing *event)
{
  DiaCanvasView *view = DIA_CANVAS_VIEW(widget);
  DiaCanvas *canvas = view->canvas;
  DiaCanvasItem *point_item;
  gdouble zf = view->zoom_factor;
  gdouble x = event->x / zf + canvas->extents.left;
  gdouble y = event->y / zf + canvas->extents.top;

  view->canvas->focus_view = view;
  gtk_widget_grab_focus(widget);

  point_item = dia_canvas_get_item_at(view->canvas, x, y, 0);
  if (!point_item)
    point_item = canvas->root;

  view->event_item = point_item;
  return emit_event(view, (GdkEvent *)event, view->event_item);
}

static gint
dia_canvas_view_leave_notify (GtkWidget *widget, GdkEventCrossing *event)
{
  DiaCanvasView *view = DIA_CANVAS_VIEW(widget);
  gboolean ret = FALSE;

  //view->canvas->focus_view = NULL;

  if (view->event_item)
    ret = emit_event(view, (GdkEvent *)event, view->event_item);
  view->event_item = NULL;
  return ret;
}

static void
dia_canvas_view_real_set_zoom(DiaCanvasView *view, gdouble zoom)
{
  /* default handling of zooms.  Extra handling may include updating GCs */
  if( GTK_IS_VIEWPORT(GTK_WIDGET(view)->parent) ) {
    GtkAdjustment *adjustment;

    adjustment = gtk_viewport_get_hadjustment(GTK_VIEWPORT(GTK_WIDGET(view)->parent));
    gtk_adjustment_set_value( adjustment, adjustment->value + (1+adjustment->page_size)*(zoom - view->zoom_factor)/2.0);
    adjustment = gtk_viewport_get_vadjustment(GTK_VIEWPORT(GTK_WIDGET(view)->parent));
    gtk_adjustment_set_value( adjustment, adjustment->value + (1+adjustment->page_size)*(zoom - view->zoom_factor)/2.0);
  }

  /* dirty whole area and queue a resize */
  view->zoom_factor = zoom;
  gtk_widget_queue_draw(GTK_WIDGET(view));
  gtk_widget_queue_resize(GTK_WIDGET(view));
}

/**
 * dia_canvas_view_set_zoom:
 * @view: a #DiaCanvasView.
 * @zoom: the zoom value.
 *
 * Sets the @zoom value to a @view.
 **/
void
dia_canvas_view_set_zoom(DiaCanvasView *view, gdouble zoom)
{
  g_return_if_fail(view != NULL);
  g_return_if_fail(DIA_IS_CANVAS_VIEW(view));
  g_return_if_fail(view->canvas != NULL);
  g_return_if_fail(0.0 < zoom);

  if (view->zoom_factor != zoom)
    g_signal_emit(view, canvas_view_signals[SET_ZOOM], 0, zoom);
}

static void
dirty_region(DiaCanvas *canvas, DiaRectangle *rect, DiaCanvasView *view)
{
  GdkRectangle grect;

  g_return_if_fail(canvas != NULL);
  g_return_if_fail(DIA_IS_CANVAS(canvas));
  g_return_if_fail(rect != NULL);
  g_return_if_fail(view != NULL);
  g_return_if_fail(DIA_IS_CANVAS_VIEW(view));
  g_return_if_fail(canvas == view->canvas);

  /* if we have no window, then don't redraw */
  if (GTK_WIDGET(view)->window == NULL)
    return;

  grect.x = (rect->left - view->canvas->extents.left) * view->zoom_factor-1;
  grect.y = (rect->top - view->canvas->extents.top) * view->zoom_factor-1;
  grect.width = (rect->right - rect->left) * view->zoom_factor+2;
  grect.height = (rect->bottom - rect->top) * view->zoom_factor+2;

  gdk_window_invalidate_rect (GTK_WIDGET(view)->window, &grect, TRUE);
}

/**
 * dia_canvas_view_set_canvas:
 * @view: a #DiaCanvasView.
 * @canvas: a #DiaCanvas.
 *
 * Sets the @canvas to a @view.
 **/
void
dia_canvas_view_set_canvas(DiaCanvasView *view, DiaCanvas *canvas)
{
  g_return_if_fail(view != NULL);
  g_return_if_fail(DIA_IS_CANVAS_VIEW(view));
  g_return_if_fail(canvas != NULL);
  g_return_if_fail(DIA_IS_CANVAS(canvas));
  g_return_if_fail(view->canvas == NULL);

  /* view holds a reference to the canvas */
  g_object_ref(G_OBJECT(canvas));
  view->canvas = canvas;
  dia_canvas_add_view(canvas, view);

  g_signal_connect(canvas, "dirty_region", G_CALLBACK(dirty_region), view);

  /* resize and redraw */
  gtk_widget_queue_resize(GTK_WIDGET(view));
  gtk_widget_queue_draw(GTK_WIDGET(view));
}

void
dia_canvas_view_grab( DiaCanvasView *view, gboolean exclusive )
{
  GdkCursor *fleur;

  fleur = gdk_cursor_new (GDK_FLEUR);
  if( exclusive == TRUE ) {
    gdk_pointer_grab(GTK_WIDGET(view)->window,
		     TRUE, GDK_POINTER_MOTION_MASK | GDK_BUTTON_RELEASE_MASK | GDK_BUTTON_PRESS_MASK,
		     GTK_WIDGET(view)->window,
		     fleur,
		     GDK_CURRENT_TIME);
  } else {
    gdk_pointer_grab(GTK_WIDGET(view)->window,
		     FALSE, GDK_POINTER_MOTION_MASK | GDK_BUTTON_RELEASE_MASK | GDK_BUTTON_PRESS_MASK,
		     NULL,
		     fleur,
		     GDK_CURRENT_TIME);
  }
  GTK_WIDGET_SET_FLAGS(view,GTK_HAS_GRAB);
  gdk_cursor_destroy (fleur);
}

void
dia_canvas_view_ungrab( DiaCanvasView *view )
{
  gdk_pointer_ungrab(GDK_CURRENT_TIME);
  GTK_WIDGET_UNSET_FLAGS(view,GTK_HAS_GRAB);
}

void
dia_canvas_view_real_to_canvas( DiaCanvasView *view, gdouble *x, gdouble *y )
{
  *x = (*x + view->canvas->extents.left) / view->zoom_factor-1;
  *y = (*y + view->canvas->extents.top)  / view->zoom_factor-1;
}

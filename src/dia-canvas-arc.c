/* dia-canvas-arc.c
 * Copyright (C) 2001 Xavier Ordoquy
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "dia-canvas-arc.h"
#include "dia-canvas-group.h"
#include <math.h>

#define _(s) (s)
#define sqr(x) ((x)*(x))

/* properties */
enum {
  PROP_NONE,
  PROP_X,
  PROP_Y,
  PROP_WIDTH,
  PROP_HEIGHT,
  PROP_ANGLE1,
  PROP_ANGLE2,
  PROP_FILL_COLOR,
  PROP_LINE_COLOR,
  PROP_LINE_WIDTH,
  PROP_TEST_FILL,
  PROP_TEST_LINE,
  PROP_LAST
};

static void dia_canvas_arc_init         (DiaCanvasArc *arc);
static void dia_canvas_arc_class_init   (DiaCanvasArcClass *klass);

static void   dia_canvas_arc_set_property (GObject *object,
					    guint property_id,
					    const GValue *value,
					    GParamSpec *pspec);
static void   dia_canvas_arc_get_property (GObject *object,
					    guint property_id,
					    GValue *value,
					    GParamSpec *pspec);

static void    dia_canvas_arc_real_render    (DiaCanvasItem *item,
					       DiaRenderer *renderer,
					       DiaRectangle *rect);
static gdouble dia_canvas_arc_real_distance  (DiaCanvasItem *item,
					       gdouble x, gdouble y);
static void    dia_canvas_arc_real_update    (DiaCanvasItem *item);
static void    dia_canvas_arc_real_move      (DiaCanvasItem *item,
					       gdouble dx, gdouble dy);

static GObjectClass *parent_class = NULL;

GType
dia_canvas_arc_get_type (void)
{
  static GtkType canvas_arc_type = 0;

  if (!canvas_arc_type)
    {
      static const GTypeInfo canvas_arc_info =
      {
        sizeof (DiaCanvasArcClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) dia_canvas_arc_class_init,
        (GClassFinalizeFunc) NULL,
        NULL,
        
        sizeof (DiaCanvasArc),
        0, /* n_preallocs */
        (GInstanceInitFunc) dia_canvas_arc_init,
      };

      canvas_arc_type = g_type_register_static (DIA_TYPE_CANVAS_ITEM,
						"DiaCanvasArc",
						&canvas_arc_info,
						0);
    }

  return canvas_arc_type;
}

static void
dia_canvas_arc_class_init (DiaCanvasArcClass *klass)
{
  GObjectClass *object_class;
  DiaCanvasItemClass *item_class;

  object_class = G_OBJECT_CLASS(klass);
  item_class   = DIA_CANVAS_ITEM_CLASS(klass);
  parent_class = g_type_class_ref(DIA_TYPE_CANVAS_ITEM);

  object_class->set_property = dia_canvas_arc_set_property;
  object_class->get_property = dia_canvas_arc_get_property;

  item_class->render     = dia_canvas_arc_real_render;
  item_class->update     = dia_canvas_arc_real_update;
  item_class->distance   = dia_canvas_arc_real_distance;
  item_class->move       = dia_canvas_arc_real_move;

  g_object_class_install_property (object_class,
				   PROP_X,
				   g_param_spec_double ("x", _("Center"),
							_("The position of the center"),
							-G_MAXDOUBLE,
							G_MAXDOUBLE,
							0.0,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_Y,
				   g_param_spec_double ("y", _("Center"),
							_("The position of the center"),
							-G_MAXDOUBLE,
							G_MAXDOUBLE,
							0.0,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_WIDTH,
				   g_param_spec_double ("width", _("Width"),
							_("The width of the arc"),
							-G_MAXDOUBLE,
							G_MAXDOUBLE,
							0.0,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_HEIGHT,
				   g_param_spec_double ("height", _("Height"),
							_("The height of the arc"),
							-G_MAXDOUBLE,
							G_MAXDOUBLE,
							0.0,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_ANGLE1,
				   g_param_spec_double ("angle1", _("The first angle"),
							_("The first angle in the arc"),
							-G_MAXDOUBLE,
							G_MAXDOUBLE,
							0.0,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_ANGLE2,
				   g_param_spec_double ("angle2", _("The second angle"),
							_("The second angle in the arc"),
							-G_MAXDOUBLE,
							G_MAXDOUBLE,
							0.0,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_FILL_COLOR,
				   g_param_spec_boxed ("fill_color", _("Fill color"),
						       _("The fill color of the rectangle (or NULL for empty)"),
						       GDK_TYPE_COLOR,
						       G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_LINE_COLOR,
				   g_param_spec_boxed ("line_color", _("Line color"),
						       _("The color of the line around the arc"),
						       GDK_TYPE_COLOR,
						       G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_LINE_WIDTH,
				   g_param_spec_double ("line_width", _("Line width"),
							_("The thickness of line around the arc"),
							0, G_MAXDOUBLE, 0,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_TEST_FILL,
				   g_param_spec_boolean ("test_fill", _("Test fill"),
							 _("Test if the rectangle is filled or not"),
							 FALSE,
							 G_PARAM_READABLE));
  g_object_class_install_property (object_class,
				   PROP_TEST_LINE,
				   g_param_spec_boolean ("test_line", _("Test line"),
							 _("Test if the rectangle has border or not"),
							 FALSE,
							 G_PARAM_READABLE));
}

static void
dia_canvas_arc_init (DiaCanvasArc *arc)
{
  GdkColor red   = { 0, 0xffff, 0, 0 };

  arc->line_color = red;
  arc->line_width = 1;
}

static void
dia_canvas_arc_set_property (GObject *object, guint property_id,
			      const GValue *value, GParamSpec *pspec)
{
  DiaCanvasArc *arc = DIA_CANVAS_ARC(object);
  gdouble val;
  GdkColor *color;

  switch (property_id)
    {
    case PROP_X:
      val = g_value_get_double(value);
      if (val != arc->center.x) {
	arc->center.x = val;
	dia_canvas_item_request_update(DIA_CANVAS_ITEM(arc));
      }
      break;
    case PROP_Y:
      val = g_value_get_double(value);
      if (val != arc->center.y) {
	arc->center.y = val;
	dia_canvas_item_request_update(DIA_CANVAS_ITEM(arc));
      }
      break;
    case PROP_WIDTH:
      val = g_value_get_double(value);
      if (val != arc->width) {
	arc->width = val;
	dia_canvas_item_request_update(DIA_CANVAS_ITEM(arc));
      }
      break;
    case PROP_HEIGHT:
      val = g_value_get_double(value);
      if (val != arc->height) {
	arc->height = val;
	dia_canvas_item_request_update(DIA_CANVAS_ITEM(arc));
      }
      break;
    case PROP_ANGLE1:
      val = g_value_get_double(value);
      if (val != arc->angle1) {
	arc->angle1 = val;
	dia_canvas_item_request_update(DIA_CANVAS_ITEM(arc));
      }
      break;
    case PROP_ANGLE2:
      val = g_value_get_double(value);
      if (val != arc->angle2) {
	arc->angle2 = val;
	dia_canvas_item_request_update(DIA_CANVAS_ITEM(arc));
      }
      break;
    case PROP_FILL_COLOR:
      color = g_value_get_boxed(value);
      if (color) {
	arc->fill_color = *color;
	arc->fill = TRUE;
      } else {
	arc->fill = FALSE;
      }
      dia_canvas_item_request_update(DIA_CANVAS_ITEM(arc));
      break;
    case PROP_LINE_COLOR:
      color = g_value_get_boxed(value);
      if (color) {
	arc->line_color = *color;
	arc->line = TRUE;
      } else {
	arc->line = FALSE;
      }
      dia_canvas_item_request_update(DIA_CANVAS_ITEM(arc));
      break;
    case PROP_LINE_WIDTH:
      val = g_value_get_double(value);
      arc->line_width = val;
      dia_canvas_item_request_update(DIA_CANVAS_ITEM(arc));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
dia_canvas_arc_get_property (GObject *object, guint property_id,
				   GValue *value, GParamSpec *pspec)
{
  DiaCanvasArc *arc = DIA_CANVAS_ARC(object);

  switch (property_id)
    {
    case PROP_X:
      g_value_set_double(value, arc->center.x);
      break;
    case PROP_Y:
      g_value_set_double(value, arc->center.y);
      break;
    case PROP_WIDTH:
      g_value_set_double(value, arc->width);
      break;
    case PROP_HEIGHT:
      g_value_set_double(value, arc->height);
      break;
    case PROP_ANGLE1:
      g_value_set_double(value, arc->angle1);
      break;
    case PROP_ANGLE2:
      g_value_set_double(value, arc->angle2);
      break;
    case PROP_FILL_COLOR:
      if (arc->fill)
	g_value_set_boxed(value, &arc->fill_color);
      else
	g_value_set_boxed(value, NULL);
      break;
    case PROP_LINE_COLOR:
      if (arc->line)
	g_value_set_boxed(value, &arc->line_color);
      else
	g_value_set_boxed(value, NULL);
      break;
    case PROP_LINE_WIDTH:
      g_value_set_double(value, arc->line_width);
      break;
    case PROP_TEST_FILL:
      if (arc->fill)
	g_value_set_boolean(value, TRUE);
      else
	g_value_set_boolean(value, FALSE);
      break;
    case PROP_TEST_LINE:
      if (arc->line)
	g_value_set_boolean(value, TRUE);
      else
	g_value_set_boolean(value, FALSE);      
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
dia_canvas_arc_real_render (DiaCanvasItem *item, DiaRenderer *renderer,
			     DiaRectangle *rect)
{
  DiaCanvasArc *arc = DIA_CANVAS_ARC(item);

  if( arc->fill ) {
    dia_renderer_set_color(renderer, &arc->fill_color);
    dia_renderer_draw_arc(renderer, TRUE, &(arc->center), arc->width, arc->height, arc->angle1, arc->angle2);
  }
  dia_renderer_set_color(renderer, &arc->line_color);
  dia_renderer_set_line_width(renderer, arc->line_width);
  dia_renderer_draw_arc(renderer, FALSE, &(arc->center), arc->width, arc->height, arc->angle1, arc->angle2);
}

static void
dia_canvas_arc_real_update (DiaCanvasItem *item)
{
  DiaCanvasArc *arc = DIA_CANVAS_ARC(item);
  gdouble half_width, tmp;

  /* the arc may have been moved, so dirty its former location */
  dia_canvas_dirty_region(item->canvas, &item->bounds);

  item->bounds.left   = arc->center.x + (arc->width  / 2.0);
  item->bounds.top    = arc->center.y - (arc->height / 2.0);
  item->bounds.right  = arc->center.x - (arc->width  / 2.0);
  item->bounds.bottom = arc->center.y + (arc->height / 2.0);

  if( item->bounds.left > item->bounds.right ) {
    tmp = item->bounds.left;
    item->bounds.left = item->bounds.right;
    item->bounds.right = tmp;
  }
  if( item->bounds.top > item->bounds.bottom ) {
    tmp = item->bounds.top;
    item->bounds.top = item->bounds.bottom;
    item->bounds.bottom = tmp;
  }
    
  half_width = arc->line_width / 2;

  item->bounds.left   -= half_width;
  item->bounds.top    -= half_width;
  item->bounds.right  += half_width;
  item->bounds.bottom += half_width;

  /* dirty the new location to redraw ... */
  dia_canvas_dirty_region(item->canvas, &item->bounds);
}

static gdouble
dia_canvas_arc_real_distance (DiaCanvasItem *item, gdouble x,gdouble y)
{
  DiaCanvasArc *arc = DIA_CANVAS_ARC(item);

  //DiaPoint *endpoints;
  DiaPoint from_center;
  gdouble angle;
  gdouble d;
  gdouble radius;

  from_center.x = x - arc->center.x;
  from_center.y = y - arc->center.y;

  angle = -atan2(from_center.y, from_center.x)*180.0/M_PI;
  if (angle<0)
    angle+=360.0;

  if( arc->angle1 > arc->angle2 ) {  /* passes 360 degrees */
    angle += 360.0;
    if( angle < arc->angle1 )
      angle += 360;
  }

  radius = ((arc->width/2.0)*(arc->height/2.0)) /
    (sqrt(sqr((arc->height/2.0) * cos(angle*(M_PI/180.0))) +
	  sqr((arc->width/2.0) * sin(angle*(M_PI/180.0)))));

  if( ( angle >= arc->angle1 ) &&
      ( angle <= arc->angle2 ) ) {
    d = sqrt(sqr(from_center.x)+sqr(from_center.y)) - radius;
    if( arc->fill ) {
      d -= arc->line_width/2.0;
      if( d < 0.0 )
	d = 0.0;
      return d;
    } else {
      d = fabs(d);
      d -= arc->line_width/2.0;
      if (d<0)
	d = 0.0;
      return d;
    }
  } else {
    /*
    d = distance_point_point(&endpoints[0], point);
    d2 = distance_point_point(&endpoints[1], point);

    return MIN(d,d2);
    */
    return G_MAXDOUBLE;
  }
}

static void
dia_canvas_arc_real_move (DiaCanvasItem *item,
			   gdouble dx, gdouble dy)
{
  DiaCanvasArc *arc = DIA_CANVAS_ARC(item);

  arc->center.x += dx;
  arc->center.y += dy;

  dia_canvas_item_request_update(DIA_CANVAS_ITEM(arc));
}

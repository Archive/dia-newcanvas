/* dia-canvas-attracted.h
 * Copyright (C) 2001 Xavier Ordoquy
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __DIA_CANVAS_ATTRACTED_H__
#define __DIA_CANVAS_ATTRACTED_H__

#include <gtk/gtk.h>
#include "dia-canvas-item.h"

#define DIA_TYPE_CANVAS_ATTRACTED		(dia_canvas_attracted_get_type ())
#define DIA_CANVAS_ATTRACTED(obj)	        (G_TYPE_CHECK_INSTANCE_CAST ((obj), DIA_TYPE_CANVAS_ATTRACTED, DiaCanvasAttracted))
#define DIA_CANVAS_ATTRACTED_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass),  DIA_TYPE_CANVAS_ATTRACTED, DiaCanvasAttractedClass))
#define DIA_IS_CANVAS_ATTRACTED(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), DIA_TYPE_CANVAS_ATTRACTED))
#define DIA_IS_CANVAS_ATTRACTED_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((obj),    DIA_TYPE_CANVAS_ATTRACTED))
#define DIA_CANVAS_ATTRACTED_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj),   DIA_TYPE_CANVAS_ATTRACTED, DiaCanvasAttractedClass))

typedef struct _DiaCanvasAttracted DiaCanvasAttracted;
typedef struct _DiaCanvasAttractedClass DiaCanvasAttractedClass;

struct _DiaCanvasAttracted
{
  DiaCanvasItem item;

  /* Position of the attracted point */
  DiaPoint pos;
};

struct _DiaCanvasAttractedClass
{
  DiaCanvasItemClass parent_class;
};

GType dia_canvas_attracted_get_type (void);

#endif

/* dia-canvas-polygon.c
 * Copyright (C) 2001 Xavier Ordoquy
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "dia-canvas-polygon.h"
#include "dia-canvas-group.h"
#include <stdio.h>

#define _(s) (s)
#define sqr(x) ((x)*(x))

static guint line_crosses_ray(const DiaPoint *line_start, 
			      const DiaPoint *line_end,
			      const DiaPoint *rayend);

/* properties */
enum {
  PROP_NONE,
  PROP_X,
  PROP_Y,
  PROP_POINT,
  PROP_FILL_COLOR,
  PROP_LINE_COLOR,
  PROP_LINE_WIDTH,
  PROP_LAST
};

static void dia_canvas_polygon_init         (DiaCanvasPolygon *polygon);
static void dia_canvas_polygon_class_init   (DiaCanvasPolygonClass *klass);

static void   dia_canvas_polygon_set_property (GObject *object,
					       guint property_id,
					       const GValue *value,
					       GParamSpec *pspec);
static void   dia_canvas_polygon_get_property (GObject *object,
					       guint property_id,
					       GValue *value,
					       GParamSpec *pspec);

static void    dia_canvas_polygon_real_render    (DiaCanvasItem *item,
						  DiaRenderer *renderer,
						  DiaRectangle *rect);
static gdouble dia_canvas_polygon_real_distance  (DiaCanvasItem *item,
						  gdouble x, gdouble y);
static void    dia_canvas_polygon_real_update    (DiaCanvasItem *item);
static void    dia_canvas_polygon_real_move      (DiaCanvasItem *item,
						  gdouble dx, gdouble dy);

static GObjectClass *parent_class = NULL;

GType
dia_canvas_polygon_get_type (void)
{
  static GtkType canvas_polygon_type = 0;

  if (!canvas_polygon_type)
    {
      static const GTypeInfo canvas_polygon_info =
      {
        sizeof (DiaCanvasPolygonClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) dia_canvas_polygon_class_init,
        (GClassFinalizeFunc) NULL,
        NULL,
        
        sizeof (DiaCanvasPolygon),
        0, /* n_preallocs */
        (GInstanceInitFunc) dia_canvas_polygon_init,
      };

      canvas_polygon_type = g_type_register_static (DIA_TYPE_CANVAS_ITEM,
						      "DiaCanvasPolygon",
						      &canvas_polygon_info,
						      0);
    }
  return canvas_polygon_type;
}

static void
dia_canvas_polygon_class_init (DiaCanvasPolygonClass *klass)
{
  GObjectClass *object_class;
  DiaCanvasItemClass *item_class;

  object_class = G_OBJECT_CLASS(klass);
  item_class   = DIA_CANVAS_ITEM_CLASS(klass);
  parent_class = g_type_class_ref(DIA_TYPE_CANVAS_ITEM);

  object_class->set_property = dia_canvas_polygon_set_property;
  object_class->get_property = dia_canvas_polygon_get_property;

  item_class->render     = dia_canvas_polygon_real_render;
  item_class->update     = dia_canvas_polygon_real_update;
  item_class->distance   = dia_canvas_polygon_real_distance;
  item_class->move       = dia_canvas_polygon_real_move;

  g_object_class_install_property (object_class,
				   PROP_X,
				   g_param_spec_double ("x", _("Position"),
							_("The position of the polygon"),
							-G_MAXDOUBLE,
							G_MAXDOUBLE,
							0.0,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_Y,
				   g_param_spec_double ("y", _("Position"),
							_("The position of the polygon"),
							-G_MAXDOUBLE,
							G_MAXDOUBLE,
							0,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_POINT,
				   g_param_spec_string ("point", _("Add point"),
							_("Add a point to the polygon"),
							"(0.0,0.0)",
							G_PARAM_WRITABLE));
  g_object_class_install_property (object_class,
				   PROP_FILL_COLOR,
				   g_param_spec_boxed ("fill_color", _("Fill color"),
						       _("The fill color of the rectangle (or NULL for empty)"),
						       GDK_TYPE_COLOR,
						       G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_LINE_COLOR,
				   g_param_spec_boxed ("line_color", _("Polygon line color"),
						       _("The line color of the polygon"),
						       GDK_TYPE_COLOR,
						       G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_LINE_WIDTH,
				   g_param_spec_double ("line_width", _("Line width"),
							_("The thickness of line"),
							0, G_MAXDOUBLE, 0,
							G_PARAM_READWRITE));
}

static void
dia_canvas_polygon_init (DiaCanvasPolygon *polygon)
{
  polygon->line_width = 1;
}

static void
dia_canvas_polygon_set_property (GObject *object, guint property_id,
			      const GValue *value, GParamSpec *pspec)
{
  DiaCanvasPolygon *polygon = DIA_CANVAS_POLYGON(object);
  gdouble   val, oldval;
  gint      i;
  DiaPoint *point;
  guchar   *string;
  GdkColor *color;

  switch (property_id)
    {
    case PROP_X:
      val = g_value_get_double(value);
      if (val != polygon->position.x) {
	oldval = polygon->position.x;
	polygon->position.x = val;
	for( i=0; i<g_list_length(polygon->list); i++ ) {
	  point = g_list_nth_data(polygon->list,i);
	  point->x += val - oldval;
	}
	dia_canvas_item_request_update(DIA_CANVAS_ITEM(polygon));
      }
      break;
    case PROP_Y:
      val = g_value_get_double(value);
      if (val != polygon->position.y) {
	oldval = polygon->position.y;
	polygon->position.y = val;
	for( i=0; i<g_list_length(polygon->list); i++ ) {
	  point = g_list_nth_data(polygon->list,i);
	  point->y += val - oldval;
	}
	dia_canvas_item_request_update(DIA_CANVAS_ITEM(polygon));
      }
      break;
    case PROP_POINT:
      string = g_value_dup_string(value);
      point = g_new0(DiaPoint,1);
      if( sscanf(string,"(%lf,%lf)",&(point->x),&(point->y)) == 2 ) {
	point->x += polygon->position.x;
	point->y += polygon->position.y;
	polygon->list = g_list_append(polygon->list,point);
	dia_canvas_item_request_update(DIA_CANVAS_ITEM(polygon));
      } else {
	g_free(point);
      }
      break;
    case PROP_FILL_COLOR:
      color = g_value_get_boxed(value);
      if (color) {
	polygon->fill_color = *color;
	polygon->fill = TRUE;
      } else {
	polygon->fill = FALSE;	
      }
      dia_canvas_item_request_update(DIA_CANVAS_ITEM(polygon));
      break;
    case PROP_LINE_COLOR:
      color = g_value_get_boxed(value);
      if (color) {
	polygon->line_color = *color;
	polygon->line = TRUE;
      } else {
	polygon->line = FALSE;
      }
      dia_canvas_item_request_update(DIA_CANVAS_ITEM(polygon));
      break;
    case PROP_LINE_WIDTH:
      val = g_value_get_double(value);
      if (val != polygon->line_width) {
	polygon->line_width = val;
	dia_canvas_item_request_update(DIA_CANVAS_ITEM(polygon));
      }
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
dia_canvas_polygon_get_property (GObject *object, guint property_id,
				   GValue *value, GParamSpec *pspec)
{
  DiaCanvasPolygon *polygon = DIA_CANVAS_POLYGON(object);

  switch (property_id)
    {
    case PROP_X:
      g_value_set_double(value, polygon->position.x);
      break;
    case PROP_Y:
      g_value_set_double(value, polygon->position.y);
      break;
    case PROP_FILL_COLOR:
      if( polygon->fill ) {
	g_value_set_boxed(value, &polygon->fill_color);
      } else {
	g_value_set_boxed(value, NULL);
      }
      break;
    case PROP_LINE_COLOR:
      if( polygon->line ) {
	g_value_set_boxed(value, &polygon->line_color);
      } else {
	g_value_set_boxed(value, NULL);
      }
      break;
    case PROP_LINE_WIDTH:
      g_value_set_double(value, polygon->line_width);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
dia_canvas_polygon_real_render (DiaCanvasItem *item, DiaRenderer *renderer,
			     DiaRectangle *rect)
{
  DiaCanvasPolygon *polygon = DIA_CANVAS_POLYGON(item);

  if( polygon->fill ) {
    dia_renderer_set_color(renderer, &polygon->fill_color);
    dia_renderer_draw_polygon(renderer, TRUE, polygon->list);
  }

  if( polygon->line ) {
    dia_renderer_set_color(renderer, &polygon->line_color);
    dia_renderer_set_line_width(renderer, polygon->line_width);
    dia_renderer_draw_polygon(renderer, FALSE, polygon->list);
  }
}

static void
dia_canvas_polygon_real_update (DiaCanvasItem *item)
{
  DiaCanvasPolygon *polygon = DIA_CANVAS_POLYGON(item);
  gdouble half_width;
  DiaPoint *point;
  gint i;

  /* the polygon may have been moved, so dirty its former location */
  dia_canvas_dirty_region(item->canvas, &item->bounds);

  item->bounds.left   = polygon->position.x;
  item->bounds.top    = polygon->position.y;
  item->bounds.right  = polygon->position.x;
  item->bounds.bottom = polygon->position.y;

  for( i=0; i<g_list_length(polygon->list); i++ ) {
    point = g_list_nth_data(polygon->list,i);
    if( item->bounds.left > point->x ) {
      item->bounds.left = point->x;
    }
    if( item->bounds.top > point->y ) {
      item->bounds.top = point->y;
    }
    if( item->bounds.right < point->x ) {
      item->bounds.right = point->x;
    }
    if( item->bounds.bottom < point->y ) {
      item->bounds.bottom = point->y;
    }
  }

  half_width = polygon->line_width / 2;

  item->bounds.left   -= half_width;
  item->bounds.top    -= half_width;
  item->bounds.right  += half_width;
  item->bounds.bottom += half_width;

  /* dirty the new location to redraw ... */
  dia_canvas_dirty_region(item->canvas, &item->bounds);
}

static gdouble
dia_canvas_polygon_real_distance (DiaCanvasItem *item, gdouble x,gdouble y)
{
  /*
  guint i, last = npoints - 1;
  */

  gint i, npoints, crossings;
  gdouble line_dist = G_MAXFLOAT;
  DiaCanvasPolygon *polygon = DIA_CANVAS_POLYGON(item);
  DiaPoint point, *last;

  npoints = g_list_length(polygon->list);
  point.x = x;
  point.y = y;
  crossings = 0;
  last = g_list_nth_data(polygon->list,npoints-1);

  /* calculate ray crossings and line distances */
  for (i = 0; i < npoints; i++) {
    gdouble dist;

    crossings += line_crosses_ray(last, g_list_nth_data(polygon->list,i), &point);
    dist = distance_line_point(last, g_list_nth_data(polygon->list,i),
			       polygon->line_width, &point);
    line_dist = MIN(line_dist, dist);
    last = g_list_nth_data(polygon->list,i);
  }
  /* If there is an odd number of ray crossings, we are inside the polygon.
   * Otherwise, return the minium distance from a line segment */
  if (crossings % 2 == 1)
    return 0.0;
  else
    return line_dist;
}

static void
dia_canvas_polygon_real_move (DiaCanvasItem *item,
			   gdouble dx, gdouble dy)
{
  gint i;
  DiaPoint *point;
  DiaCanvasPolygon *polygon = DIA_CANVAS_POLYGON(item);

  polygon->position.x += dx;
  polygon->position.y += dy;

  for( i=0; i<g_list_length(polygon->list); i++ ) {
    point = g_list_nth_data(polygon->list,i);
    point->x += dx;
    point->y += dy;
  }

  dia_canvas_item_request_update(DIA_CANVAS_ITEM(polygon));
}

static guint
line_crosses_ray(const DiaPoint *line_start, 
                 const DiaPoint *line_end,
		 const DiaPoint *rayend)
{
  gdouble xpos;

  /* swap end points if necessary */
  if (line_start->y > line_end->y) {
    const DiaPoint *tmp;

    tmp = line_start;
    line_start = line_end;
    line_end = tmp;
  }
  /* if y coords of line do not include rayend.y */
  if (line_start->y > rayend->y || line_end->y < rayend->y)
    return 0;
  /* Avoid division by zero */
  if (line_end->y - line_start->y < 0.00000000001) {
    return line_end->y - rayend->y < 0.00000000001;
  }
  xpos = line_start->x + (rayend->y - line_start->y) * 
    (line_end->x - line_start->x) / (line_end->y - line_start->y);
  return xpos <= rayend->x;
}


#ifndef __dia_marshal_MARSHAL_H__
#define __dia_marshal_MARSHAL_H__

#include	<glib-object.h>

G_BEGIN_DECLS

/* VOID:POINTER,POINTER (dia-marshal.list:1) */
extern void dia_marshal_VOID__POINTER_POINTER (GClosure     *closure,
                                               GValue       *return_value,
                                               guint         n_param_values,
                                               const GValue *param_values,
                                               gpointer      invocation_hint,
                                               gpointer      marshal_data);

/* VOID:DOUBLE (dia-marshal.list:2) */
#define dia_marshal_VOID__DOUBLE	g_cclosure_marshal_VOID__DOUBLE

/* VOID:OBJECT,POINTER (dia-marshal.list:3) */
extern void dia_marshal_VOID__OBJECT_POINTER (GClosure     *closure,
                                              GValue       *return_value,
                                              guint         n_param_values,
                                              const GValue *param_values,
                                              gpointer      invocation_hint,
                                              gpointer      marshal_data);

/* BOOLEAN:BOXED,OBJECT (dia-marshal.list:4) */
extern void dia_marshal_BOOLEAN__BOXED_OBJECT (GClosure     *closure,
                                               GValue       *return_value,
                                               guint         n_param_values,
                                               const GValue *param_values,
                                               gpointer      invocation_hint,
                                               gpointer      marshal_data);

/* BOOLEAN:BOXED,OBJECT,POINTER (dia-marshal.list:5) */
extern void dia_marshal_BOOLEAN__BOXED_OBJECT_POINTER (GClosure     *closure,
                                                       GValue       *return_value,
                                                       guint         n_param_values,
                                                       const GValue *param_values,
                                                       gpointer      invocation_hint,
                                                       gpointer      marshal_data);

/* DOUBLE:DOUBLE,DOUBLE (dia-marshal.list:6) */
extern void dia_marshal_DOUBLE__DOUBLE_DOUBLE (GClosure     *closure,
                                               GValue       *return_value,
                                               guint         n_param_values,
                                               const GValue *param_values,
                                               gpointer      invocation_hint,
                                               gpointer      marshal_data);

/* BOOLEAN:POINTER,POINTER (dia-marshal.list:7) */
extern void dia_marshal_BOOLEAN__POINTER_POINTER (GClosure     *closure,
                                                  GValue       *return_value,
                                                  guint         n_param_values,
                                                  const GValue *param_values,
                                                  gpointer      invocation_hint,
                                                  gpointer      marshal_data);

/* DOUBLE:POINTER,POINTER (dia-marshal.list:8) */
extern void dia_marshal_DOUBLE__POINTER_POINTER (GClosure     *closure,
                                                 GValue       *return_value,
                                                 guint         n_param_values,
                                                 const GValue *param_values,
                                                 gpointer      invocation_hint,
                                                 gpointer      marshal_data);

/* VOID:DOUBLE,DOUBLE (dia-marshal.list:9) */
extern void dia_marshal_VOID__DOUBLE_DOUBLE (GClosure     *closure,
                                             GValue       *return_value,
                                             guint         n_param_values,
                                             const GValue *param_values,
                                             gpointer      invocation_hint,
                                             gpointer      marshal_data);

/* DOUBLE:OBJECT,POINTER,POINTER (dia-marshal.list:10) */
extern void dia_marshal_DOUBLE__OBJECT_POINTER_POINTER (GClosure     *closure,
                                                        GValue       *return_value,
                                                        guint         n_param_values,
                                                        const GValue *param_values,
                                                        gpointer      invocation_hint,
                                                        gpointer      marshal_data);

/* VOID:OBJECT,DOUBLE,DOUBLE (dia-marshal.list:11) */
extern void dia_marshal_VOID__OBJECT_DOUBLE_DOUBLE (GClosure     *closure,
                                                    GValue       *return_value,
                                                    guint         n_param_values,
                                                    const GValue *param_values,
                                                    gpointer      invocation_hint,
                                                    gpointer      marshal_data);

/* VOID:OBJECT (dia-marshal.list:12) */
#define dia_marshal_VOID__OBJECT	g_cclosure_marshal_VOID__OBJECT

/* VOID:VOID (dia-marshal.list:13) */
#define dia_marshal_VOID__VOID	g_cclosure_marshal_VOID__VOID

/* VOID:BOOLEAN (dia-marshal.list:14) */
#define dia_marshal_VOID__BOOLEAN	g_cclosure_marshal_VOID__BOOLEAN

G_END_DECLS

#endif /* __dia_marshal_MARSHAL_H__ */


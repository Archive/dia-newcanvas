/* -*- Mode: C; c-basic-offset: 4 -*- */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/* include this first, before NO_IMPORT_PYGOBJECT is defined */
//#include <pygtk/pygtk.h>
#include <pygobject.h>

void pydianewcanvas_register_classes (PyObject *d);
void pydianewcanvas_add_constants(PyObject *module, const gchar *strip_prefix);

extern PyMethodDef pydianewcanvas_functions[];

DL_EXPORT(void)
initdianewcanvas(void)
{
    PyObject *m, *d;

    init_pygobject ();
    //init_pygtk ();

    m = Py_InitModule ("dianewcanvas", pydianewcanvas_functions);
    d = PyModule_GetDict (m);
	
    pydianewcanvas_register_classes (d);
    //pydianewcanvas_add_constants(m, "DIANEWCANVAS_");    

    if (PyErr_Occurred ()) {
	Py_FatalError ("can't initialise module dianewcanvas");
    }
}

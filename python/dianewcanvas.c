/* -*- Mode: C; c-basic-offset: 4 -*- */

#include <Python.h>



#line 4 "dianewcanvas.override"
#include <Python.h>

#include <pygtk/pygtk.h>
#include <dia-canvas-arc.h>
#include <dia-canvas-attract.h>
#include <dia-canvas-ellipse.h>
#include <dia-canvas-item.h>
#include <dia-canvas-view-aa.h>
#include <dia-canvas.h>
#include <dia-canvas-attracted.h>
#include <dia-canvas-grid.h>
#include <dia-canvas-line.h>
#include <dia-canvas-view-gdk.h>
#include <dia-geometry.h>
#include <dia-renderer.h>
#include <dia-canvas-connection.h>
#include <dia-canvas-group.h>
#include <dia-canvas-rectangle.h>
#include <dia-canvas-view.h>
#include <dia-marshal.h>
#include <dia-canvas-point.h>
#include <dia-canvas-polygon.h>
#include <dia-canvas-text.h>
#line 32 "dianewcanvas.c"


/* ---------- types from other modules ---------- */
static PyTypeObject *_PyGObject_Type;
#define PyGObject_Type (*_PyGObject_Type)
static PyTypeObject *_PyGtkWidget_Type;
#define PyGtkWidget_Type (*_PyGtkWidget_Type)


/* ---------- forward type declarations ---------- */
PyTypeObject PyDiaCanvas_Type;
PyTypeObject PyDiaCanvasItem_Type;
PyTypeObject PyDiaCanvasGroup_Type;
PyTypeObject PyDiaCanvasGrid_Type;
PyTypeObject PyDiaCanvasConnection_Type;
PyTypeObject PyDiaCanvasAttracted_Type;
PyTypeObject PyDiaCanvasAttract_Type;
PyTypeObject PyDiaCanvasArc_Type;
PyTypeObject PyDiaCanvasLine_Type;
PyTypeObject PyDiaCanvasPoint_Type;
PyTypeObject PyDiaCanvasPolygon_Type;
PyTypeObject PyDiaCanvasRectangle_Type;
PyTypeObject PyDiaCanvasEllipse_Type;
PyTypeObject PyDiaCanvasText_Type;
PyTypeObject PyDiaCanvasView_Type;
PyTypeObject PyDiaCanvasViewGdk_Type;


/* ----------- DiaCanvas ----------- */

static int
_wrap_dia_canvas_new(PyGObject *self, PyObject *args, PyObject *kwargs)
{
    static char *kwlist[] = { NULL };

    if (!PyArg_ParseTupleAndKeywords(args, kwargs, ":DiaCanvas.__init__", kwlist))
        return -1;
    self->obj = (GObject *)dia_canvas_new();

    if (!self->obj) {
        PyErr_SetString(PyExc_RuntimeError, "could not create DiaCanvas object");
        return -1;
    }
    pygobject_register_wrapper((PyObject *)self);
    return 0;
}

static PyObject *
_wrap_dia_canvas_view_gdk_new(PyGObject *self)
{
    GtkWidget *ret;

    ret = dia_canvas_view_gdk_new(DIA_CANVAS(self->obj));
    /* pygobject_new handles NULL checking */
    return pygobject_new((GObject *)ret);
}

static PyObject *
_wrap_dia_canvas_add_view(PyGObject *self, PyObject *args, PyObject *kwargs)
{
    static char *kwlist[] = { "view", NULL };
    PyGObject *view;

    if (!PyArg_ParseTupleAndKeywords(args, kwargs, "O!:DiaCanvas.add_view", kwlist, &PyDiaCanvasView_Type, &view))
        return NULL;
    dia_canvas_add_view(DIA_CANVAS(self->obj), DIA_CANVAS_VIEW(view->obj));
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
_wrap_dia_canvas_remove_view(PyGObject *self, PyObject *args, PyObject *kwargs)
{
    static char *kwlist[] = { "view", NULL };
    PyGObject *view;

    if (!PyArg_ParseTupleAndKeywords(args, kwargs, "O!:DiaCanvas.remove_view", kwlist, &PyDiaCanvasView_Type, &view))
        return NULL;
    dia_canvas_remove_view(DIA_CANVAS(self->obj), DIA_CANVAS_VIEW(view->obj));
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
_wrap_dia_canvas_get_item_at(PyGObject *self, PyObject *args, PyObject *kwargs)
{
    static char *kwlist[] = { "x", "y", "close_enough", NULL };
    double x, y, close_enough;
    DiaCanvasItem *ret;

    if (!PyArg_ParseTupleAndKeywords(args, kwargs, "ddd:DiaCanvas.get_item_at", kwlist, &x, &y, &close_enough))
        return NULL;
    ret = dia_canvas_get_item_at(DIA_CANVAS(self->obj), x, y, close_enough);
    /* pygobject_new handles NULL checking */
    return pygobject_new((GObject *)ret);
}

static PyObject *
_wrap_dia_canvas_schedule_update(PyGObject *self)
{
    dia_canvas_schedule_update(DIA_CANVAS(self->obj));
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
_wrap_dia_canvas_ensure_update(PyGObject *self)
{
    dia_canvas_ensure_update(DIA_CANVAS(self->obj));
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
_wrap_dia_canvas_connect(PyGObject *self, PyObject *args, PyObject *kwargs)
{
    static char *kwlist[] = { "connection", NULL };
    PyGObject *connection;

    if (!PyArg_ParseTupleAndKeywords(args, kwargs, "O!:DiaCanvas.connect", kwlist, &PyDiaCanvasConnection_Type, &connection))
        return NULL;
    dia_canvas_connect(DIA_CANVAS(self->obj), DIA_CANVAS_CONNECTION(connection->obj));
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
_wrap_dia_canvas_add_selection(PyGObject *self, PyObject *args, PyObject *kwargs)
{
    static char *kwlist[] = { "item", NULL };
    PyGObject *item;

    if (!PyArg_ParseTupleAndKeywords(args, kwargs, "O!:DiaCanvas.add_selection", kwlist, &PyDiaCanvasItem_Type, &item))
        return NULL;
    dia_canvas_add_selection(DIA_CANVAS(self->obj), DIA_CANVAS_ITEM(item->obj));
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
_wrap_dia_canvas_remove_from_selection(PyGObject *self, PyObject *args, PyObject *kwargs)
{
    static char *kwlist[] = { "item", NULL };
    PyGObject *item;

    if (!PyArg_ParseTupleAndKeywords(args, kwargs, "O!:DiaCanvas.remove_from_selection", kwlist, &PyDiaCanvasItem_Type, &item))
        return NULL;
    dia_canvas_remove_from_selection(DIA_CANVAS(self->obj), DIA_CANVAS_ITEM(item->obj));
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
_wrap_dia_canvas_clear_selection(PyGObject *self)
{
    dia_canvas_clear_selection(DIA_CANVAS(self->obj));
    Py_INCREF(Py_None);
    return Py_None;
}

static PyMethodDef _PyDiaCanvas_methods[] = {
    { "view_gdk_new", (PyCFunction)_wrap_dia_canvas_view_gdk_new, METH_NOARGS },
    { "add_view", (PyCFunction)_wrap_dia_canvas_add_view, METH_VARARGS|METH_KEYWORDS },
    { "remove_view", (PyCFunction)_wrap_dia_canvas_remove_view, METH_VARARGS|METH_KEYWORDS },
    { "get_item_at", (PyCFunction)_wrap_dia_canvas_get_item_at, METH_VARARGS|METH_KEYWORDS },
    { "schedule_update", (PyCFunction)_wrap_dia_canvas_schedule_update, METH_NOARGS },
    { "ensure_update", (PyCFunction)_wrap_dia_canvas_ensure_update, METH_NOARGS },
    { "connect", (PyCFunction)_wrap_dia_canvas_connect, METH_VARARGS|METH_KEYWORDS },
    { "add_selection", (PyCFunction)_wrap_dia_canvas_add_selection, METH_VARARGS|METH_KEYWORDS },
    { "remove_from_selection", (PyCFunction)_wrap_dia_canvas_remove_from_selection, METH_VARARGS|METH_KEYWORDS },
    { "clear_selection", (PyCFunction)_wrap_dia_canvas_clear_selection, METH_NOARGS },
    { NULL, NULL, 0 }
};

PyTypeObject PyDiaCanvas_Type = {
    PyObject_HEAD_INIT(NULL)
    0,					/* ob_size */
    "dianewcanvas.Canvas",			/* tp_name */
    sizeof(PyGObject),	        /* tp_basicsize */
    0,					/* tp_itemsize */
    /* methods */
    (destructor)0,			/* tp_dealloc */
    (printfunc)0,			/* tp_print */
    (getattrfunc)0,	/* tp_getattr */
    (setattrfunc)0,	/* tp_setattr */
    (cmpfunc)0,		/* tp_compare */
    (reprfunc)0,		/* tp_repr */
    0,			/* tp_as_number */
    0,		/* tp_as_sequence */
    0,			/* tp_as_mapping */
    (hashfunc)0,		/* tp_hash */
    (ternaryfunc)0,		/* tp_call */
    (reprfunc)0,		/* tp_str */
    (getattrofunc)0,			/* tp_getattro */
    (setattrofunc)0,			/* tp_setattro */
    0,					/* tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /* tp_flags */
    NULL, 				/* Documentation string */
    (traverseproc)0,			/* tp_traverse */
    (inquiry)0,			/* tp_clear */
    (richcmpfunc)0,	/* tp_richcompare */
    offsetof(PyGObject, weakreflist),             /* tp_weaklistoffset */
    (getiterfunc)0,		/* tp_iter */
    (iternextfunc)0,	/* tp_iternext */
    _PyDiaCanvas_methods,			/* tp_methods */
    0,					/* tp_members */
    0,		       	/* tp_getset */
    NULL,				/* tp_base */
    NULL,				/* tp_dict */
    (descrgetfunc)0,	/* tp_descr_get */
    (descrsetfunc)0,	/* tp_descr_set */
    offsetof(PyGObject, inst_dict),                 /* tp_dictoffset */
    (initproc)_wrap_dia_canvas_new,		/* tp_init */
};



/* ----------- DiaCanvasItem ----------- */

static int
pygobject_no_constructor(PyObject *self, PyObject *args, PyObject *kwargs)
{
    gchar buf[512];

    g_snprintf(buf, sizeof(buf), "%s is an abstract widget", self->ob_type->tp_name);
    PyErr_SetString(PyExc_NotImplementedError, buf);
    return -1;
}

static PyObject *
_wrap_dia_canvas_item_request_update(PyGObject *self)
{
    dia_canvas_item_request_update(DIA_CANVAS_ITEM(self->obj));
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
_wrap_dia_canvas_item_sink(PyGObject *self)
{
    dia_canvas_item_sink(DIA_CANVAS_ITEM(self->obj));
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
_wrap_dia_canvas_item_show(PyGObject *self)
{
    dia_canvas_item_show(DIA_CANVAS_ITEM(self->obj));
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
_wrap_dia_canvas_item_hide(PyGObject *self)
{
    dia_canvas_item_hide(DIA_CANVAS_ITEM(self->obj));
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
_wrap_dia_canvas_item_move(PyGObject *self, PyObject *args, PyObject *kwargs)
{
    static char *kwlist[] = { "dx", "dy", NULL };
    double dx, dy;

    if (!PyArg_ParseTupleAndKeywords(args, kwargs, "dd:DiaCanvasItem.move", kwlist, &dx, &dy))
        return NULL;
    dia_canvas_item_move(DIA_CANVAS_ITEM(self->obj), dx, dy);
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
_wrap_dia_canvas_item_make_connection(PyGObject *self)
{
    dia_canvas_item_make_connection(DIA_CANVAS_ITEM(self->obj));
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
_wrap_dia_canvas_item_select(PyGObject *self)
{
    dia_canvas_item_select(DIA_CANVAS_ITEM(self->obj));
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
_wrap_dia_canvas_item_unselect(PyGObject *self)
{
    dia_canvas_item_unselect(DIA_CANVAS_ITEM(self->obj));
    Py_INCREF(Py_None);
    return Py_None;
}

static PyMethodDef _PyDiaCanvasItem_methods[] = {
    { "request_update", (PyCFunction)_wrap_dia_canvas_item_request_update, METH_NOARGS },
    { "sink", (PyCFunction)_wrap_dia_canvas_item_sink, METH_NOARGS },
    { "show", (PyCFunction)_wrap_dia_canvas_item_show, METH_NOARGS },
    { "hide", (PyCFunction)_wrap_dia_canvas_item_hide, METH_NOARGS },
    { "move", (PyCFunction)_wrap_dia_canvas_item_move, METH_VARARGS|METH_KEYWORDS },
    { "make_connection", (PyCFunction)_wrap_dia_canvas_item_make_connection, METH_NOARGS },
    { "select", (PyCFunction)_wrap_dia_canvas_item_select, METH_NOARGS },
    { "unselect", (PyCFunction)_wrap_dia_canvas_item_unselect, METH_NOARGS },
    { NULL, NULL, 0 }
};

PyTypeObject PyDiaCanvasItem_Type = {
    PyObject_HEAD_INIT(NULL)
    0,					/* ob_size */
    "dianewcanvas.CanvasItem",			/* tp_name */
    sizeof(PyGObject),	        /* tp_basicsize */
    0,					/* tp_itemsize */
    /* methods */
    (destructor)0,			/* tp_dealloc */
    (printfunc)0,			/* tp_print */
    (getattrfunc)0,	/* tp_getattr */
    (setattrfunc)0,	/* tp_setattr */
    (cmpfunc)0,		/* tp_compare */
    (reprfunc)0,		/* tp_repr */
    0,			/* tp_as_number */
    0,		/* tp_as_sequence */
    0,			/* tp_as_mapping */
    (hashfunc)0,		/* tp_hash */
    (ternaryfunc)0,		/* tp_call */
    (reprfunc)0,		/* tp_str */
    (getattrofunc)0,			/* tp_getattro */
    (setattrofunc)0,			/* tp_setattro */
    0,					/* tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /* tp_flags */
    NULL, 				/* Documentation string */
    (traverseproc)0,			/* tp_traverse */
    (inquiry)0,			/* tp_clear */
    (richcmpfunc)0,	/* tp_richcompare */
    offsetof(PyGObject, weakreflist),             /* tp_weaklistoffset */
    (getiterfunc)0,		/* tp_iter */
    (iternextfunc)0,	/* tp_iternext */
    _PyDiaCanvasItem_methods,			/* tp_methods */
    0,					/* tp_members */
    0,		       	/* tp_getset */
    NULL,				/* tp_base */
    NULL,				/* tp_dict */
    (descrgetfunc)0,	/* tp_descr_get */
    (descrsetfunc)0,	/* tp_descr_set */
    offsetof(PyGObject, inst_dict),                 /* tp_dictoffset */
    (initproc)pygobject_no_constructor,		/* tp_init */
};



/* ----------- DiaCanvasGroup ----------- */

static PyObject *
_wrap_dia_canvas_group_add_item(PyGObject *self, PyObject *args, PyObject *kwargs)
{
    static char *kwlist[] = { "item", NULL };
    PyGObject *item;

    if (!PyArg_ParseTupleAndKeywords(args, kwargs, "O!:DiaCanvasGroup.add_item", kwlist, &PyDiaCanvasItem_Type, &item))
        return NULL;
    dia_canvas_group_add_item(DIA_CANVAS_GROUP(self->obj), DIA_CANVAS_ITEM(item->obj));
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
_wrap_dia_canvas_group_remove_item(PyGObject *self, PyObject *args, PyObject *kwargs)
{
    static char *kwlist[] = { "item", NULL };
    PyGObject *item;

    if (!PyArg_ParseTupleAndKeywords(args, kwargs, "O!:DiaCanvasGroup.remove_item", kwlist, &PyDiaCanvasItem_Type, &item))
        return NULL;
    dia_canvas_group_remove_item(DIA_CANVAS_GROUP(self->obj), DIA_CANVAS_ITEM(item->obj));
    Py_INCREF(Py_None);
    return Py_None;
}

static PyMethodDef _PyDiaCanvasGroup_methods[] = {
    { "add_item", (PyCFunction)_wrap_dia_canvas_group_add_item, METH_VARARGS|METH_KEYWORDS },
    { "remove_item", (PyCFunction)_wrap_dia_canvas_group_remove_item, METH_VARARGS|METH_KEYWORDS },
    { NULL, NULL, 0 }
};

PyTypeObject PyDiaCanvasGroup_Type = {
    PyObject_HEAD_INIT(NULL)
    0,					/* ob_size */
    "dianewcanvas.CanvasGroup",			/* tp_name */
    sizeof(PyGObject),	        /* tp_basicsize */
    0,					/* tp_itemsize */
    /* methods */
    (destructor)0,			/* tp_dealloc */
    (printfunc)0,			/* tp_print */
    (getattrfunc)0,	/* tp_getattr */
    (setattrfunc)0,	/* tp_setattr */
    (cmpfunc)0,		/* tp_compare */
    (reprfunc)0,		/* tp_repr */
    0,			/* tp_as_number */
    0,		/* tp_as_sequence */
    0,			/* tp_as_mapping */
    (hashfunc)0,		/* tp_hash */
    (ternaryfunc)0,		/* tp_call */
    (reprfunc)0,		/* tp_str */
    (getattrofunc)0,			/* tp_getattro */
    (setattrofunc)0,			/* tp_setattro */
    0,					/* tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /* tp_flags */
    NULL, 				/* Documentation string */
    (traverseproc)0,			/* tp_traverse */
    (inquiry)0,			/* tp_clear */
    (richcmpfunc)0,	/* tp_richcompare */
    offsetof(PyGObject, weakreflist),             /* tp_weaklistoffset */
    (getiterfunc)0,		/* tp_iter */
    (iternextfunc)0,	/* tp_iternext */
    _PyDiaCanvasGroup_methods,			/* tp_methods */
    0,					/* tp_members */
    0,		       	/* tp_getset */
    NULL,				/* tp_base */
    NULL,				/* tp_dict */
    (descrgetfunc)0,	/* tp_descr_get */
    (descrsetfunc)0,	/* tp_descr_set */
    offsetof(PyGObject, inst_dict),                 /* tp_dictoffset */
    (initproc)pygobject_no_constructor,		/* tp_init */
};



/* ----------- DiaCanvasGrid ----------- */

PyTypeObject PyDiaCanvasGrid_Type = {
    PyObject_HEAD_INIT(NULL)
    0,					/* ob_size */
    "dianewcanvas.CanvasGrid",			/* tp_name */
    sizeof(PyGObject),	        /* tp_basicsize */
    0,					/* tp_itemsize */
    /* methods */
    (destructor)0,			/* tp_dealloc */
    (printfunc)0,			/* tp_print */
    (getattrfunc)0,	/* tp_getattr */
    (setattrfunc)0,	/* tp_setattr */
    (cmpfunc)0,		/* tp_compare */
    (reprfunc)0,		/* tp_repr */
    0,			/* tp_as_number */
    0,		/* tp_as_sequence */
    0,			/* tp_as_mapping */
    (hashfunc)0,		/* tp_hash */
    (ternaryfunc)0,		/* tp_call */
    (reprfunc)0,		/* tp_str */
    (getattrofunc)0,			/* tp_getattro */
    (setattrofunc)0,			/* tp_setattro */
    0,					/* tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /* tp_flags */
    NULL, 				/* Documentation string */
    (traverseproc)0,			/* tp_traverse */
    (inquiry)0,			/* tp_clear */
    (richcmpfunc)0,	/* tp_richcompare */
    offsetof(PyGObject, weakreflist),             /* tp_weaklistoffset */
    (getiterfunc)0,		/* tp_iter */
    (iternextfunc)0,	/* tp_iternext */
    NULL,			/* tp_methods */
    0,					/* tp_members */
    0,		       	/* tp_getset */
    NULL,				/* tp_base */
    NULL,				/* tp_dict */
    (descrgetfunc)0,	/* tp_descr_get */
    (descrsetfunc)0,	/* tp_descr_set */
    offsetof(PyGObject, inst_dict),                 /* tp_dictoffset */
    (initproc)pygobject_no_constructor,		/* tp_init */
};



/* ----------- DiaCanvasConnection ----------- */

PyTypeObject PyDiaCanvasConnection_Type = {
    PyObject_HEAD_INIT(NULL)
    0,					/* ob_size */
    "dianewcanvas.CanvasConnection",			/* tp_name */
    sizeof(PyGObject),	        /* tp_basicsize */
    0,					/* tp_itemsize */
    /* methods */
    (destructor)0,			/* tp_dealloc */
    (printfunc)0,			/* tp_print */
    (getattrfunc)0,	/* tp_getattr */
    (setattrfunc)0,	/* tp_setattr */
    (cmpfunc)0,		/* tp_compare */
    (reprfunc)0,		/* tp_repr */
    0,			/* tp_as_number */
    0,		/* tp_as_sequence */
    0,			/* tp_as_mapping */
    (hashfunc)0,		/* tp_hash */
    (ternaryfunc)0,		/* tp_call */
    (reprfunc)0,		/* tp_str */
    (getattrofunc)0,			/* tp_getattro */
    (setattrofunc)0,			/* tp_setattro */
    0,					/* tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /* tp_flags */
    NULL, 				/* Documentation string */
    (traverseproc)0,			/* tp_traverse */
    (inquiry)0,			/* tp_clear */
    (richcmpfunc)0,	/* tp_richcompare */
    offsetof(PyGObject, weakreflist),             /* tp_weaklistoffset */
    (getiterfunc)0,		/* tp_iter */
    (iternextfunc)0,	/* tp_iternext */
    NULL,			/* tp_methods */
    0,					/* tp_members */
    0,		       	/* tp_getset */
    NULL,				/* tp_base */
    NULL,				/* tp_dict */
    (descrgetfunc)0,	/* tp_descr_get */
    (descrsetfunc)0,	/* tp_descr_set */
    offsetof(PyGObject, inst_dict),                 /* tp_dictoffset */
    (initproc)pygobject_no_constructor,		/* tp_init */
};



/* ----------- DiaCanvasAttracted ----------- */

PyTypeObject PyDiaCanvasAttracted_Type = {
    PyObject_HEAD_INIT(NULL)
    0,					/* ob_size */
    "dianewcanvas.CanvasAttracted",			/* tp_name */
    sizeof(PyGObject),	        /* tp_basicsize */
    0,					/* tp_itemsize */
    /* methods */
    (destructor)0,			/* tp_dealloc */
    (printfunc)0,			/* tp_print */
    (getattrfunc)0,	/* tp_getattr */
    (setattrfunc)0,	/* tp_setattr */
    (cmpfunc)0,		/* tp_compare */
    (reprfunc)0,		/* tp_repr */
    0,			/* tp_as_number */
    0,		/* tp_as_sequence */
    0,			/* tp_as_mapping */
    (hashfunc)0,		/* tp_hash */
    (ternaryfunc)0,		/* tp_call */
    (reprfunc)0,		/* tp_str */
    (getattrofunc)0,			/* tp_getattro */
    (setattrofunc)0,			/* tp_setattro */
    0,					/* tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /* tp_flags */
    NULL, 				/* Documentation string */
    (traverseproc)0,			/* tp_traverse */
    (inquiry)0,			/* tp_clear */
    (richcmpfunc)0,	/* tp_richcompare */
    offsetof(PyGObject, weakreflist),             /* tp_weaklistoffset */
    (getiterfunc)0,		/* tp_iter */
    (iternextfunc)0,	/* tp_iternext */
    NULL,			/* tp_methods */
    0,					/* tp_members */
    0,		       	/* tp_getset */
    NULL,				/* tp_base */
    NULL,				/* tp_dict */
    (descrgetfunc)0,	/* tp_descr_get */
    (descrsetfunc)0,	/* tp_descr_set */
    offsetof(PyGObject, inst_dict),                 /* tp_dictoffset */
    (initproc)pygobject_no_constructor,		/* tp_init */
};



/* ----------- DiaCanvasAttract ----------- */

PyTypeObject PyDiaCanvasAttract_Type = {
    PyObject_HEAD_INIT(NULL)
    0,					/* ob_size */
    "dianewcanvas.CanvasAttract",			/* tp_name */
    sizeof(PyGObject),	        /* tp_basicsize */
    0,					/* tp_itemsize */
    /* methods */
    (destructor)0,			/* tp_dealloc */
    (printfunc)0,			/* tp_print */
    (getattrfunc)0,	/* tp_getattr */
    (setattrfunc)0,	/* tp_setattr */
    (cmpfunc)0,		/* tp_compare */
    (reprfunc)0,		/* tp_repr */
    0,			/* tp_as_number */
    0,		/* tp_as_sequence */
    0,			/* tp_as_mapping */
    (hashfunc)0,		/* tp_hash */
    (ternaryfunc)0,		/* tp_call */
    (reprfunc)0,		/* tp_str */
    (getattrofunc)0,			/* tp_getattro */
    (setattrofunc)0,			/* tp_setattro */
    0,					/* tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /* tp_flags */
    NULL, 				/* Documentation string */
    (traverseproc)0,			/* tp_traverse */
    (inquiry)0,			/* tp_clear */
    (richcmpfunc)0,	/* tp_richcompare */
    offsetof(PyGObject, weakreflist),             /* tp_weaklistoffset */
    (getiterfunc)0,		/* tp_iter */
    (iternextfunc)0,	/* tp_iternext */
    NULL,			/* tp_methods */
    0,					/* tp_members */
    0,		       	/* tp_getset */
    NULL,				/* tp_base */
    NULL,				/* tp_dict */
    (descrgetfunc)0,	/* tp_descr_get */
    (descrsetfunc)0,	/* tp_descr_set */
    offsetof(PyGObject, inst_dict),                 /* tp_dictoffset */
    (initproc)pygobject_no_constructor,		/* tp_init */
};



/* ----------- DiaCanvasArc ----------- */

PyTypeObject PyDiaCanvasArc_Type = {
    PyObject_HEAD_INIT(NULL)
    0,					/* ob_size */
    "dianewcanvas.CanvasArc",			/* tp_name */
    sizeof(PyGObject),	        /* tp_basicsize */
    0,					/* tp_itemsize */
    /* methods */
    (destructor)0,			/* tp_dealloc */
    (printfunc)0,			/* tp_print */
    (getattrfunc)0,	/* tp_getattr */
    (setattrfunc)0,	/* tp_setattr */
    (cmpfunc)0,		/* tp_compare */
    (reprfunc)0,		/* tp_repr */
    0,			/* tp_as_number */
    0,		/* tp_as_sequence */
    0,			/* tp_as_mapping */
    (hashfunc)0,		/* tp_hash */
    (ternaryfunc)0,		/* tp_call */
    (reprfunc)0,		/* tp_str */
    (getattrofunc)0,			/* tp_getattro */
    (setattrofunc)0,			/* tp_setattro */
    0,					/* tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /* tp_flags */
    NULL, 				/* Documentation string */
    (traverseproc)0,			/* tp_traverse */
    (inquiry)0,			/* tp_clear */
    (richcmpfunc)0,	/* tp_richcompare */
    offsetof(PyGObject, weakreflist),             /* tp_weaklistoffset */
    (getiterfunc)0,		/* tp_iter */
    (iternextfunc)0,	/* tp_iternext */
    NULL,			/* tp_methods */
    0,					/* tp_members */
    0,		       	/* tp_getset */
    NULL,				/* tp_base */
    NULL,				/* tp_dict */
    (descrgetfunc)0,	/* tp_descr_get */
    (descrsetfunc)0,	/* tp_descr_set */
    offsetof(PyGObject, inst_dict),                 /* tp_dictoffset */
    (initproc)pygobject_no_constructor,		/* tp_init */
};



/* ----------- DiaCanvasLine ----------- */

PyTypeObject PyDiaCanvasLine_Type = {
    PyObject_HEAD_INIT(NULL)
    0,					/* ob_size */
    "dianewcanvas.CanvasLine",			/* tp_name */
    sizeof(PyGObject),	        /* tp_basicsize */
    0,					/* tp_itemsize */
    /* methods */
    (destructor)0,			/* tp_dealloc */
    (printfunc)0,			/* tp_print */
    (getattrfunc)0,	/* tp_getattr */
    (setattrfunc)0,	/* tp_setattr */
    (cmpfunc)0,		/* tp_compare */
    (reprfunc)0,		/* tp_repr */
    0,			/* tp_as_number */
    0,		/* tp_as_sequence */
    0,			/* tp_as_mapping */
    (hashfunc)0,		/* tp_hash */
    (ternaryfunc)0,		/* tp_call */
    (reprfunc)0,		/* tp_str */
    (getattrofunc)0,			/* tp_getattro */
    (setattrofunc)0,			/* tp_setattro */
    0,					/* tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /* tp_flags */
    NULL, 				/* Documentation string */
    (traverseproc)0,			/* tp_traverse */
    (inquiry)0,			/* tp_clear */
    (richcmpfunc)0,	/* tp_richcompare */
    offsetof(PyGObject, weakreflist),             /* tp_weaklistoffset */
    (getiterfunc)0,		/* tp_iter */
    (iternextfunc)0,	/* tp_iternext */
    NULL,			/* tp_methods */
    0,					/* tp_members */
    0,		       	/* tp_getset */
    NULL,				/* tp_base */
    NULL,				/* tp_dict */
    (descrgetfunc)0,	/* tp_descr_get */
    (descrsetfunc)0,	/* tp_descr_set */
    offsetof(PyGObject, inst_dict),                 /* tp_dictoffset */
    (initproc)pygobject_no_constructor,		/* tp_init */
};



/* ----------- DiaCanvasPoint ----------- */

PyTypeObject PyDiaCanvasPoint_Type = {
    PyObject_HEAD_INIT(NULL)
    0,					/* ob_size */
    "dianewcanvas.CanvasPoint",			/* tp_name */
    sizeof(PyGObject),	        /* tp_basicsize */
    0,					/* tp_itemsize */
    /* methods */
    (destructor)0,			/* tp_dealloc */
    (printfunc)0,			/* tp_print */
    (getattrfunc)0,	/* tp_getattr */
    (setattrfunc)0,	/* tp_setattr */
    (cmpfunc)0,		/* tp_compare */
    (reprfunc)0,		/* tp_repr */
    0,			/* tp_as_number */
    0,		/* tp_as_sequence */
    0,			/* tp_as_mapping */
    (hashfunc)0,		/* tp_hash */
    (ternaryfunc)0,		/* tp_call */
    (reprfunc)0,		/* tp_str */
    (getattrofunc)0,			/* tp_getattro */
    (setattrofunc)0,			/* tp_setattro */
    0,					/* tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /* tp_flags */
    NULL, 				/* Documentation string */
    (traverseproc)0,			/* tp_traverse */
    (inquiry)0,			/* tp_clear */
    (richcmpfunc)0,	/* tp_richcompare */
    offsetof(PyGObject, weakreflist),             /* tp_weaklistoffset */
    (getiterfunc)0,		/* tp_iter */
    (iternextfunc)0,	/* tp_iternext */
    NULL,			/* tp_methods */
    0,					/* tp_members */
    0,		       	/* tp_getset */
    NULL,				/* tp_base */
    NULL,				/* tp_dict */
    (descrgetfunc)0,	/* tp_descr_get */
    (descrsetfunc)0,	/* tp_descr_set */
    offsetof(PyGObject, inst_dict),                 /* tp_dictoffset */
    (initproc)pygobject_no_constructor,		/* tp_init */
};



/* ----------- DiaCanvasPolygon ----------- */

PyTypeObject PyDiaCanvasPolygon_Type = {
    PyObject_HEAD_INIT(NULL)
    0,					/* ob_size */
    "dianewcanvas.CanvasPolygon",			/* tp_name */
    sizeof(PyGObject),	        /* tp_basicsize */
    0,					/* tp_itemsize */
    /* methods */
    (destructor)0,			/* tp_dealloc */
    (printfunc)0,			/* tp_print */
    (getattrfunc)0,	/* tp_getattr */
    (setattrfunc)0,	/* tp_setattr */
    (cmpfunc)0,		/* tp_compare */
    (reprfunc)0,		/* tp_repr */
    0,			/* tp_as_number */
    0,		/* tp_as_sequence */
    0,			/* tp_as_mapping */
    (hashfunc)0,		/* tp_hash */
    (ternaryfunc)0,		/* tp_call */
    (reprfunc)0,		/* tp_str */
    (getattrofunc)0,			/* tp_getattro */
    (setattrofunc)0,			/* tp_setattro */
    0,					/* tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /* tp_flags */
    NULL, 				/* Documentation string */
    (traverseproc)0,			/* tp_traverse */
    (inquiry)0,			/* tp_clear */
    (richcmpfunc)0,	/* tp_richcompare */
    offsetof(PyGObject, weakreflist),             /* tp_weaklistoffset */
    (getiterfunc)0,		/* tp_iter */
    (iternextfunc)0,	/* tp_iternext */
    NULL,			/* tp_methods */
    0,					/* tp_members */
    0,		       	/* tp_getset */
    NULL,				/* tp_base */
    NULL,				/* tp_dict */
    (descrgetfunc)0,	/* tp_descr_get */
    (descrsetfunc)0,	/* tp_descr_set */
    offsetof(PyGObject, inst_dict),                 /* tp_dictoffset */
    (initproc)pygobject_no_constructor,		/* tp_init */
};



/* ----------- DiaCanvasRectangle ----------- */

PyTypeObject PyDiaCanvasRectangle_Type = {
    PyObject_HEAD_INIT(NULL)
    0,					/* ob_size */
    "dianewcanvas.CanvasRectangle",			/* tp_name */
    sizeof(PyGObject),	        /* tp_basicsize */
    0,					/* tp_itemsize */
    /* methods */
    (destructor)0,			/* tp_dealloc */
    (printfunc)0,			/* tp_print */
    (getattrfunc)0,	/* tp_getattr */
    (setattrfunc)0,	/* tp_setattr */
    (cmpfunc)0,		/* tp_compare */
    (reprfunc)0,		/* tp_repr */
    0,			/* tp_as_number */
    0,		/* tp_as_sequence */
    0,			/* tp_as_mapping */
    (hashfunc)0,		/* tp_hash */
    (ternaryfunc)0,		/* tp_call */
    (reprfunc)0,		/* tp_str */
    (getattrofunc)0,			/* tp_getattro */
    (setattrofunc)0,			/* tp_setattro */
    0,					/* tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /* tp_flags */
    NULL, 				/* Documentation string */
    (traverseproc)0,			/* tp_traverse */
    (inquiry)0,			/* tp_clear */
    (richcmpfunc)0,	/* tp_richcompare */
    offsetof(PyGObject, weakreflist),             /* tp_weaklistoffset */
    (getiterfunc)0,		/* tp_iter */
    (iternextfunc)0,	/* tp_iternext */
    NULL,			/* tp_methods */
    0,					/* tp_members */
    0,		       	/* tp_getset */
    NULL,				/* tp_base */
    NULL,				/* tp_dict */
    (descrgetfunc)0,	/* tp_descr_get */
    (descrsetfunc)0,	/* tp_descr_set */
    offsetof(PyGObject, inst_dict),                 /* tp_dictoffset */
    (initproc)pygobject_no_constructor,		/* tp_init */
};



/* ----------- DiaCanvasEllipse ----------- */

PyTypeObject PyDiaCanvasEllipse_Type = {
    PyObject_HEAD_INIT(NULL)
    0,					/* ob_size */
    "dianewcanvas.CanvasEllipse",			/* tp_name */
    sizeof(PyGObject),	        /* tp_basicsize */
    0,					/* tp_itemsize */
    /* methods */
    (destructor)0,			/* tp_dealloc */
    (printfunc)0,			/* tp_print */
    (getattrfunc)0,	/* tp_getattr */
    (setattrfunc)0,	/* tp_setattr */
    (cmpfunc)0,		/* tp_compare */
    (reprfunc)0,		/* tp_repr */
    0,			/* tp_as_number */
    0,		/* tp_as_sequence */
    0,			/* tp_as_mapping */
    (hashfunc)0,		/* tp_hash */
    (ternaryfunc)0,		/* tp_call */
    (reprfunc)0,		/* tp_str */
    (getattrofunc)0,			/* tp_getattro */
    (setattrofunc)0,			/* tp_setattro */
    0,					/* tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /* tp_flags */
    NULL, 				/* Documentation string */
    (traverseproc)0,			/* tp_traverse */
    (inquiry)0,			/* tp_clear */
    (richcmpfunc)0,	/* tp_richcompare */
    offsetof(PyGObject, weakreflist),             /* tp_weaklistoffset */
    (getiterfunc)0,		/* tp_iter */
    (iternextfunc)0,	/* tp_iternext */
    NULL,			/* tp_methods */
    0,					/* tp_members */
    0,		       	/* tp_getset */
    NULL,				/* tp_base */
    NULL,				/* tp_dict */
    (descrgetfunc)0,	/* tp_descr_get */
    (descrsetfunc)0,	/* tp_descr_set */
    offsetof(PyGObject, inst_dict),                 /* tp_dictoffset */
    (initproc)pygobject_no_constructor,		/* tp_init */
};



/* ----------- DiaCanvasText ----------- */

PyTypeObject PyDiaCanvasText_Type = {
    PyObject_HEAD_INIT(NULL)
    0,					/* ob_size */
    "dianewcanvas.CanvasText",			/* tp_name */
    sizeof(PyGObject),	        /* tp_basicsize */
    0,					/* tp_itemsize */
    /* methods */
    (destructor)0,			/* tp_dealloc */
    (printfunc)0,			/* tp_print */
    (getattrfunc)0,	/* tp_getattr */
    (setattrfunc)0,	/* tp_setattr */
    (cmpfunc)0,		/* tp_compare */
    (reprfunc)0,		/* tp_repr */
    0,			/* tp_as_number */
    0,		/* tp_as_sequence */
    0,			/* tp_as_mapping */
    (hashfunc)0,		/* tp_hash */
    (ternaryfunc)0,		/* tp_call */
    (reprfunc)0,		/* tp_str */
    (getattrofunc)0,			/* tp_getattro */
    (setattrofunc)0,			/* tp_setattro */
    0,					/* tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /* tp_flags */
    NULL, 				/* Documentation string */
    (traverseproc)0,			/* tp_traverse */
    (inquiry)0,			/* tp_clear */
    (richcmpfunc)0,	/* tp_richcompare */
    offsetof(PyGObject, weakreflist),             /* tp_weaklistoffset */
    (getiterfunc)0,		/* tp_iter */
    (iternextfunc)0,	/* tp_iternext */
    NULL,			/* tp_methods */
    0,					/* tp_members */
    0,		       	/* tp_getset */
    NULL,				/* tp_base */
    NULL,				/* tp_dict */
    (descrgetfunc)0,	/* tp_descr_get */
    (descrsetfunc)0,	/* tp_descr_set */
    offsetof(PyGObject, inst_dict),                 /* tp_dictoffset */
    (initproc)pygobject_no_constructor,		/* tp_init */
};



/* ----------- DiaCanvasView ----------- */

static PyObject *
_wrap_dia_canvas_view_set_zoom(PyGObject *self, PyObject *args, PyObject *kwargs)
{
    static char *kwlist[] = { "zoom", NULL };
    double zoom;

    if (!PyArg_ParseTupleAndKeywords(args, kwargs, "d:DiaCanvasView.set_zoom", kwlist, &zoom))
        return NULL;
    dia_canvas_view_set_zoom(DIA_CANVAS_VIEW(self->obj), zoom);
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
_wrap_dia_canvas_view_set_canvas(PyGObject *self, PyObject *args, PyObject *kwargs)
{
    static char *kwlist[] = { "canvas", NULL };
    PyGObject *canvas;

    if (!PyArg_ParseTupleAndKeywords(args, kwargs, "O!:DiaCanvasView.set_canvas", kwlist, &PyDiaCanvas_Type, &canvas))
        return NULL;
    dia_canvas_view_set_canvas(DIA_CANVAS_VIEW(self->obj), DIA_CANVAS(canvas->obj));
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
_wrap_dia_canvas_view_grab(PyGObject *self, PyObject *args, PyObject *kwargs)
{
    static char *kwlist[] = { "exclusive", NULL };
    int exclusive;

    if (!PyArg_ParseTupleAndKeywords(args, kwargs, "i:DiaCanvasView.grab", kwlist, &exclusive))
        return NULL;
    dia_canvas_view_grab(DIA_CANVAS_VIEW(self->obj), exclusive);
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
_wrap_dia_canvas_view_ungrab(PyGObject *self)
{
    dia_canvas_view_ungrab(DIA_CANVAS_VIEW(self->obj));
    Py_INCREF(Py_None);
    return Py_None;
}

static PyMethodDef _PyDiaCanvasView_methods[] = {
    { "set_zoom", (PyCFunction)_wrap_dia_canvas_view_set_zoom, METH_VARARGS|METH_KEYWORDS },
    { "set_canvas", (PyCFunction)_wrap_dia_canvas_view_set_canvas, METH_VARARGS|METH_KEYWORDS },
    { "grab", (PyCFunction)_wrap_dia_canvas_view_grab, METH_VARARGS|METH_KEYWORDS },
    { "ungrab", (PyCFunction)_wrap_dia_canvas_view_ungrab, METH_NOARGS },
    { NULL, NULL, 0 }
};

PyTypeObject PyDiaCanvasView_Type = {
    PyObject_HEAD_INIT(NULL)
    0,					/* ob_size */
    "dianewcanvas.CanvasView",			/* tp_name */
    sizeof(PyGObject),	        /* tp_basicsize */
    0,					/* tp_itemsize */
    /* methods */
    (destructor)0,			/* tp_dealloc */
    (printfunc)0,			/* tp_print */
    (getattrfunc)0,	/* tp_getattr */
    (setattrfunc)0,	/* tp_setattr */
    (cmpfunc)0,		/* tp_compare */
    (reprfunc)0,		/* tp_repr */
    0,			/* tp_as_number */
    0,		/* tp_as_sequence */
    0,			/* tp_as_mapping */
    (hashfunc)0,		/* tp_hash */
    (ternaryfunc)0,		/* tp_call */
    (reprfunc)0,		/* tp_str */
    (getattrofunc)0,			/* tp_getattro */
    (setattrofunc)0,			/* tp_setattro */
    0,					/* tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /* tp_flags */
    NULL, 				/* Documentation string */
    (traverseproc)0,			/* tp_traverse */
    (inquiry)0,			/* tp_clear */
    (richcmpfunc)0,	/* tp_richcompare */
    offsetof(PyGObject, weakreflist),             /* tp_weaklistoffset */
    (getiterfunc)0,		/* tp_iter */
    (iternextfunc)0,	/* tp_iternext */
    _PyDiaCanvasView_methods,			/* tp_methods */
    0,					/* tp_members */
    0,		       	/* tp_getset */
    NULL,				/* tp_base */
    NULL,				/* tp_dict */
    (descrgetfunc)0,	/* tp_descr_get */
    (descrsetfunc)0,	/* tp_descr_set */
    offsetof(PyGObject, inst_dict),                 /* tp_dictoffset */
    (initproc)pygobject_no_constructor,		/* tp_init */
};



/* ----------- DiaCanvasViewGdk ----------- */

PyTypeObject PyDiaCanvasViewGdk_Type = {
    PyObject_HEAD_INIT(NULL)
    0,					/* ob_size */
    "dianewcanvas.CanvasViewGdk",			/* tp_name */
    sizeof(PyGObject),	        /* tp_basicsize */
    0,					/* tp_itemsize */
    /* methods */
    (destructor)0,			/* tp_dealloc */
    (printfunc)0,			/* tp_print */
    (getattrfunc)0,	/* tp_getattr */
    (setattrfunc)0,	/* tp_setattr */
    (cmpfunc)0,		/* tp_compare */
    (reprfunc)0,		/* tp_repr */
    0,			/* tp_as_number */
    0,		/* tp_as_sequence */
    0,			/* tp_as_mapping */
    (hashfunc)0,		/* tp_hash */
    (ternaryfunc)0,		/* tp_call */
    (reprfunc)0,		/* tp_str */
    (getattrofunc)0,			/* tp_getattro */
    (setattrofunc)0,			/* tp_setattro */
    0,					/* tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /* tp_flags */
    NULL, 				/* Documentation string */
    (traverseproc)0,			/* tp_traverse */
    (inquiry)0,			/* tp_clear */
    (richcmpfunc)0,	/* tp_richcompare */
    offsetof(PyGObject, weakreflist),             /* tp_weaklistoffset */
    (getiterfunc)0,		/* tp_iter */
    (iternextfunc)0,	/* tp_iternext */
    NULL,			/* tp_methods */
    0,					/* tp_members */
    0,		       	/* tp_getset */
    NULL,				/* tp_base */
    NULL,				/* tp_dict */
    (descrgetfunc)0,	/* tp_descr_get */
    (descrsetfunc)0,	/* tp_descr_set */
    offsetof(PyGObject, inst_dict),                 /* tp_dictoffset */
    (initproc)pygobject_no_constructor,		/* tp_init */
};



/* ----------- functions ----------- */

PyMethodDef pydianewcanvas_functions[] = {
    { NULL, NULL, 0 }
};

/* intialise stuff extension classes */
void
pydianewcanvas_register_classes(PyObject *d)
{
    PyObject *module;

    if ((module = PyImport_ImportModule("gobject")) != NULL) {
        PyObject *moddict = PyModule_GetDict(module);

        _PyGObject_Type = (PyTypeObject *)PyDict_GetItemString(moddict, "GObject");
        if (_PyGObject_Type == NULL) {
            PyErr_SetString(PyExc_ImportError,
                "cannot import name GObject from gobject");
            return;
        }
    } else {
        PyErr_SetString(PyExc_ImportError,
            "could not import gobject");
        return;
    }
    if ((module = PyImport_ImportModule("gtk")) != NULL) {
        PyObject *moddict = PyModule_GetDict(module);

        _PyGtkWidget_Type = (PyTypeObject *)PyDict_GetItemString(moddict, "Widget");
        if (_PyGtkWidget_Type == NULL) {
            PyErr_SetString(PyExc_ImportError,
                "cannot import name Widget from gtk");
            return;
        }
    } else {
        PyErr_SetString(PyExc_ImportError,
            "could not import gtk");
        return;
    }


#line 1145 "dianewcanvas.c"
    pygobject_register_class(d, "DiaCanvas", DIA_TYPE_CANVAS, &PyDiaCanvas_Type, Py_BuildValue("(O)", &PyGObject_Type));
    pygobject_register_class(d, "DiaCanvasItem", DIA_TYPE_CANVAS_ITEM, &PyDiaCanvasItem_Type, Py_BuildValue("(O)", &PyGObject_Type));
    pygobject_register_class(d, "DiaCanvasGroup", DIA_TYPE_CANVAS_GROUP, &PyDiaCanvasGroup_Type, Py_BuildValue("(O)", &PyDiaCanvasItem_Type));
    pygobject_register_class(d, "DiaCanvasGrid", DIA_TYPE_CANVAS_GRID, &PyDiaCanvasGrid_Type, Py_BuildValue("(O)", &PyDiaCanvasGroup_Type));
    pygobject_register_class(d, "DiaCanvasConnection", DIA_TYPE_CANVAS_CONNECTION, &PyDiaCanvasConnection_Type, Py_BuildValue("(O)", &PyDiaCanvasItem_Type));
    pygobject_register_class(d, "DiaCanvasAttracted", DIA_TYPE_CANVAS_ATTRACTED, &PyDiaCanvasAttracted_Type, Py_BuildValue("(O)", &PyDiaCanvasItem_Type));
    pygobject_register_class(d, "DiaCanvasAttract", DIA_TYPE_CANVAS_ATTRACT, &PyDiaCanvasAttract_Type, Py_BuildValue("(O)", &PyDiaCanvasItem_Type));
    pygobject_register_class(d, "DiaCanvasArc", DIA_TYPE_CANVAS_ARC, &PyDiaCanvasArc_Type, Py_BuildValue("(O)", &PyDiaCanvasItem_Type));
    pygobject_register_class(d, "DiaCanvasLine", DIA_TYPE_CANVAS_LINE, &PyDiaCanvasLine_Type, Py_BuildValue("(O)", &PyDiaCanvasItem_Type));
    pygobject_register_class(d, "DiaCanvasPoint", DIA_TYPE_CANVAS_POINT, &PyDiaCanvasPoint_Type, Py_BuildValue("(O)", &PyDiaCanvasItem_Type));
    pygobject_register_class(d, "DiaCanvasPolygon", DIA_TYPE_CANVAS_POLYGON, &PyDiaCanvasPolygon_Type, Py_BuildValue("(O)", &PyDiaCanvasItem_Type));
    pygobject_register_class(d, "DiaCanvasRectangle", DIA_TYPE_CANVAS_RECTANGLE, &PyDiaCanvasRectangle_Type, Py_BuildValue("(O)", &PyDiaCanvasItem_Type));
    pygobject_register_class(d, "DiaCanvasEllipse", DIA_TYPE_CANVAS_ELLIPSE, &PyDiaCanvasEllipse_Type, Py_BuildValue("(O)", &PyDiaCanvasRectangle_Type));
    pygobject_register_class(d, "DiaCanvasText", DIA_TYPE_CANVAS_TEXT, &PyDiaCanvasText_Type, Py_BuildValue("(O)", &PyDiaCanvasItem_Type));
    pygobject_register_class(d, "DiaCanvasView", DIA_TYPE_CANVAS_VIEW, &PyDiaCanvasView_Type, Py_BuildValue("(O)", &PyGtkWidget_Type));
    pygobject_register_class(d, "DiaCanvasViewGdk", DIA_TYPE_CANVAS_VIEW_GDK, &PyDiaCanvasViewGdk_Type, Py_BuildValue("(O)", &PyDiaCanvasView_Type));
}
